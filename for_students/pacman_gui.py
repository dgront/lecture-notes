from pacman import *

import pygame

board_drawed = """
#################################
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
#################################
"""

# board_drawed = """
# ################
# #              #
# ################
# """

PXY = 20
SPRITE_COLOR = (100, 100, 0)
WALL_COLOR = (255, 0, 0)
BLACK = (0, 0, 0)

def draw_board(board: Board, window):

    # ---------- Set the screen background
    window.fill(BLACK)
    for i in range(board.width()):
        for j in range(board.height()):
            if isinstance(board.at(i, j), Wall):
                pygame.draw.rect(window, WALL_COLOR, (i * PXY, j * PXY, PXY, PXY))
            else:
                for s in board.at(i, j).my_sprites():
                    draw_sprite(s, window)


def draw_sprite(sprit: Sprite, window):
    x = int(PXY * (sprit.x + 0.4))
    y = int(PXY * (sprit.y + 0.4))
    pygame.draw.circle(window, SPRITE_COLOR, (x, y), int(PXY * 0.4))


if __name__ == "__main__":

    board = Board.board_from_asci(board_drawed)
    pc_mover = ManualWalk()
    sprites = [
        Ghost("g1", *board.random_cell(), RandomWalk()),
        Ghost("g2", *board.random_cell(), RandomWalk()),
        Ghost("g3", *board.random_cell(), PersistentWalk()),
        Ghost("g4", *board.random_cell(), PersistentWalk()),
        PacMan("pc", *board.random_cell(), PersistentWalk()),
    ]
    for si in sprites: board.insert(si)

    pygame.init()

    window = pygame.display.set_mode((board.width() * PXY, board.height() * PXY))
    pygame.display.set_caption("PacMan")
    done = False
    while not done:
        pygame.time.delay(100)

        # ---------- process external input
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                pc_mover.the_direction = Direction.Left
            if event.key == pygame.K_RIGHT:
                pc_mover.the_direction = Direction.Right
            if event.key == pygame.K_UP:
                pc_mover.the_direction = Direction.Up
            if event.key == pygame.K_DOWN:
                pc_mover.the_direction = Direction.Down

        # ---------- move all sprites
        for si in sprites:
            si.move(board)

        # ---------- redraw the board
        draw_board(board, window)

        pygame.display.update()

pygame.quit()
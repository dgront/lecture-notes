.. _Pętle_warunki:

Instrukcje warunkowe i pętle
++++++++++++++++++++++++++++++++++++++

Komendy omawiane w tej części zajęć: ``basename``, ``dirname``, ``sed`` oraz pętla ``for`` i instrukcja warunkowa ``if``.

W bashu możemy korzystać z pętli ``for`` oraz instrukcji warunkowych ``if``. Instrukcje te umożliwiają pisanie *skryptów* - prostych programów interpretowanych przez powłokę Unixa, służących do automatcznego przetwarzania plików. Zanim jednak zaczniemy przetwarzać pliki w pętli, warto zapoznać się z komendą ``find``.


Program ``find``
"""""""""""""""""""

Polecenie ``find`` umożliwia znajdowanie *wszystkich* plików, które spełniają zadane kryteria. Ogólnie składnia polecenia jest następująca: ``find ścieżka -kryterium1 wartość  -kryterium2 wartość ...``. Kryteria mogą być na prawdę różne:

 - ``find ../PDB -name *.pdb`` wyszukuje wszystkie pliki ``*.pdb`` w podanym katalogu
 - ``find ./ -cmin -60 `` wyszukuje w bierzącym katalogu wszystkie pliki i katalogi zmodyfikowane w ciągu ostatnich 60 minut
 - ``find ./ -cmin -60 -type f`` jak wyżej, ale tym razem *tylko pliki*, dzięki kryterium ``-type f``
 - ``find . -name '*.txt' -size 0`` wyszukuje puste pliki (tzn o rozmiarze 0)
 - ``find path -type f ! -size 0`` tym razem pliki niepuste (znak ``!`` neguje wartość logiczną następującego po nim kryterium)
 - ``find . -type f -size +2k -size -8k`` znajduje pliki mniejsze niż 8KB ale większe niż 2KB


Przetwarzanie w pętli
"""""""""""""""""""""""""

W powłoce *bash* istnieje pętla ``for``, mająca następującą składnię:

.. code-block:: bash

  for i in tu lista wielu wyrazow
  do
      echo $i
  done
  



.. _Jak zacząć:

Wstęp
--------------------------------------

Na stronach tych przedstawiłem pracę w linii poleceń terminalu tekstowego. Po jego uchomieniu myszka już nie będzie konieczna; wszystkie komendy trzeba wpisać korzystając z klawiatury. A jak go uruchomić?

W systemie Windows
"""""""""""""""""""""""""""""""""""""""""""""

Najprostsza droga z systemu Windows do maszyn Unixowych wiedzie przez program PuTTY. Należy go pobrać z `tej lokalizacji <http://www.putty.org>`_ i zainstalować. Program PuTTY umożliwia połączenie się z odległym komputerem (serwerem)  korzystając z protokołu SSH. Potrzebne są do tego:

 - adres serwera
 - nazwa użytkownika (login)
 - hasło

Informacje te zostaną podane na pierwszych zajęciach. Pamiętaj, że pracując na odległym komputerze nie będziesz miał dostepu do swoich plików na maszynie z Windows.


W systemie MacOS lub Linx
"""""""""""""""""""""""""""""""""""""""""""""

W tych systemach wystarczy uruchomić program *Terminal*. Będziesz pracował na swoim własnym komputerze i miał dostęp do wszystkich plikó. Logowanie na inne maszyny możliwe jest przez polecenie ``ssh``. W tym przypadku oczywiście trzeba znać adres serwera, login i hasło, jak w przypadku pracy przez PuTTY.


Plan zajęć
--------------------------------------

Przedmiot rozpisany jest na cztery ćwiczenia. Program zajęć obejmuje:

 - zajęcia 1: pliki i katalogi: zakładanie, przenoszenie, kopiowanie i kasowanie; ścieżki i maski
 - zajęcia 2: przetwarzanie plików tekstowych
 - zajęcia 3: zmiennie w bash-u, prosta arytmetyka, program ``find``, tworzenie skryptów
 - zajęcia 4: elementy programowania w bashu: pętla, instrukcja warunkowa, funkcje




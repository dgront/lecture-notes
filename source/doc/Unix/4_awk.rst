.. _awk:

Program awk, zmienne i arytmetyka
++++++++++++++++++++++++++++++++++++++

Zmienne i prosta arytmetyka
"""""""""""""""""""""""""""""""""""""""""""""

Powłoka systemu Linux umożliwia również deklarowanie zmiennych, które pełnią istotną rolę w kontrolowaniu działania powłoki. *Zmienne środowiskowe* kontrolują m.in. kolorowanie listy plików, które drukuje na ekran polecenie ``ls`` albo ścieżkę do uruchamianych programów. Każdy użytkownik może mieć inne ustawienia. Sprawdzić je można poleceniem ``env``.

Każda sesja powłoki, czyli okres od zalogowania się do wylogowania (zamknięcia terminala) jest niezależna. Zmienne zadeklarowane w jednym terminalu nie istnieją w drugim. Istnieje jednak plik konfiguracyjny (np. ``.bashrc`` dla powłoki ``bash``), który jest ładowany na początku sesji. Wpisane tam ustawienia są dostępne w każdym otwartym terminalu. Aby zmiany w pliku ``.bashrc`` były widoczne, po jedgo nagraniu trzeba się wylogować i zalogować ponownie, otworzyć nowy terminal, lub załadować ten plik ponownie komendą ``source ~/.bashrc``.

Zmienne w powłoce wykorzystujemy też do pisania programów - służą do przechowywania wartości.
Zawartość zmiennej wydrukować możemy poleceniem ``echo``:

.. code-block:: bash

  echo $LOGNAME

.. admonition:: Uwaga
  :class: def

  Nazwy zmiennych są *case sensitive*: ``Zmienna``, ``ZMIENNA`` i ``zmienna`` to trzy różne zmienne.

Zmienne deklarujemy przez nadanie im wartości; podczas użycia zmiennej musimy jej nazwę poprzedzić znakiem ``$``:  

.. code-block:: bash

  zmienna="Ala"
  ZMIENNA="ma kota"
  WYNIK=$zmienna" "$ZMIENNA
  echo $WYNIK

Przeanalizuj powyższy przykład. Zauważ, że:

  - między nazwą zmiennej, znakiem ``=``  a nadawaną wartością **nie może być spacji**
  - inicjując zmienne nie dodajemy znaku ``$``
  - wykorzystując już istniejące zmienne zawsze dodajemy ``$``
  - zmienne z założenia są napisami
  - sklejanie napisów polega na połączeniu ich w jeden ciąg


Prosta arytmetyka w bash'u
"""""""""""""""""""""""""""""""""""""""""""""

Obliczenia w terminalu można zrealizować na różne sposoby:

  1) korzystając z poleceń ``declare`` albo ``let``
  2) za pomocą wyrażeń w *podwójnych nawiasach*
  3) wykorzystując program ``expr`` 
  4) wykorzystując program ``bc`` 

Podejścia wymienione w punktach (1), (2) i (3) umożliwiają jedynie operowanie na liczbach całkowitych. Tu omówię tylko  podejście (2). W kolejnej zaś części znajdziesz wprowadzenie do programu ``awk``, który umożliwia operowanie na liczbach rzeczywistych.

Działanie na zmiennych przeprowadzamy w podwójnych nawiasach, np tu: ``x=$((a+5))`` tworzymy nową zmienną dodając do już istniejącej zmiennej ``a`` liczbę ``5``. Zwróć uwagę na składnię! To chyba jedyny wyjątek, kiedy odwołujemy się do istniejącej zmiennej bez znaku ``$``. Znak ``$`` przed nawiasami jest za to wymaganym elementem składni. Wynik działania drukujemy komendą ``echo $x``.



Program ``awk``
"""""""""""""""""""""""""""""""""""""""""""""

Program ``awk`` to temat-rzeka. Na potrzeby tego wykładu przedstawię tylko te najpotrzebniejsze nam dopływy.

Polecenie awk wczytuje dane tekstowe, przetwarza je, i drukuje wyniki. Operacje mogą być wykonywane dla bierzącej linii bądź zbiorczo. Umożliwia on takie operacje jak:

  - policzenie sumy lub średniej z liczb z danej kolumny
  - wycięcie kolumny z pliku
  - wydrukownie wierszy spełniających określone warunki

Argumentem komendy awk jest *skrypt* z zapisem tego, co ``awk`` ma robić dla każdej linii oraz nazwy plików wejściowych. Ów skrypt może składać się z 3 sekcji, na przykład:

.. code-block:: bash

  awk 'BEGIN{s=0} {s+=$1} END{print s}' dane.txt

Przykład ten oblicza sumę liczb z pierwszej kolumny z pliku *dane.txt*. Sekcje ``BEGIN`` i ``END`` nie są obowiązkowe - używamy ich tylko wtedy, kiedy ich naprawdę potrzebujemy. W tym przykładzie ``BEGIN{s=0}`` jest poprawne nie nie konieczne, gdyż ``awk`` domyślnie inicjalizuje wszystkie zmienne wartością 0.0.

.. admonition:: Ćwiczenie 1: Średnia i odchylenie standardowe
  :class: def

  Zmodyfikuj powyższy przykład tak, aby liczył średnią oraz odchylenie standardowe z kolumny danych
  

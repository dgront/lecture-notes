.. _Pliki_katalogi:

Pliki i katalogi
+++++++++++++++++++

Komendy omawiane w tej części zajęć: ``pwd``, ``ls``, ``cd``, ``mkdir``, ``rm``, ``mv``, ``touch``, ``less``, ``tail``, ``head``

System plików
"""""""""""""""""""""""""""""""""""""""""""""

System plików systemu Unix jest pod kilkoma względami inny, niż w Windows:

+-----------------------+---------------------------+-----------------------------------+
|                       | Unix                      | Windows                           |
+=======================+===========================+===================================+
|            hierarchie | jedna - /root             | każdy dysk ma swoją               |
+-----------------------+---------------------------+-----------------------------------+
|                 dyski | brak, dyski jako katalogi | A, B, C, D, E ...                 |
+-----------------------+---------------------------+-----------------------------------+
|                skróty | tak, tzw. *linki*         | tak                               |
+-----------------------+---------------------------+-----------------------------------+
|          nazwy plików | dowolne.dowolne           | pozostałość po DOS: 8.3           |
+-----------------------+---------------------------+-----------------------------------+
| separator w ścieżce   | katalog/podkatalog        | katalog\\\podkatalog              |
+-----------------------+---------------------------+-----------------------------------+
| case sensitive:       | tak                       | nie                               |
|                       |                           |                                   |
| PLIK, Plik, plik      |                           |                                   |
+-----------------------+---------------------------+-----------------------------------+
| katalog domowy        | /home/myusername          |  My Documents                     |
+-----------------------+---------------------------+-----------------------------------+
| uruchamialne programy | wg atrybutu ``x``         | wg rozszerzenia: .bat .com .exe   |
+-----------------------+---------------------------+-----------------------------------+
| pliki ukryte          | . na początku nazwy       | wyklikane we właściwościach       |
+-----------------------+---------------------------+-----------------------------------+
| sprzęt                | wszystko jest plikiem,    | jako oddzielne ikonki do klikania |
|                       |                           |                                   |
|                       | nawet myszka              | w panelu sterowania               |
+-----------------------+---------------------------+-----------------------------------+

Ścieżki
--------------------------------------

Ścieżka to unikalna lokalizacja pliku: jego nazwa z oznaczeniem katalogu. Możliwe są ścieżki *bezwzględne*, których nazwa musi zaczynać się od ``/``,  (np ``/home/dgront/plik.txt``) oraz *względne*, np ``../../plik``. Oznaczenia stosowane w ścieżkach względnych:

  - ``./`` - katalog bieżący
  - ``~/`` - *Twój* katalog domowy
  - ``../`` - jeden katalog wstecz

Powyższe można łączyć, np: ``~/../agatka/plik.txt``


Poruszanie się po katalogach
--------------------------------------

Polecenie ``cd ścieżka`` zmienia katalog na ten w *ścieżce*, np : ``cd /home/dgront/dir``, albo ``cd ../``. **Bonus** : polecenie ``cd`` bez parametrów przenosi Cię do Twojego katalogu domowego, czyli robi to, co ``cd ~/``

Zawartość katalogów podglądamy poleceniem ``ls`` (jak *list*). Dodanie flagi ``-l`` wyświetli je w tabelce, ``-a`` pokaże ukryte pliki a ``-h`` wyświetli ładnie rozmiar plików. Flagi te można połączyć, pisząc ``ls -lah`` 

  - ``ls`` - wyświetla zawartość bieżącego katalogu
  - ``ls ../other_dir/`` - wyświetla zawartość katalogu ``../other_dir/``; generalnie dowolnego wskazanego ścieżką

Krótko o plikach tekstowych
--------------------------------------

Zawartość plików najwygodniej jest podejrzeć poleceniem ``less``. Podgląd jest interaktywny:  zawartość pliku można przewijać strzałkami lub klawiszem *spacja*. Podgląd zamykamy, wciskając klawisz ``q``. Komenda ``cat`` wypisuje zawartość całego pliku na ekran. Czasem interesuje nas jedynie początek pliku - wyświetli nam go polecenie ``head``; koniec pliku zobaczymy dzięku poleceniu ``tail``. Domyślnie komendy ``head`` i ``tail`` wyświetlają 10 pierwszych bądź ostatnich znaków w pliku. Możemy to zmienić, podając konkretną liczbę linii jako flagę (ze znakiem ``-``). Całkowitą liczbą linii w pliku można sprawdzić poleceniem ``wc`` (*word count*), np:

  - ``head 2gb1.pdb`` - wyświetla na ekranie pierwsze 10 linijek pliku ``2gb1.pdb``
  - ``tail -20 2gb1.pdb`` - wyświetla na ekranie ostatnie 20 linijek pliku ``2gb1.pdb``
  - ``cat 2gb1.pdb`` - wyświetla na ekranie cały plik ``2gb1.pdb``
  - ``wc 2gb1.pdb`` - wypisuje liczbę linii, wyrazów oraz znaków w pliku ``2gb1.pdb``

Więcej o pracy z plikami tekstowymi - na kolejnych zajęciach.

Kopiowanie, przenoszenie i kasowanie
--------------------------------------

Polecenie ``cp ścieżka-źródło ścieżka-cel`` (``cp`` jak *copy*) kopiuje ze źródła docelu. Źródło może wskazywać na plik (i ten plik zostanie skopiowany), może też oznaczać katalog. W przypadku kopiowania katalogów trzeba dodać flagę ``-r``, np: ``cp -r dane ../``

Przenoszenie działa jak kopiowanie, służy do tego komenda ``mv`` ( jak *move*) różnica polega na tym że ``ścieżka-źródło`` znika w przypadku przenoszenia a w przypadku kopiowania nie. *Uwaga*: do przenoszenia katalogów nie potrzeba stosować flagi ``-r``.

Pliki kasujemy ``rm plik`` (``rm`` jak *remove*), katalogi ``rm -rf katalog``. **UWAGA**: Kasowanie plików i katalogów w systemie Unix **jest nieodwracalne!**

Maski
--------------------------------------

Kopiowanie 100 plików wg. powyższych reguł wymagałoby wpisania 100 komend, co by było bardzo denerwujące. Na szczęście istnieją maski, które możliwiają *zaznaczanie* grup plików o podobnych nazwach. Wykorzystujemy w nich symbole wieloznaczne: znak ``*`` zastępuje dowolny ciąg znaków (*w tym ciąg pusty*), znak ``?`` oznacza zaś dokładnie jeden dowolny znak. Dodatkowo można zdefiniować swoją własną *klasę znaków*: ``[123ab]`` oznacza *dokładnie jeden* znak z listy w nawiasach. Przy wykorzystaniu masek kopiowanie plików staje się znacznie prostsze, np:

  - ``cp *.pdb ../d`` - kopiuje wszystkie pliki o końcówce ``.pdb`` do innego katalogu
  - ``cp [12]* ../d`` - kopiuje wszystkie pliki zaczynające się znakiem ``1`` lub ``2``
  - ``cp *.??? ../d`` - kopiuje wszystkie pliki o trzyznakowym rozszerzeniu

Ćwiczenia
"""""""""""

  - załóż katalogi: ``dane``, ``wyniki``
  - skopiuj plik
  - skopiuj pliki
  - przenieś pliki
  - skasować pliki / katalogi
  - wyświetlić zawartość pliku





.. _unix:

Podstawy pracy w środowisku Unix
=======================================

.. toctree::
   :maxdepth: 2
   
   1_Początki

.. toctree::
   :maxdepth: 2
   
   2_Pliki_katalogi

.. toctree::
   :maxdepth: 2
   
   3_Przetwarzanie_tekstu
   
.. toctree::
   :maxdepth: 2
   
   4_awk

.. toctree::
   :maxdepth: 2
   
   5_Pętle_warunki


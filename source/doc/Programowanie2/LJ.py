from Vec3 import Vec3

class LJ:

    __slots__ = ['__vi', '__vj', '__epsilon4','__sigma2']

    def __init__(self,v_i, v_j, **params) :
        self.__vi = v_i
        self.__vj = v_j
        self.__epsilon4 = params["epsilon"] * 4
        self.__sigma2 = params["sigma"] * params["sigma"]

    def __call__(self):
        d2 = self.__sigma2 / self.__vi.distance_square_to(self.__vj)
        d6 = d2*d2*d2
        d12 = d6*d6

        return self.__epsilon4 * (d12 - d6)

v_1 = Vec3(0,0,0)
v_2 = Vec3(8,1,0)
en = LJ(v_1, v_2, epsilon = 0.04, sigma = 3.4)

for i in range(0,100) :
    v_1.x = v_1.x + 0.1
    print(v_1.distance_to(v_2), en())


Interpreter (*Interpreter Pattern*)
==============================================

Od wzorca Komenda jest już blisko do Interpretera.

.. note::
   Interpreter wykonuje polecenia, działając na globalnym kontekście (danych)

Wspomniany powyżej *kontekst* to zmienne, do których mogą odwoływać się polecenia. Wydzielmy zatem klasę ``Turtle``,
która będzie owym kontekstem - obiektem na który wpływać będą komendy:

.. literalinclude:: programy/TurtleCommands.py
  :language: python
  :linenos:
  :lines: 1-10

Żółw zna swoje położenie, umie też podać swoją reprezentację jako napis. Teraz powinniśmy zmodyfikować komendy
(czyli klasy ``R``, ``L`` itp tak, aby metoda ``__call__()`` przyjmowała żółwia (obiekt klasy ``Turtle``) jako argument.
Zmiany te trzeba oczywiście wprowadzić najpierw do klasy bazowej:

.. literalinclude:: programy/TurtleCommands.py
  :language: python
  :linenos:
  :lines: 12-38

Analogiczne zmiany wprowadzono do klas ``F`` i ``B``. Ponieważ komendy są obiektami,
możemy przechowywać je w liście, a nawet zdediniować komendę, która zawiera w sobie inne komendy (klasa ``Procedure``):

.. literalinclude:: programy/TurtleCommands.py
  :language: python
  :linenos:
  :lines: 78-97

Samo wykorzystanie interpretera przebiega podobnie, jak poprzednio (linie 3-7). Dodatkowo w linii 1 definiowana jest
procedura rysująca kwadrat. Odpowiednie komendy są wstawiane do tej procedury w linii 2.

.. literalinclude:: programy/TurtleCommands.py
  :language: python
  :linenos:
  :lines: 101-107


Dodatkowo dla każdej komendy zdefiniowano metodę ``__str__()``, co umożliwia np. nagranie programu na dysk
lub wypisanie na ekranie. Kompletny program znajduje się poniżej:

.. raw:: html

   <details>
   <summary>(rozwiń kod programu TurtleCommands.py)/summary>

.. literalinclude:: programy/TurtleCommands.py
   :language: python
   :linenos:

.. raw:: html

   </details>
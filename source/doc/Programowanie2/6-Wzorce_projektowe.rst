Wzorce projektowe
=====================================

.. toctree::
   :maxdepth: 2

   6a-delegacja

.. toctree::
   :maxdepth: 2

   6b-iterator

.. toctree::
   :maxdepth: 2

   6c-dyspozytor.rst

.. toctree::
   :maxdepth: 2

   6d-strategia.rst

.. toctree::
   :maxdepth: 2

   6e-dekorator

.. toctree::
   :maxdepth: 1

   6f-polecenie.rst

.. toctree::
   :maxdepth: 1

   6g-interpreter.rst

.. toctree::
   :maxdepth: 1

   6h-fabryka_klas.rst


.. toctree::
   :maxdepth: 1

   6i-singleton.rst

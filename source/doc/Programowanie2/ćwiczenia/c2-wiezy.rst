Fabryka klas: obliczenia energii więzów
++++++++++++++++++++++++++++++++++++++++++++++++++++

Stwórz prostą hierarchię klas, która umożliwi obliczanie funkcji kary za niedopasowanie struktury cząsteczki
do danych, np eksperymentalnych. Program powinien umożliwiać łatwe konfigurowanie typów więzów; osiągniesz to,
wykorzystując wzorzec *fabryka klas*

Skorzystaj tym celu z podanego poniżej zalążka programu. Dopisz do niego następujace elementy:

  - w klasie bazowej ``AbstractRestrainFunction`` dodaj ``@property`` udostępniające pola ``__ai`` oraz ``__aj``

  - w klasie ``LinearRestrainFunction`` zaimplementuj liczenie wartości funkcji, zgodnie ze wzorem:
    :math:`E_{ij} = k (d_0 - d_{ij})` gdzie :math:`d_0` to odległość wymagana (założona) a :math:`k` to współczynnik;
    wartość :math:`d` to argument funkcji

  - stwórz klasę ``LorentzianRestrainFunction`` i zaimplementuj w niej odpowiednie metody: ``__str__()``, konstruktor
    oraz operator wywołania; energię więzów oblicz wg wzoru: :math:`E_{ij} =  \frac{(d_0 - d_{ij})^2}{(d_0 - d_{ij})^2 + \gamma^2}`

  - stwórz klasę ``HarmonicRestrainFunction``, analogicznie do powyższych; tym razem użyj wzoru:
    :math:`E_{ij} = k (d_0 - d_{ij})^2`

.. literalinclude:: restraints.py
   :language: python
   :linenos:

Powyższy program *powinien* poprawnie obliczać energię więzów dla zadanej konformacji molekuły (zbioru położeń atomów),
gdy tylko zaimplementowane zostaną brakujace metody i klasy. Program wykorzystuje :ref:`klasę Atom <klasa_Atom>`,
która już pojawiła się na tych zajęciach. Będziesz również potrzebował  :ref:`klasy Vec3<klasa_Vec3>`,
po której klasa ``Atom`` dziedziczy. Przykładowy plik ze współrzędnymi :download:`znajduje się tutaj <surpass.pdb>`
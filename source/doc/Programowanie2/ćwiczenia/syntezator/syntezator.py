import math
from abc import ABC, abstractmethod

import numpy
import numpy as np
from matplotlib import pyplot as plt

SAMPLING_RATE = 44100  # 44100 samples per second

class AbstractSound(ABC):

    @abstractmethod
    def play(self, milliseconds: float) -> numpy.ndarray: pass

class SineWave(AbstractSound):
    def __init__(self, frequency: float):
        self.__frequency = frequency

    def play(self, milliseconds: float) -> numpy.ndarray:
        N = int(milliseconds / 1000.0 * SAMPLING_RATE)
        time_in_sec = np.linspace(0, milliseconds/1000.0, N, False)
        s = np.sin(time_in_sec * self.__frequency * 2 * np.pi)
        return s

class PulseWave(AbstractSound):
    def __init__(self, frequency: float, width: float):
        self.__frequency = frequency
        self.__width = width

    def play(self, milliseconds: float) -> numpy.ndarray:
        N = int(milliseconds / 1000.0 * SAMPLING_RATE)
        s = np.zeros(N)
        period = SAMPLING_RATE / self.__frequency
        n_periods = N // period
        for i_period in range(int(n_periods)):
            for i_up in range(int(self.__width*period)):
                s[int(i_period*period+i_up)] = 1.0
        return s


class AbstractEffect(AbstractSound):
    def __init__(self, sound: AbstractSound):
        self._sound = sound

class Power3(AbstractEffect):
    def play(self, milliseconds: int) -> numpy.ndarray:
        s = self._sound.play(milliseconds)
        return s**3


class Combine(AbstractEffect):
    def __init__(self, sound, added_sound):
        super().__init__(sound)
        self.__added_sound = added_sound

    def play(self, milliseconds: int) -> numpy.ndarray:
        s = self._sound.play(milliseconds)
        s2 = self.__added_sound.play(milliseconds)
        for i in range(len(s)):
            s[i] += s2[i]
        return s

class Tremble(AbstractEffect):
    def __init__(self, sound, frequency):
        super().__init__(sound)
        self.__frequency = frequency
        self.__counter = 0.0
        self.__time_interval = 1.0 / SAMPLING_RATE

    @property
    def tremble_frequency(self): return self.__frequency

    @tremble_frequency.setter
    def tremble_frequency(self, f):
        self.__frequency = f

    def play(self, milliseconds: int) -> numpy.ndarray:
        s = self._sound.play(milliseconds)
        for i in range(len(s)):
            self.__counter += self.__time_interval
            s[i] *= math.sin(self.__counter*2*math.pi*self.__frequency)

        return s

def play_buffer(buffer):
    import simpleaudio as sa
    audio = buffer * (2 ** 15 - 1) / np.max(np.abs(buffer))
    # Convert to 16-bit data
    audio = audio.astype(np.int16)
    # Start playback
    play_obj = sa.play_buffer(audio, 1, 2, SAMPLING_RATE)
    # Wait for playback to finish before exiting
    play_obj.wait_done()

def plot_buffer(*buffers):
    for buffer in buffers:
        buffer = buffer[:1000]
        plt.plot(range(len(buffer)), buffer)
    plt.show()


if __name__ == "__main__":
    # https://pages.mtu.edu/~suits/notefreqs.html
    cegc_freq = [261.63, 329.63, 392.00, 523.25]
    c4, e4, g4, c5 = [SineWave(f) for f in cegc_freq]
    c4_tr = Tremble(c4, 5)
    chord = Combine(Combine(c4_tr, e4), g4)
    play_buffer(c4.play(1000))
    play_buffer(e4.play(1000))
    play_buffer(g4.play(1000))
    play_buffer(chord.play(1000))


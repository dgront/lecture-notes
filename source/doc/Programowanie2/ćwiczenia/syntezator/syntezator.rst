Dekorator: syntezator dźwięku
================================

Napisz prosty syntezator dźwięku, który zbudowany będzie z kilku oddzielnych modułów.
Pierwszy z nich generuje falę dźwiękową o zadanej częstotliwości, a kolejne ją modyfikują.
W tym zadaniu wykorzystasz wzorzec projektowy **dekorator**.

Napisz kod, który będzie się składał z:

 1. abstrakcyjnej klasy ``AbstractSound`` i implementujących ich konkretnych dźwięków,
    przede wszystkim ``SineWave``, która w zasadzie jest po prostu funkcją *sinus*
 2. *dekoratorów*, czyli klas dziedziczących po ``AbstractEffect``; przykładowy efekt
    to ``Power3``, który podnosi wartości wartości funkcji dźwięku do trzeciej potęgi.
    Inne efekty to ``Tremble``, która mnoży wartości funkcji dźwięku przez inną funkcję sinus
    (o mniejszej częstotliwości) a także ``Combine``, który składa dwa dźwięki w jeden.

Wyniki działania programu możesz wizualizować, rysując wykresy funkcji korzystając z biblioteki ``matplotlib``,
np tak:

.. literalinclude:: syntezator.py
   :language: python
   :linenos:
   :lines: 6,7,95-99

Inną możliwością jest bezpośrednie odtwarzanie dźwięku, np wykorzystując moduł ``simpleaudio``:

.. literalinclude:: syntezator.py
   :language: python
   :linenos:
   :lines: 85-93
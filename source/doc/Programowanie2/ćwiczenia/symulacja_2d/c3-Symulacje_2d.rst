Symulacja w dwóch wymiarach
++++++++++++++++++++++++++++++++++++++++++++++++++++

Stwórz prostą hierarchię klas, która umożliwi symulację prostych cząsteczek w dwóch wymiarach.

Plan minimum to utworzenie nastepujących klas:

  - ``Atom`` : klasa przechowująca współrzędne :math:`x` oraz :math:`y` atomu, jego ładunek oraz promień; dodatkowo
    klasa ta powinna implementować metodę ``distance_to(a: Atom)``, liczącą odległość do innego atomu; wartość
    odległości pomiędzy atomami  :math:`i` oraz :math:`j` jest oznaczona w poniższych wzorach jako  :math:`d_{ij}`

  - ``Coordinates`` : klasa przechowująca wszystkie atomy (np jako listę) i udostępniać je poprzez funkcje
    ``__getitem__()`` i ``__setitem__()``. Konstruktor klasy ``Coordinates`` powinien wczytywać plik jak podano poniżej
    i tworzyć odpowiednie atomy wywołując konstruktor klasy ``Atom``:

    .. literalinclude:: sim2d-system.txt

  - hierarchia klas umożliwiających liczenie energii różnego typu:

      - ``AbstractEnergy`` : klasa bazowa umożliwiająca liczenie energii; klasa ta jest abstrakcyjna
        i definiuje bezargumentową metodę ``def energy()`` jak poniżej:

        .. literalinclude:: sim2d.py
           :language: python
           :lines: 83-86

      - ``Harmonic`` : licząca energię harmoniczną pojedynczej sprężyny pomiędzy atomami :math:`i` oraz :math:`j`;
        skorzystaj ze wzoru: :math:`E = k (d_0 - d_{ij})^2` gdzie :math:`d_0` to odległość równowagowa
        a :math:`k` to stała sprężystości; obydwie te wartości powinny być przekazane konstruktorowi klasy ``Harmonic``

      - ``Coulomb`` : licząca energię elektrostatyczną całego układu; w tym przypadku wywołanie metody ``energy()``
        powinno policzyć energię całego układu, korzystając ze wzoru:

        .. math::

           E = \sum_{i=1}^{N} \sum_{i=0}^{i-1} c \frac{q_i q_j}{d_{ij}}

      - ``Repulsion`` : licząca energię odpychania dla całego układu; implementacja metody ``energy()`` w tej klasie
        podobna będzie do klasy ``Coulomb``, obliczanie tego członu energii powinno jednak przebiegać wg wzoru:

        .. math::

           E = \sum_{i=1}^{N} \sum_{i=0}^{i-1} c (r_i + r_j - d_{ij})^6

        dla :math:`d_{i,j} < r_i + r_j` gdzie :math:`r_i` to promień i-tego atomu. Klasy ``Harmonic``, ``Coulomb``
        oraz ``Repulsion`` powinny dziedziczyć po ``AbstractEnergy``

  - fabryka klas energii, tworząca obiekt ``TotalEnergy`` na podstawie pliku konfiguracyjnego. klasa ``TotalEnergy``
    powinna mieć metodę ``from_file(fname: str)``, która wczyta plik jak poniżej:

    .. literalinclude:: sim2d-energy.txt

    Aby fabryka klas działała poprawnie, stwórz klasę  ``Maker`` odpowiednią dla klas ``Harmonic``, ``Coulomb``
    oraz ``Repulsion``, wyprowadzając je z abstrakcyjnek klasy ``AbstractMaker``.:
    Klasa ``TotalEnergy`` również powinna dziedziczyć po ``AbstractEnergy``.

     .. literalinclude:: sim2d.py
        :language: python
        :lines: 89-92

Zauważ, że klasy ``Coulomb`` i  ``Repulsion`` potrzebują *wszystkich* współrzędnych w jednym wywołaniu metody ``energy()``,
zaś klasa ``Harmonic`` - tylko dwóch atomów. Można to rozwiązać na dwa sposoby:

  a) ``HarmonicMaker`` może przekazać konstruktorowi ``Harmonic`` obiekt klasy ``Coordinates`` oraz dwa indeky atomów, albo
  b) może sam wybrać te atomy i przekazać je konstruktorowi ``Harmonic``

W przypadku  klas ``Coulomb`` i  ``Repulsion`` nie ma takiego wyboru: klasy te muszą przechowywać obiekt współrzędnych układu

Powyższy program powinien poprawnie obliczać energię dla zadanej konformacji układu (zbioru położeń atomów).
gdy tylko zaimplementowane zostaną brakujace metody i klasy. Program główny (sekcja ``__main__``) powinna być podobna
do tej poniżej:

.. literalinclude:: sim2d.py
   :language: python
   :lines: 293-311

Program ten tworzy obiekt ``Coordinates``, czyli symulowany układ, wczytując współrzędne atomów z pliku. Podobnie z pliku
wczytywana jest definicja pola siłowego (funkcji energii całkowitej). Pliki te dostępne są tu:
:download:`sim2d-system.txt <sim2d-system.txt>` i :download:`sim2d-energy.txt <sim2d-energy.txt>`

Za przeprowadzenie symulacji odpowiedzialna jest klasa MonteCarlo. Jej metoda ``sample()`` losowo modyfikuje położenia losowo
wybranego atomu a następnie akceptuje (bądź odrzuca) nową konformację w opariu o kryterium Metopolisa. Po każdym kroku symulacji
na ekranie drukowana jest aktualna energia układu:

.. literalinclude:: sim2d.py
   :language: python
   :lines: 232-239,241-256

Reasumując: rozwiązanie powyższego problemu wymaga napisania 11 klas:
 - dwie do przechowywania danych o układzie (``Atom`` i ``Coordinates``),
 - trzy rodzaje energii: harmoniczną, Coulombowską i odpychania
 - producenci dla tych trzech typów energii
 - ``TotalEnergy``, która jest jednocześnie fabryką klas energii
 - oraz dwie klasy abstrakcyjne (napisane już powyżej)

Rozwiązanie zapewne nie przekroczy 200 - 250 linii kodu.

.. rubric:: Rozbudowa programu

Naszkicowane powyżej rozwiązanie jest bardzo minimalistyczne. Dodanie około 100 linii kodu istotnie poprawia jego funkcjonalność.

  - Dodaj klasę ``AttachedHarmonic``, która definiuje sprężynkę jednym końcem zaczepioną na wybranym atomie
    a drugim - na podanym punkcie w pudełku symulacji
  - Stwórz hierarchię klas obserwujących przebieg symulacji. Klasę bazową, definiującą interjes ``AbstractObserver``,
    przytaczam poniżej:

    .. literalinclude:: sim2d.py
       :language: python
       :lines: 260-262

    Na przykład klasa ``EnergyObserver`` może obserwować całkowitą energię układu. Każde wywołanie metody ``observe(self)``
    powino policzyć energię (wywołując ``energy()`` z obiektu ``TotalEnergy``) i wydrukować ją na ekran. Obserwacje prowadzić
    będzie klasa ``MonteCarlo``. W tym celu należy: (*i*) stworzyć w konstruktorze ``MonteCarlo`` pustą prywatną listę
    obserwerów oraz  (*ii*) dodać metodę ``add_observer(self, obs:AbstractObserver)``, która będzie dodawać obserwery do tejże listy
    Same obserwacje zaś odbywać będą się w metodzie ``simulate()`` następująco:

    .. literalinclude:: sim2d.py
       :language: python
       :lines: 257-258

  - Rozdzielenie funkcjonalności liczenia energii w ``TotalEnergy`` od wczytywania pliku. Aby skrócić nieco kod potrzebny
    do zrealizowania projektu, klasa ``TotalEnergy`` jest jednocześnie energią oraz fabryką klas; rozwiązanie to jest
    sprzeczne z dobrymi praktykami projektowania obiektowego. Do klasy ``TotalEnergy`` dopisz metodę ``add_component()``,
    działąjącą podobnie do dodawania obserwerów. Stwórz też klasę ``TotalEnergyFromFile``, która będzie fabryką klas.
    Nowo wytworzone człony energii będą dodawane do energii całkowitej poprzez ``add_component(self, en:AbstractEnergy)``

  - Aby klasa ``Coulomb`` mogła policzyła energię, musi w pętli odwiedzić wszystkie pary atomów. Najprościej zrealizować to,
    umożliwiając dostęp do i-tego atomu obiektu ``Coordinates`` oraz ich liczbę. Bardziej eleganckim rozwiązaniem jest
    stworzenie klasy ``AtomIterator``


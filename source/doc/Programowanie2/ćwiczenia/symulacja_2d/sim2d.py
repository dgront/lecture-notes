from __future__ import annotations

import random
from abc import ABC, abstractmethod
from math import sqrt, exp


class AtomIterator:
    def __init__(self,atoms_list):
        self.__atoms = atoms_list
        self.__next = 0

    def __next__(self):
        if self.__next < len(self.__atoms):
            self.__next += 1
            return self.__atoms[self.__next - 1]
        else:
            raise StopIteration()


class Atom:
    def __init__(self, x, y, q, r):
        self.__x = float(x)
        self.__y = float(y)
        self.__q = float(q)
        self.__r = float(r)

    @property
    def x(self): return self.__x

    @x.setter
    def x(self,new_x):
        self.__x = new_x

    @property
    def y(self): return self.__y

    @y.setter
    def y(self, new_y):
        self.__y = new_y

    @property
    def r(self): return self.__r

    @property
    def q(self): return self.__q

    def distance_to(self, aj) -> float:
        dx = self.__x - aj.x
        dy = self.__y - aj.y
        return sqrt(dx * dx + dy * dy)

    def __str__(self):
        return "%5.2f %5.2f %5.2f %6.3f" % (self.x,self.y, self.r, self.q)


class Coordinates:
    def __init__(self, fname_or_text: str):
        self.__atoms = []
        if fname_or_text.find("\n") < 0:
            text = open(fname_or_text).readlines()
        else:
            text = fname_or_text.splitlines()
        for line in text:
            if line.strip()[0] == '#': continue
            tokens = line.strip().split()
            self.__atoms.append(Atom(*tokens[1:]))

    def __getitem__(self, item) -> Atom:
        return self.__atoms[item]

    @property
    def n_atoms(self): return len(self.__atoms)

    def __iter__(self):
        return AtomIterator(self.__atoms)

    def __str__(self):
        s = ""
        for i, atm in enumerate(self.__atoms):
            s += str(i)+" "+str(atm)+"\n"
        return s

class AbstractEnergy(ABC):

    @abstractmethod
    def energy(self): pass


class AbstractEnergyMaker(ABC):

    @abstractmethod
    def make(self, coords:Coordinates, *args): pass


class TotalEnergy(AbstractEnergy):

    def __init__(self):
        self.__components: list(AbstractEnergy) = []
        self.__maker_dispatch = {}

    def register_energy_type(self,name, maker):
        self.__maker_dispatch[name] = maker

    def energy(self):
        val = 0
        for e in self.__components:
            val += e.energy()
        return val

    def from_file(self, fname_or_text):
        if fname_or_text.find("\n") < 0:
            text = open(fname_or_text).readlines()
        else:
            text = fname_or_text.splitlines()
        for line in text:
            tokens = line.strip().split()
            self.__maker_dispatch[tokens[0]].make(*tokens[1:])

    def add_component(self, e:AbstractEnergy):
        self.__components.append(e)


class Harmonic(AbstractEnergy):
    def __init__(self, coords:Coordinates, ai: int, aj: int, d0: float, k: float):
        self.__ai = coords[ai]
        self.__aj = coords[aj]
        self.__d0 = d0
        self.__k = k

    def energy(self):
        d = self.__ai.distance_to(self.__aj)
        dd = d - self.__d0
        return self.__k * dd * dd / 2


class AttachedHarmonic(AbstractEnergy):
    def __init__(self, coords:Coordinates, ai: int, x0: float, y0: float, d0: float, k: float):
        self.__ai = coords[ai]
        self.__x0 = x0
        self.__y0 = y0
        self.__d0 = d0
        self.__k = k

    def energy(self):
        d = self.__ai.distance_to(self.__aj)
        dd = d - self.__d0
        return self.__k * dd * dd / 2


class AttachedHarmonicMaker(AbstractEnergyMaker):

    def make(self, coords, *args):
        i, x0, y0, d0, k = *args[0], *args[1], *args[2], *args[3], *args[4]
        return AttachedHarmonic(coords, int(i), float(y0), float(x0), float(d0), float(k))


class HarmonicMaker(AbstractEnergyMaker):

    def make(self, coords, *args):
        i, j, d0, k = *args[0], *args[1], *args[2], *args[3]
        return Harmonic(coords, int(i), int(j), float(d0), float(k))


class Coulomb(AbstractEnergy):
    def __init__(self,coords, cnst):
        self.__coords = coords
        self.__cnst = float(cnst)

    def energy(self):
        en = 0
        for ai in self.__coords:
            if ai.q == 0: continue
            for aj in self.__coords:
                if ai == aj: break
                en += ai.q * aj.q / ai.distance_to(aj)
        return self.__cnst * en


class CoulombMaker(AbstractEnergyMaker):

    def make(self, coords, *args):
        return Coulomb(coords, args[0])

class Repulsion(AbstractEnergy):
    def __init__(self,coords, cnst):
        self.__coords = coords
        self.__cnst = float(cnst)

    def energy(self):
        en = 0
        for ai in self.__coords:
            for aj in self.__coords:
                r = ai.r + aj.r
                d = ai.distance_to(aj)
                if d<r:
                    en += (r-d)**6
        return self.__cnst * en


class RepulsionMaker(AbstractEnergyMaker):

    def make(self, coords, *args):
        return Coulomb(coords, args[0])


class EnergyFromFile:
    def __init__(self, coordinates):
        self.__maker_dispatch = {}
        self.__coords = coordinates

    def register_energy_type(self, name, maker):
        self.__maker_dispatch[name] = maker

    def is_known_type(self, name): return name in self.__maker_dispatch

    def from_file(self, fname_or_text) ->TotalEnergy:
        total = TotalEnergy()
        if fname_or_text.find("\n") < 0:
            text = open(fname_or_text).readlines()
        else:
            text = fname_or_text.splitlines()
        for line in text:
            tokens = line.strip().split()
            if len(tokens) > 0:
                if not self.is_known_type(tokens[0]):
                    print("Unknown energy type",tokens[0])
                ee = self.__maker_dispatch[tokens[0]].make(self.__coords, *tokens[1:])
                total.add_component(ee)

        return total

class MonteCarlo:
    def __init__(self, coords: Coordinates, total_en: AbstractEnergy, T: float):
        self.__T = T
        self.__coords = coords
        self.__max_step = 0.1
        self.__en = total_en
        self.__obs = []

    def add_observer(self, o): self.__obs.append(o)

    def sample(self,n_steps):
        for i in range(n_steps):
            for j in range(self.__coords.n_atoms):
                dx = random.uniform(-self.__max_step,self.__max_step)
                dy = random.uniform(-self.__max_step, self.__max_step)
                old_x, old_y = self.__coords[j].x, self.__coords[j].y
                old_en = self.__en.energy()
                self.__coords[j].x = self.__coords[j].x + dx
                self.__coords[j].y = self.__coords[j].y + dy
                new_en = self.__en.energy()
                delta_e = new_en-old_en
                if delta_e > 0 and random.random() > exp(-delta_e/self.__T):
                    self.__coords[j].x = old_x
                    self.__coords[j].y = old_y
            # print(self.__en.energy())     # --- print energy to check how the sim is going
            for o in self.__obs:
                o.observe()


class AbstractObserver(ABC):
    @abstractmethod
    def observe(self): pass


class EnergyOnScreen(AbstractObserver):
    def __init__(self, energy_func:AbstractEnergy):
        self.__en = energy_func

    def observe(self):
        print(self.__en.energy())


class ObservePDBTrajectory(AbstractObserver):
    def __init__(self, system: Coordinates, fname:str):
        self.__crds = system
        self.__i_frame = 0
        self.__fname = fname
        f = open(self.__fname, "w")
        f.close()

    def observe(self):
        self.__i_frame += 1
        f = open(self.__fname, "a")
        print("MODEL  %6d" % (self.__i_frame), file=f)
        for i in range(self.__crds.n_atoms):
            a = self.__crds[i]
            print("ATOM   %4d %4s  ARG A   1    %8.3f%8.3f%8.3f  0.50 35.88           N" % \
                (i+1, " C  ", a.x, a.y, 0), file=f)
        print("ENDMDL", file=f)
        f.close()


if __name__ == "__main__":

    crds = Coordinates("sim2d-system.txt")
    # print(crds)                   # --- check if the system is OK

    factory = EnergyFromFile(crds)
    factory.register_energy_type("HARMONIC",HarmonicMaker())
    factory.register_energy_type("ATTACHED_HARMONIC", AttachedHarmonicMaker())
    factory.register_energy_type("COULOMB",CoulombMaker())
    factory.register_energy_type("REPULSION", RepulsionMaker())

    en = factory.from_file("sim2d-energy.txt")
    print(en.energy())              # --- check initial energy

    simulation = MonteCarlo(crds,en,3.0)
    simulation.add_observer(EnergyOnScreen(en))
    tra = ObservePDBTrajectory(crds, "tra.pdb")
    simulation.add_observer(tra)
    simulation.sample(1000)

    # tra.observe()
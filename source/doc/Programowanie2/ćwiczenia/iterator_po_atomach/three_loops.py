from protein_structure import *

strctr = read_pdb_content("2GB1", open("2gb1.pdb"))
for chain in strctr.chains():
    for residue in chain.residues():
        for atom in residue.atoms():
            print(atom.name)
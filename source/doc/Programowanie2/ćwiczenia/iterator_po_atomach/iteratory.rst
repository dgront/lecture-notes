Iteratory
================================

Celem tego zadania jest dodanie brakujących iteratorów do klas ``Chain`` i ``Structure``.

Klasy opisujące biomakromolekuły
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Makromolekuły z których zbudowane są organizmy żywe: białka oraz kwasy nukleinowe
składają się z oddzielnych łańcuchów. Każdy z nich składa się z reszt aminokwasowych
(w przypadku białek) bądź nukleotydów (kwasy nukleinowe). Każda z reszt zaś zbudowana
jest z atomów. Poniższy program jest odtworzeniem tej biochemicznej konstrukcji w postaci klas
``Structure``, ``Chain``, ``Residue`` i ``Atom``. Struktura biomolekuły (``Structure``)
przechowuje w prywatnym słowniku swoje łańcuchy. Każdy z łańcuchów (``Chain``) zawiera słownik
reszt. Wreszcie, każda z reszt (``Residue``) zawiera listę jej atomów. Kompletny kod tych klas
znajduje się na dole strony.

Iteratory
^^^^^^^^^^

W przyjętej konstrukcji odwiedzenie wszystkich atomów ze struktury wymaga *de facto* trzech petli:

.. literalinclude:: three_loops.py
   :language: python
   :linenos:

Zaimplementuj brakujące metody: ``atoms()`` i ``residues()`` w klasie ``Structure``,
oraz ``atoms()`` w klasie ``Chain`` tak, aby możliwe było iterowanie
po atomach i resztach w jednej pętli. Dla przykładu, poniższy kod powinien prawidłowo policzyć
środek masy całego białka.

.. literalinclude:: single_loop.py
   :language: python
   :linenos:

Zaimplementowanie iteratorów przyniesie także inne korzyści. Dla przykładu, użytkownicy
będą mogli wykorzystać bibliotekę ``itertools`` w analizie struktur.
Poniżej, oprócz klas reprezentujących biomolekuły, dołączono prosty test
obliczający mapę odległości pomiędzy węglami alfa (atomy o nazwie " CA ") wczytanego białka.

.. literalinclude:: protein_structure.py
   :language: python
   :linenos:

Przykładowy plik w formacie PDB :download:`znajduje się tutaj <2gb1.pdb>`.

Poniżej zaś znajduje się szkic rozwiązania tego zadania:

.. raw:: html

   <details>
   <summary>(rozwiń szkic rozwiązania)</summary>

.. literalinclude:: szkic_rozwiązania.py
   :language: python
   :linenos:

.. raw:: html

   </details>
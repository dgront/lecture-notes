from protein_structure import Structure, Chain


class AtomsInChainIterator:
    def __init__(self, chain: Chain):
        self.__residues = list(chain.residues())
        self.__current_residue = 0
        self.__current_atom = 0

    def __next__(self):
        # sprawdź, czy możesz dać (__current_atom+1) atom z __current_residue
        # TAK:
        #    __current_atom+=1
        #    zwróć __current_atom-1
        # NIE:
        #    __current_residue += 1
        #    sprawdź, czy nie wyszliśmy poza łańcuch
        #    NIE:
        #        __current_residue += 1
        #        __current_atom = 1
        #        zwróć atom 0
        #    TAK:
        #        raise StopIteration
        pass


class AtomsInStructureIterator:
    def __init__(self, strctr: Structure):
        self.__chains = list(strctr.chains())
        self.__current_chain = 0
        self.__current_chain_it = AtomsInChainIterator(self.__chains[0])

    def __next__(self):
        try:
            a = self.__current_chain_it.__next__()
            return a
        except:
            # przesuń AtomsInChainIterator o jeden łańcuch
            # zwróć atom
            pass


class Structure:

    def atoms(self):
        return AtomsInStructureIterator(self)
from __future__ import annotations

import itertools
import math
from dataclasses import dataclass, field
from typing import Dict


@dataclass
class Structure:
    id_code: str
    __chains: Dict[str, Chain] = field(default_factory=dict)

    def get_chain(self, chain_id: str):
        return self.__chains.get(chain_id, None)

    def add_chain(self, chain: Chain):
        self.__chains[chain.code] = chain

    def chains(self):
        return self.__chains.values()

    def atoms(self):
        raise NotImplemented

    def residues(self):
        raise NotImplemented

@dataclass
class Chain:
    code: str
    __residues: Dict[str, Residue] = field(default_factory=dict)

    def get_residue(self, res_seq: int, i_code: str):
        key = str(res_seq) + i_code
        return self.__residues.get(key, None)

    def add_residue(self, res: Residue):
        key = str(res.res_seq) + res.i_code
        self.__residues[key] = res

    def residues(self):
        return self.__residues.values()

    def atoms(self):
        raise NotImplemented


@dataclass
class Residue:
    res_name: str
    res_seq: int
    i_code: str
    __atoms: list[Atom] = field(default_factory=list)

    def add_atom(self, atom):
        self.__atoms.append(atom)

    def ca(self):
        """Returns the alpha carbon of this residue or None when not present"""
        return next((a for a in self.__atoms if a.name == " CA "), None)

    def atoms(self):
        return self.__atoms

    def __str__(self):
        return f"{self.res_name} {self.res_seq}{self.i_code}"

@dataclass
class Atom:
    serial: int
    name: str
    x: float
    y: float
    z: float

    def distance_to(self, a):
        d2 = (self.x - a.x) * (self.x - a.x)
        d2 += (self.y - a.y) * (self.y - a.y)
        return math.sqrt(d2 + (self.z - a.z) * (self.z - a.z))

def read_pdb_content(code, input_lines):

    strctr = Structure(code)

    for line in input_lines:
        if not line.startswith("ATOM") and not line.startswith("HETAT"): continue
        name = line[12:16]
        serial = int(line[6:11].strip())
        res_name = line[17:20]
        res_seq = int(line[22:26].strip())
        i_code = line[26]
        chain_id = line[21]
        x = float(line[30:38].strip())
        y = float(line[38:46].strip())
        z = float(line[46:54].strip())
        chain = strctr.get_chain(chain_id)
        if chain is None:
            chain = Chain(chain_id)
            strctr.add_chain(chain)
        resid = chain.get_residue(res_seq, i_code)
        if resid is None:
            resid = Residue(res_name, res_seq, i_code)
            chain.add_residue(resid)
        atom = Atom(serial, name, x, y, z)
        resid.add_atom(atom)

    return strctr


if __name__ == "__main__":
    strctr = read_pdb_content("2GB1",open("2gb1.pdb"))
    for c in strctr.chains():
        print(c.code)

    chain_A = strctr.get_chain("A")
    for ri, rj in itertools.combinations(chain_A.residues(), 2):
        print(ri, rj, ri.ca().distance_to(rj.ca()))
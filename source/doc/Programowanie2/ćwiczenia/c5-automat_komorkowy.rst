Automat komórkowy
-------------------------------

Zaimplementuj automat komórkowy, w którym aktywne komórki mogą się przemieszczać i podlegają regułom,
zdefiniowanym przez użytkownika. Zaimeplementowana hierarchia klas powinna umożliwić symulację
`Gry w Życie <https://pl.wikipedia.org/wiki/Gra_w_%C5%BCycie>`__,
`Mrówki Langtona <https://pl.wikipedia.org/wiki/Mr%C3%B3wka_Langtona>`__, czy ... pandemii wirusa.


Zaimplementuj w tym celu następujące klasy:

  - ``World`` - reprezentującą planszę na której toczy się gra
  - ``Cell`` - pojedyncza komórka
  - ``Fate`` - enum definiujący los komórki
  - ``Rule`` - reprezentujaca pojedynczą regułę, sterującą zachowaniem komórki, np podział lub jej śmierć
  - ``Condition`` - reguła rządząca światem, mająca wpływ na los komórki

.. image:: ../../../_static/AgentSimulation.png
  :width: 600
  :alt: class inheritance diagram

Program główny symulacji powinien wyglądać mniej więcej tak:

.. code-block:: python

    max_iter = 1000
    w = World(100,100, n_cells = 800)
    for i in range(max_iter):
        for a in w.cells():
            a.apply_rules()
            w.apply_condition(a)

Strategia (*Strategy Pattern*)
-------------------------------------

.. note::
    Strategia umożliwia zmodyfikowanie działania obiektu już po jego utworzeniu.

Wzorzec ten wykorzystuje obiekty jako swego rodzaju "wtyczki". W poniższym przykładzie wzorzec tem modyfikuje algorytm
wykorzystywany do szyfrowania wiadomości:


.. raw:: html

  <div id="szyfry"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw3 = ScriptWidget("szyfry.py","szyfry", height=370, console_height=80, name="__main__", alignment="top-bottom")
  </script>

Wzorzec ten wymaga napisania nadrzędnej klasy, która odpowiedzialna jest za wykonywanie poleconych zadań
(często określanej w literaturze jako ``Context``); w powyższy przypadku tę rolę pełni ``Encryptor``. Klasa ta zawiera
(jako pole) obiekt, który definiuje *strategię*; tu poszczególne strategie to algorytmy szyfrujące. Dziedziczą
one po klasie bazowej ``EncryptionEngine``. ``Encryptor`` umożliwia dynamiczną zmianę strategii - w trakcie działania
programu.

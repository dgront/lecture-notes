class StaticR :

    r = 50
    __slots__ = [ 'x', 'y' ] 
    def __init__(self,x,y) :
        self.x = x
        self.y = y
    
    @staticmethod
    def scale_by(factor) :
        StaticR.r *= factor

c1 = StaticR(1,2)
c2 = StaticR(3,4)
#c1.r = 20      # This line should fail !!!
print(c2.r)
print(StaticR.r)
StaticR.scale_by(1.2)
print(c1.r)

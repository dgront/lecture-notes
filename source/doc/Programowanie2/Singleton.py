class Singleton :

    __instance = None

    @staticmethod
    def get_instance():

      """ Static access method. """
      if Singleton.__instance == None:
         Singleton()
      return Singleton.__instance

    def __init__(self) :
        if Singleton.__instance == None :
            Singleton.__instance = self
        else :
          raise Exception("This class is a singleton!")

s = Singleton()
print(s)

s = Singleton.get_instance()
print(s)

s = Singleton()
print(s)


.. _dyspozytor:

Dyspozytor (*Dispatch Pattern*)
-------------------------------------

.. note::
   W ogólnym ujęciu dyspozytor pozwala przypisywać akcje (funkcje, metody) zmiennym o różych wartościach bądź
   różnych typów.


Co oznacza *"przypisywać akcje zmiennym  o różnych wartościach"*? Na przykład w grze terenowej możliwych jest 8 kierunków
ruchu gracza:

.. raw:: html

  <div id="move_by_direction"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw1 = ScriptWidget("move_by_direction.py","move_by_direction", height=250)
  </script>

W powyższym przykładzie wpisałem tylko 4 możliwości (północ, południe, wschód, zachód); dodanie pozostały 4 (NE, SW itd)
wymagać będzie dodanie kolejnych czterech instrukcji ``elif``. Aby uniknąć problemów w przyszłości, warto już teraz
zaimplementować  opcję WS (*west-south*), która będzie robiła to samo, co SW. Liczba linii ``elif``,
które trzeba by napisać szybko rośnie ...

Dyspozytor to rozwiązanie, w którym akcje (w powyższym przykładzie: dodawanie 1 do x lub y) przechowujemy w słowniku.
Kluczami do słownika są wartości zmiennej sterującej (w powyższym przykładzie: napisy "N", "SW" itp). Rozwiązanie to
przedstawiono w poniższym przykładzie.

.. raw:: html

  <div id="dispatch_by_direction"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw2 = ScriptWidget("dispatch_by_direction.py","dispatch_by_direction", height=250)
  </script>

Wykorzystanie dyspozytora wymaga napisania obiektów - po jednym na każdą akcję. Ma on jednak kilka poważnych zalet:

 - dodawanie nowych akcji nie wymaga modyfikowania bloku ``if ... elif`` a jedynie napisania oddzielnej klasy i dodanie
   jej instacji do słownika; podobnie modyfikacja pojedynczej akcji wymaga zmodyfikowania klasy tej akcji
 - łatwo możemy rozszerzać akcje, rozbudowując kod odpowiednich ``__call__()``.

.. rubric:: Kierowanie wg typu - przeciążanie w Pythonie

W pogramowaniu obiektowym często pojawia się też konieczność różnego potraktowania obiektów o różnych typach.
Najlepiej z punktu widzenia projektowania i programowania było by, gdyby wszystkie różne rodzaje argumentów
przetwarzane były tą samą metodą albo co najmniej - metodami o tej samej nazwie. Ta druga możliwość dostępna
jest w niektórych językach pogramowania jako przeciążanie (*overloading*) metod.


.. code-block:: C++
  :linenos:

  double min(double a, double b) { return (a<b) ? a : b; }
  double min(double *a, int n) { ... }

W powyższym przykładzie wywołanie ``min(1, 3)`` wywoła pierwszą z tych metod, a ``min(1.0, 3.0)``  - drugą.
W Pythonie mechanizm ten niestety nie jest dostępny. Można jednak dynamicznie sprawdzać typ argumentu metody,
przeciążenie można więc zastąpić połączeniem wielu warunków. Przydają się też argumenty ``*args``:

.. literalinclude:: overloading.py
  :language: python
  :linenos:

*Przeciążenie* wykorzystane zostało w powyższym przykładzie w dwóch miejscach: do rysowania obiektów (format SVG) oraz do dodwania wierzchołków wielokąta.
Zauważ, że ponieważ klasa ``Polygon`` dziedziczy po klasie ``Circle``, obiekt klasy Polygon *jest również obiektem typu Circle*
Obiekt klast Circle jednak nie jest Polygon'em. Dlatego najpierw sprawdzamy, czy argument ``o`` to wielokąt (linia26)
a dopiero potem czy jest to koło (linia 30).

Instrukcja warunkowa jest mało elastyczna a na dodatek wraz z rosnącą liczbą możliwości do wyboru kod szybko staje się mało czytelny.
Spróbujmy napisać grę w papier, kamień i nożyce:

.. literalinclude:: paper_rock_scissors.py
  :language: python
  :linenos:

Funkcja ``play()`` jest dwuargumentowa a każdy z argumentów może być jednego z trzech typów. Mamy zatem już 9 kombinacji.
Dodanie czwartego typu argumentów zwiększyłoby liczbę kombinacji do 16. Kod ten może być nieco czytelniejszy, jeżeli zastosujemy
**dynamic dispatch**, jak w programie poniżej:

.. literalinclude:: paper_rock_scissors2.py
  :language: python
  :linenos:

Oczywiście wciąż musimy wpisać owe 9 możliwych wyników gry. Słownik poprawia jednak czytelność kodu i ułatwia dodawanie nowych wariantów.

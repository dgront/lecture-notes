import copy

from Vec2 import Vec2


class Atom:

    __slots__ =['__mass','__v','__r','__pos','__id']

    def __init__(self, m, r, x):
        self.__mass = m
        self.__v = Vec2(0,0)
        self.__r = r
        self.__pos = copy.deepcopy(x)
        self.__id = 0

    @property
    def m(self): return self.__mass

    @m.setter
    def m(self, new_mass): self.__mass = new_mass

    @property
    def r(self): return self.__r

    @r.setter
    def r(self, new_r): self.__r = new_r

    @property
    def pos(self): return self.__pos

    @property
    def v(self): return self.__v

    @property
    def id(self): return self.__id

    @id.setter
    def id(self, new_id): self.__id = new_id

    def __str__(self):
        return "%3d %6.2f %6.2f  %.1f" % (self.id, self.pos.x, self.pos.y, self.r)

    def name(self): return "Atom"


class HeavyAtom(Atom):

    def __init__(self, r, x):
        super().__init__(0, r, x)

    def name(self): return "HeavyAtom"


class Polygon(Atom):

    def __init__(self, m, nodes):
        super().__init__(m, 0, Vec2(0, 0))
        self.__nodes = []
        for n in nodes:
            if isinstance(n, list): self.__nodes.append(Vec2(n[0], n[1]))
            else: self.__nodes.append(Vec2(n.x, n.y))
            self.pos.x += self.__nodes[-1].x
            self.pos.y += self.__nodes[-1].y
        self.pos.x /= float(len(nodes))
        self.pos.y /= float(len(nodes))

    @property
    def nodes(self): return self.__nodes

    def name(self): return "Polygon"

class Segment:

    def __init__(self, begin, end):
        self.begin = begin
        self.end = end
        self.__id = 0

    def name(self): return "Segment"

    @property
    def id(self): return self.__id

    @id.setter
    def id(self, new_id): self.__id = new_id

    def __str__(self):
        return "seg %3d %6.2f %6.2f  -> %6.2f %6.2f" % (self.id, self.begin.x, self.begin.y, self.end.x, self.end.y)


if __name__ == "__main__" :

    nodes = [Vec2(0, 0), Vec2(1, 0), Vec2(1,1), Vec2(0, 1)]
    p = Polygon(1.0, nodes, 0)
    print(p)
    p = Polygon(1.0, [[0, 0], [1, 0], [1, 1], [0, 1]], 0)
    print(p)


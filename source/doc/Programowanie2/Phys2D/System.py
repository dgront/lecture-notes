from random import random
from Atom import Atom, Polygon, Segment, HeavyAtom


class System:

    def __init__(self):
        self.__atoms = []
        self.__heavy_atoms = []
        self.__polygons = []
        self.__segments = []
        self.__scene = []
        self.__cnt = 0
        self.__add_dispatch = {Atom:self.__atoms, HeavyAtom: self.__heavy_atoms,
                               Segment:self.__segments, Polygon: self.__polygons}

    def add(self, o):

        if isinstance(o, list):
            for oi in o:
                self.__add_dispatch[oi.__class__].append(oi)
                self.__cnt += 1
                oi.id = self.__cnt
        else:
            self.__add_dispatch[o.__class__].append(o)
            self.__cnt += 1
            o.id = self.__cnt

    @property
    def atoms(self): return self.__atoms

    @property
    def segments(self): return self.__segments

    @property
    def heavy_atoms(self): return self.__heavy_atoms

    def random_velocities(self, v_max):
        for a in self.__atoms :
            a.v.x = (1.0 - 2.0 * random()) * v_max
            a.v.y = (1.0 - 2.0 * random()) * v_max



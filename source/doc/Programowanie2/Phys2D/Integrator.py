from Vec2 import *


class Integrator:

    def __init__(self, system):
        self.__system = system

    def run(self, dt, n_steps):

        for i in range(n_steps):
            for a in self.__system.atoms:
                a.pos.x += a.v.x * dt
                a.pos.y += a.v.y * dt

            self.__circle_circle_collision()
            self.__circle_heavycircle_collision()
            self.__circle_segment_collision()

    def __circle_heavycircle_collision(self):

        for c in self.__system.atoms:
            for obstacle in self.__system.heavy_atoms:
                r = c.r + obstacle.r
                d2 = c.pos.distance_square_to(obstacle.pos)
                if d2 > r*r: continue

                xab = vector_sub(c.pos, obstacle.pos) # vector from A circle to B
                vab = c.v
                dp = vab.dot_product(xab)
                if dp > 0 : continue
                f = 2.0 * dp / d2
                c.v.x -= xab.x * f
                c.v.y -= xab.y * f

    def __circle_circle_collision(self):

        for i1 in range(len(self.__system.atoms)):
            c1 = self.__system.atoms[i1]
            for i2 in range(i1):
                c2 = self.__system.atoms[i2]
                r = c1.r + c2.r
                d2 = c1.pos.distance_square_to(c2.pos)
                if d2 > r*r: continue

                xab = vector_sub(c1.pos, c2.pos) # vector from A circle to B
                vab = vector_sub(c1.v, c2.v)
                dp = vab.dot_product(xab)
                if dp > 0 : continue
                f = 2.0 / (c1.m+c2.m) * dp / d2
                c1.v.x -= xab.x * f * c2.m
                c1.v.y -= xab.y * f * c2.m
                c2.v.x += xab.x * f * c1.m
                c2.v.y += xab.y * f * c1.m

    def __circle_segment_collision(self):

        for c in self.__system.atoms:
            for seg in self.__system.segments:
                sfrom = seg.begin
                sto = seg.end
                t, d2 = point_on_segment_projection(sfrom, sto, c.pos)
                if d2 > c.r * c.r or t < 0 or t > 1: continue

                n = normal_towards(sfrom, sto, c.pos)   # Vector normal to the segment
                dp = n.dot_product(c.v)
                if dp > 0: continue                     # The circle c traveling apart the segment, not towards it
                cf = dp / n.length2()
                n *= cf                                 # now n is the velocity vector perpendicular (normal) to the segment
                w = vector_sub(c.v, n)                  # w is the velocity component parallel to the segment
                w -= n                                  # now w is the new velocity (after collision)
                c.v.x, c.v.y = w.x, w.y



import re
from random import random

from Atom import *
from System import *


class SquareMaker:

    def __init__(self, default_mass = 1):
        self.__match_width = re.compile("(?:w |width )(\d+\.*\d*)")
        self.__match_center = re.compile("at ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")
        self.__match_mass = re.compile("(?:m |mass )(\d+\.*\d*)")
        self.__default_mass = default_mass

    def __call__(self, line):
        grp = self.__match_mass.search(line)
        mass = grp[1] if grp else self.__default_mass
        grp = self.__match_width.search(line)
        if grp:
            w = float(grp[1])
        grp = self.__match_center.search(line)
        if grp:
            x, y = float(grp[1]), float(grp[2])
        else: x, y = 0, 0

        return Polygon(mass, [[x, y], [x + w, y], [x + w, y + w], [x, y + w]])


class CircleMaker:

    def __init__(self, default_mass = 1, default_radius = 10):
        self.__match_radius = re.compile("(?:r |radius )(\d+\.*\d*)")
        self.__match_mass = re.compile("(?:m |mass )(\d+\.*\d*)")
        self.__match_center = re.compile("at ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")
        self.__default_radius = default_radius
        self.__default_mass = default_mass

    def __call__(self, line):
        grp = self.__match_mass.search(line)
        mass = float(grp.group(1)) if grp else self.__default_mass
        grp = self.__match_radius.search(line)
        radius = float(grp.group(1)) if grp else self.__default_radius
        grp = self.__match_center.search(line)
        if grp:
            x, y = float(grp.group(1)), float(grp.group(2))
        else: x, y = 0, 0

        if line.startswith('fixed'):
            return HeavyAtom(radius, Vec2(x, y))
        else:
            return Atom(mass, radius, Vec2(x,y))


class RandomCircleMaker:

    def __init__(self, default_mass = 1, default_radius = 10):
        self.__match_radius = re.compile("(?:r |radius )(\d+\.*\d*)")
        self.__match_n_circles = re.compile("(\d+) circles")
        self.__match_mass = re.compile("(?:m |mass )(\d+\.*\d*)")
        self.__default_radius = default_radius
        self.__default_mass = default_mass
        self.__match_from = re.compile("from ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")
        self.__match_to = re.compile("to ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")

    def __call__(self, line):
        grp = self.__match_n_circles.search(line)
        if grp:
            n = int(grp.group(1))
        else: return []
        grp = self.__match_mass.search(line)
        mass = float(grp.group(1)) if grp else self.__default_mass
        grp = self.__match_radius.search(line)
        radius = float(grp.group(1)) if grp else self.__default_radius

        grp = self.__match_from.search(line)
        if grp :
            x1, y1 = float(grp.group(1)), float(grp.group(2))
        grp = self.__match_to.search(line)
        if grp :
            x2, y2 = float(grp.group(1)), float(grp.group(2))

        out = []
        for i in range(n):
            x = (x2-x1) * random() + x1
            y = (y2-y1) * random() + y1
            out.append(Atom(mass, radius, Vec2(x,y)))

        return out


class BoxMaker:

    def __init__(self):
        self.__match_from = re.compile("from ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")
        self.__match_to = re.compile("to ([+-]?\d+\.*\d*),* *([+-]?\d+\.*\d*)")

    def __call__(self, line):
        grp = self.__match_from.search(line)
        if grp :
            x1, y1 = float(grp.group(1)), float(grp.group(2))
        grp = self.__match_to.search(line)
        if grp :
            x2, y2 = float(grp.group(1)), float(grp.group(2))

        v1, v2, v3, v4 = Vec2(x1,y1), Vec2(x1,y2), Vec2(x2,y1), Vec2(x2,y2)

        return [Segment(v1, v2), Segment(v1, v3), Segment(v4, v2), Segment(v4, v3)]


make_circle = CircleMaker()
make_box = BoxMaker()
make_square = SquareMaker()
make_random = RandomCircleMaker()
dispatch = {"fixed-circle": make_circle, "circle": make_circle,
            "box": make_box, "square": make_square, "random" : make_random}


def build_scene(config_txt):

    s = System()
    for line in config_txt.split("\n"):
        line = line.strip()
        if len(line) == 0 or line[0] == '#': continue
        tokens = line.split(' ', 1)
        print(line)
        o = dispatch[tokens[0]](line)
        s.add(o)

    return s


if __name__ == "__main__":

    s = build_scene("""
    circle at 9,1 radius 1
    circle radius 1 at 3,1
    fixed-circle radius 2 at 5,5
    random 10 circles from 0,0 to 200,200 radius 0.5 mass 0.5
    box from 0,0 to 10,10""")
    s.random_velocities(5.0)


from math import sqrt

class Vec2:

    __slots__ = ["__x","__y"]

    def __init__(self, x=0.0, y=0.0):
        self.__x = x
        self.__y = y

    def __str__(self):
        return "["+str(self.__x)+","+str(self.__y)+"]"

    def __iadd__(self, other):
        self.__x += other.__x
        self.__y += other.__y
        return self

    def __isub__(self, other):
        self.__x -= other.__x
        self.__y -= other.__y
        return self

    def __imul__(self, var):
        self.__x *= var
        self.__y *= var
        return self

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self,new_x):
        self.__x = new_x

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, new_y):
        self.__y = new_y

    def dot_product(self, other):
        return self.__x * other.__x + self.__y * other.__y

    def cross_product(self, other):
        return self.__x * other.__y - self.__y * other.__x

    def normalize(self):
        self.__x /= self.length()
        self.__y /= self.length()

    def length2(self): return self.dot_product(self)

    def length(self): return sqrt(self.length2())

    def angle(self, other):
        return self.dot_product(other) / (self.length() * other.length())

    def distance_square_to(self, other):
        return (other.__x-self.__x)*(other.__x-self.__x)+(other.__y-self.__y)*(other.__y-self.__y)

    def distance_to(self,other):
        return sqrt(self.distance_square_to(other))


def vector_add(vi,vj) :
    v = Vec2(vi.x,vi.y)
    v += vj
    return v


def vector_sub(vi, vj):
    v = Vec2(vi.x, vi.y)
    v -= vj
    return v


def normal_towards(begin_segment, end_segment, point):

    v1 = Vec2(end_segment.x - begin_segment.x, end_segment.y - begin_segment.y)
    v2 = Vec2(point.x - begin_segment.x, point.y - begin_segment.y)
    cp = v1.cross_product(v2)
    if cp > 0: return Vec2(-v1.y, v1.x)
    return Vec2(v1.y, -v1.x)


def point_on_segment_projection(begin_segment, end_segment, point):

    """Returns t coordinate along the segment and the squared distance between the segment and the point"""

    px = end_segment.x - begin_segment.x
    py = end_segment.y - begin_segment.y
    norm = px*px + py*py

    t = ((point.x - begin_segment.x) * px + (point.y - begin_segment.y) * py) / norm
    x = begin_segment.x + t * px
    y = begin_segment.y + t * py

    dx = x - point.x
    dy = y - point.y

    return t, dx*dx + dy*dy


if __name__ == "__main__":

    v1 = Vec2(1, 2)
    v2 = Vec2(3, 4)
    print(vector_add(v1, v2))
    print(vector_sub(v1, v2))

    v3 = vector_sub(v2, v1)
    p = Vec2(1, 4)
    z = Vec2(0,0)
    print(v3)
    print(v3.cross_product(p))
    print(normal_towards(z, v3, p))

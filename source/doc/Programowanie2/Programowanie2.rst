.. _programowanie_2:

Programowane i projektowanie obiektowe
======================================

.. toctree::
   :maxdepth: 2
   
   1-Pytonowe_to_i_owo

.. toctree::
   :maxdepth: 2
   
   2-Klasy

.. toctree::
   :maxdepth: 2

   3-Dlaczego_obiektowo

.. toctree::
   :maxdepth: 2
   
   4-Python_i_obiektowosc

.. toctree::
   :maxdepth: 2
   
   Dziedziczenie

.. toctree::
   :maxdepth: 2

.. toctree::
   :maxdepth: 2

   6-Wzorce_projektowe


.. toctree::
   :maxdepth: 2

   Podsumowanie


Kilka przykładów
=====================================

Tu znajdziesz kilka przykładów - zadań wraz z rozwiązaniami i komentarzem. Mam nadzieję, że pomogą Ci w samodzielnej nauce programowania obiektowego

.. toctree::
   :maxdepth: 1

   przykłady/p1-obliczenia_statystyczne

.. toctree::
   :maxdepth: 1

   przykłady/energie/p5-energie

.. toctree::
   :maxdepth: 1

   przykłady/graf/p2-GrafJakoPrzyklad

.. toctree::
   :maxdepth: 1

   przykłady/p3-kształty


.. toctree::
   :maxdepth: 2

   przykłady/warcaby/p4-warcaby.rst


.. toctree::
   :maxdepth: 2

   przykłady/logo/logo.rst


Przykładowe zadania
=====================================

Ta sekcja zawiera zadania do samodzielnego rozwiązania.

.. toctree::
   :maxdepth: 2

   zadania/zdarzenia_w_mario.rst


Ćwiczenia I
=====================================

Poniższe zadania rozwiązywane są na ćwiczeniach w pierwszej połowie semestru.

.. toctree::
   :maxdepth: 2

   ćwiczenia/c1-wielomian
   ćwiczenia/c2-wiezy
   ćwiczenia/syntezator/syntezator
   ćwiczenia/iterator_po_atomach/iteratory

Ćwiczenia II
=====================================

W drugiej połowie semestru realizowany jest jeden z poniższych projektów

.. toctree::
   :maxdepth: 2

   ćwiczenia/c4-makao
   ćwiczenia/c5-automat_komorkowy
   ćwiczenia/c6-arcade
      
.. _Dlaczego_obiektowo:

Dlaczego programowanie obiektowe
----------------------------------


Po co stosować klasy? Czy nie wystaczyły by zmienne i funkcje? Programy, które powstają na początku nauki programowania
obiektowego są najczęściej dłuższe i bardziej skomplikowane niż analogiczny program złożony w funkcji.
Programy obiektowe mają jednak pewne ważne zalety


Kapsułkowanie
""""""""""""""
Poniższy kod to implementacja prostego generatora liczb losowych.
Z matematycznego punktu widzenia, generator taki to dość prosta funkcja wyrażona wzorem: :math:`x_i = ( a x_{i-1} + c ) mod m`, gdzie
:math:`a, c` oraz :math:`m` to pewne stałe a :math:`x_i` oraz :math:`x_{i-1}` to aktualna i poprzednia wartość losowa.
Zaimplementowanie tej funkcji wymaga przechowywania wartości z poprzedniego wywołania. Stosując klasę, możemy wygodnie przechowywać
:math:`x_{i-1}` jako pole, podobnie zresztą jak stałe :math:`a, c, m` (które jak widać nie dość, że są prywatne, to użytkownik nie może ich zmienić - brak funkcji ``set()``)

.. literalinclude:: Rnd.py
   :language: python
   :linenos:

Kolejny przykład, kiedy kapsułkowanie okazuje się być bardzo przydatne, to sortowanie obiektów, np listy punktów wg odległości od zadanego położenia.

.. raw:: html
   :file: sortable_circles.html


W trakcie sortowania wielokrotnie wykonywane jest przestawienie miejscami elementów. Położenia x, y można trzymać w oddzielnych tablicach (listach), wtedy:

.. code-block:: python
   :linenos:

   # Współrzędne trzymane w listach
   x, y, z = [rand() for i in range(10)], [rand() for i in range(10)], [rand() for i in range(10)]
   # zamienia wektory pod indeksami i oraz j
   tmp = x[i]
   x[i] = x[j]
   x[j] = tmp
   tmp = y[i]
   y[i] = y[j]
   y[j] = tmp
   tmp = z[i]
   z[i] = z[j]
   z[j] = tmp


a można zamieniać całe obiekty:

.. code-block:: python
   :linenos:

   # Lista wektorów
   v = [ Vec3(rand(),rand(),rand()) for i in range(10) ]
   # zamienia wektory pod indeksami i oraz j
   tmp = v[i]
   v[i] = v[j]
   v[j] = tmp

W Pythonie można osiągnąć to jeszcze prościej:

.. code-block:: python
   :linenos:

   # Lista wektorów
   v = [ Vec3(rand(),rand(),rand()) for i in range(10) ]
   # zamienia wektory pod indeksami i oraz j
   v[i], v[j] = v[j], v[i]

W praktyce oczywiście wykorzytujemy biblioteczną funkcję ``sort`` lub ``lista.sorted()``, która dokonuje takich przestawień.




.. _Pytonowa_obiektowosc:

Python i obiektowość
------------------------------


Język Python ma swoje własne podejście do obiektowości. Znajdziemy w nim rozwiązania niespotykane w innych językach,
jak np. lista ``__slots__``. Inne zaś, jak na przykład znane z języka C++ czy JAVA przeciążanie operatorów,
rozwiązuje się w *magiczny* sposób.

.. _slots:

Lista ``__slots__``
^^^^^^^^^^^^^^^^^^^^^

Od wersji 3.3 Pythona w każdej klasie można stworzyć listę ``__slots__``. Lista ta definiuje,
jakie pola może mieć dana klasa. Próba stworzenia pola o innej nazwie nie powiedzie się:

.. literalinclude:: slots.py
   :language: python
   :linenos:


.. _magic_functions:

Funkcje specjalne
^^^^^^^^^^^^^^^^^^^^^

Każdy obiekt w Pythonie ma zdefiniowane pewne funkcje niejako *"z definicji"*. Nazwy tych funkcji  zaczynają
i kończą się na ``__`` (*magic functions*) i mają specjalne znaczenie. Zerknijmy na taki przykład:

.. code-block:: python

    x = 5
    print(x)
    
    v = Vec3(1.2, 3.9, 3.4)
    print(v)

Dlaczego wydruk obiektu ``Vec3`` wygląda mało przekonująco? Aby wyglądał inaczej, trzeba przedefiniować funkcję ``__str__()``

.. literalinclude:: str.py
   :language: python
   :linenos:

Inną ciekawą możliwością jest przedefiniowanie operatorów matematycznych, takich jak ``+``,  ``*`` czy  ``+=``. Umożliwia to *"naturalny"* zapis opercji matematycznych na zdefiniowanych przez siebie obiektach, jak np. poniżej na liczbach zespolonych:

.. literalinclude:: Complex.py
   :language: python
   :linenos:


Kolejnym operatorem wartym uwagi jest operator wywołania (*call operator*). Jego nadpisanie umożliwia wykorzystanie obiektu w dokładnie ten sposób jak funkcja.

.. literalinclude:: programy/Call.py
   :language: python
   :linenos:

Przykładów zastosowania jest mnóstwo; weźmy chociażby pole siłowe dynamiki molekularnej:

.. literalinclude:: LJ.py
   :language: python
   :linenos:

Metody i pola statyczne
^^^^^^^^^^^^^^^^^^^^^^^^^

Funkcje (i pola) statyczne istnieją nawet wtedy, kiedy nie utworzono ani jednego obiektu danej klasy. Jeżeli jednak obiekt już jest, to mają do niego pełen dostęp - jako pełnoprawny członek klasy. Zauważ, że składowe statyczne również mogą być prywatne lub publiczne.

   - w JAVA są konieczne: nie ma innej możliwości zdefiniowania funkcji
   - w Pythonie, C++ : ułatwiają organizację kodu.

Statyczne pole klasy w Pythonie implementujemy następująco:

.. literalinclude:: Static.py
   :language: python
   :linenos:

.. _klasa_Vec3:

Zamiast podsumowania
^^^^^^^^^^^^^^^^^^^^^

.. literalinclude:: programy/Vec3.py
   :language: python
   :linenos:




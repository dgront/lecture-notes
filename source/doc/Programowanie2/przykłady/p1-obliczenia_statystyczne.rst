Prosta hierarchia klas: obliczenia statystyczne
++++++++++++++++++++++++++++++++++++++++++++++++

W poniższym przykładzie powstanie kilka klas, umożliwiających obliczenia statystyczne na kolumnie liczb.
Każda z klas ma jasno sprecyzowany cel, a funkcjonalność, którą dostarcza, zaimplementowana jest w operatorze wywołania
(*call operator*). Dodatkowo każda z tych klas umie się przedstawić (metoda ``name()``)

Klasy dziedziczą po abstrakcyjnej klasie bazowej ``AbstractStatistCalculator``, która definiuje metody,
zaimplementowane w klasach potomnych. Najciekawszym przypadkiem jest klasa ``SumVariance``, która zarówno zawiera
obiekty klasy ``AbstractStatistCalculator`` jak i sama po niej dziedziczy. Również sama nie wykonuje żadnych obliczeń.
Wymaganą funkcjonalność zapewnia, wywołując odpowiednie metody ze składowych obiektów (delegacje).


.. literalinclude:: statistics.py
   :language: python
   :linenos:



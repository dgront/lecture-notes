from __future__ import annotations

import random
from abc import ABC, abstractmethod
from enum import Enum


class Color(Enum):
    BLACK = 1
    WHITE = 2

    @staticmethod
    def enemy_color(col):
        return Color.WHITE if col==Color.BLACK else Color.BLACK

class Direction:
    def __init__(self,dr,dc):
        self.__dr = dr
        self.__dc = dc

    @property
    def dr(self): return self.__dr

    @property
    def dc(self): return self.__dc

Direction.UR = Direction(1,1)
Direction.UL = Direction(1,-1)
Direction.BR = Direction(-1,1)
Direction.BL = Direction(-1,-1)


class Location:

    __letters = list("_ABCDEFGHIJKLMNOP")
    board_size = 8

    def __init__(self, row: int, col: int):
        self.__row, self.__col = row, col

    def __str__(self):
        return "%c%d" % (Location.__letters[self.__row], self.__col)

    @property
    def row(self) -> int: return self.__row

    @property
    def col(self) -> int: return self.__col

    def move_by(self, d: Direction, n: int = 1):
        return self.__move_by_vector(d.dr * n, d.dc * n)

    def __move_by_vector(self, dr, dc):
        r, c = self.__row + dr, self.__col + dc
        if any([r < 1, r > Location.board_size, c < 1, c > Location.board_size]): return None
        return Location(r, c)


class PieceBase(ABC):
    def __init__(self, color:Color):
        self.__color = color

    @property
    def color(self) -> Color: return self.__color

    @abstractmethod
    def move_proposals(self, loc:Location, brd): pass

    @abstractmethod
    def symbol(self): pass


class Move:
    def __init__(self, start: Location, stop: Location, captured: Location = None):
        self.__start = start
        self.__stop = stop
        self.__captured = captured

    @property
    def start(self): return self.__start

    @property
    def stop(self): return self.__stop

    @property
    def captured(self): return self.__captured

    def __str__(self):
        return str(self.__start)+" -> "+str(self.__stop)

class Piece(PieceBase):
    def __init__(self, color: Color):
        super().__init__(color)

    def symbol(self):
        return '*' if self.color==Color.BLACK else 'o'

    def move_proposals(self, loc: Location, brd) -> list[Move]:

        moves = []
        if self.color == Color.BLACK:
            dirs = [Direction.BL, Direction.BR]
        else:
            dirs = [Direction.UL, Direction.UR]

        for d in dirs:
            if l:=loc.move_by(d):                    # --- l==None means the move went outside the board
                if brd[l] is None: moves.append(Move(loc, l))
                elif brd[l].color == Color.enemy_color(self.color):
                    if ll := l.move_by(d):
                        if brd[ll] is None:
                            moves.append(Move(loc, ll, l))
        return moves


class Queen(PieceBase):
    def __init__(self, color: Color):
        super().__init__(color)

    def symbol(self):
        return 'X' if self.color==Color.BLACK else 'O'

    def move_proposals(self, loc: Location, brd):

        moves = []
        dirs = [Direction.BL, Direction.BR, Direction.UL, Direction.UR]

        for d in dirs:
            for i in range(1,Location.board_size):
                l = loc.move_by(d, i)
                if l is None: break             # --- None means the move went outside the board
                if brd[l] is None:
                    moves.append(Move(loc, l))
                elif brd[l].color == Color.enemy_color(self.color):
                    if ll := l.move_by(d):
                        if brd[ll] is None:
                            moves.append(Move(loc, ll, l))
                else:
                    break

        return moves


class Board:
    def __init__(self, size=8, n_pieces=12):
        # should check if size is even!
        Location.board_size = size
        self.__size = size
        self.__b = [[None for _ in range(size+1)] for _ in range(size+1)]
        blacks = self.locations(Color.BLACK)
        if n_pieces > 0:
            for pos in blacks[:n_pieces]:
                self[pos] = Piece(Color.WHITE)

            for pos in blacks[-n_pieces:]:
                self[pos] = Piece(Color.BLACK)

    def __getitem__(self, loc:Location):
        return self.__b[loc.row][loc.col]

    def __setitem__(self, loc:Location, item: Piece):
        self.__b[loc.row][loc.col] = item

    def pieces(self, color:Color) -> list[tuple[Location, PieceBase]]:
        ret = []
        for r in range(1, self.__size+1):
            for c in range(1, self.__size + 1):
                piece = self.__b[r][c]
                if piece is not None and piece.color == color:
                    ret.append((Location(r, c), piece))
        return ret

    def locations(self, color: Color):
        all_loc = []
        for row in range(1, self.__size + 1):
            for col in range(1, self.__size + 1):
                if l := Location(row, col):
                    if Board.color(l) == color:
                        all_loc.append(l)
        return all_loc

    def make_move(self, m:Move):
        piece: PieceBase = self[m.start]
        self[m.start] = None
        self[m.stop] = piece
        if piece.color == Color.BLACK and m.stop.row == 1:
            self[m.stop] = Queen(Color.BLACK)
        elif piece.color == Color.WHITE and m.stop.row == self.__size:
            self[m.stop] = Queen(Color.WHITE)
        if m.captured is not None:
            self[m.captured]=None

    @staticmethod
    def color(loc:Location):
        if (loc.col + loc.row) % 2 == 0:
            return Color.BLACK
        else:
            return Color.WHITE

    def __str__(self):
        s = ""
        for r in range(1, self.__size+1):
            s += Location.letters[r] + " |"
            for c in range(1, self.__size + 1):
                if self.__b[r][c] == None: s += " |"
                else:
                    s += self.__b[r][c].symbol()+"|"
            s += "\n"
        s += "   "
        for c in range(1, self.__size + 1):
            s += str(c) + "|"
        return s + "\n"


class Player:
    def __init__(self, color:Color):
        self.__my_color = color

    @property
    def color(self): return self.__my_color

    def move(self, board:Board):
        moves = self.move_proposals(board)
        if len(moves) == 0: return None
        # --- check for capturing moves
        capt = [m for m in moves if m.captured is not None]
        if len(capt) != 0:
            return random.choice(capt)
        else:
            return random.choice(moves)

    def move_proposals(self, board:Board) -> list[Move]:
        pcs = board.pieces(self.color)
        moves = []
        for loc, p in pcs:
            moves.extend(p.move_proposals(loc, board))
        return moves


if __name__ == "__main__":

    # random.seed(1)
    board = Board(8, 12)
    players = (Player(Color.WHITE), Player(Color.BLACK))

    i = 0
    while True:
        the_player = players[i % 2]
        move = the_player.move(board)
        if move is None:
            print("Player", the_player.color, "won", "after", i + 1, "moves")
            print(board)
            break
        else:
            board.make_move(move)
            i += 1

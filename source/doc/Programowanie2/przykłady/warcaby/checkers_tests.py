import unittest

# from checkers import *
from checkers2 import *

board8x8 = """|o| |o| |o| |o| |
| |o| |o| |o| |o|
|o| |o| |o| |o| |
| | | | | | | | |
| | | | | | | | |
| |*| |*| |*| |*|
|*| |*| |*| |*| |
| |*| |*| |*| |*|
"""

board6x6 = """|o| |o| |o| |
| |o| |o| |o|
| | | | | | |
| | | | | | |
|*| |*| |*| |
| |*| |*| |*|
"""

board8x8_empty = """| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
"""

class TestBoard(unittest.TestCase):

    def setUp(self):
        pass

    def test_initial_setup(self):
        board = Board()
        self.assertEqual(board8x8, str(board))
        board = Board(6,6)
        self.assertEqual(board6x6, str(board))
        # --- test empty board
        board = Board(8,0)
        self.assertEqual(board8x8_empty, str(board))


class TestLocation(unittest.TestCase):

    def test_few_locations(self):
        a1 = Location(1,1)
        self.assertEqual("A1", str(a1))
        self.assertEqual("B2", str(a1.upper_right()))
        self.assertEqual(None, a1.upper_left())
        self.assertEqual(None, a1.bottom_left())
        self.assertEqual(None, a1.bottom_right())
        g4 = Location(7,4)
        self.assertEqual("G4", str(g4))
        self.assertEqual("H5", str(g4.upper_right()))
        self.assertEqual("H3", str(g4.upper_left()))
        self.assertEqual("F3", str(g4.bottom_left()))
        self.assertEqual("F5", str(g4.bottom_right()))

class TestPiece(unittest.TestCase):

    def test_piece_moves(self):
        brd = Board(8,0)
        p = Piece(Color.WHITE)
        # --- test moves from A1 (should be only B2)
        moves = p.move_proposals(Location(1,1), brd)
        self.assertEqual(1, len(moves))
        self.assertEqual("A1 -> B2", str(moves[0]))
        # --- test moves from B2 (should be only C1 and C3)
        moves = p.move_proposals(Location(2,2), brd)
        self.assertEqual(2, len(moves))
        self.assertEqual("B2 -> C1", str(moves[0]))
        self.assertEqual("B2 -> C3", str(moves[1]))
        # --- try a capturing move
        brd[Location(3,3)] = Piece(Color.BLACK)
        moves = p.move_proposals(Location(2,2), brd)
        self.assertEqual(2, len(moves))
        self.assertEqual("B2 -> C1", str(moves[0]))
        self.assertEqual("B2 -> D4", str(moves[1]))
        # --- two capturing moves
        brd[Location(3,3)] = Piece(Color.BLACK)
        brd[Location(3,5)] = Piece(Color.BLACK)
        moves = p.move_proposals(Location(2,4), brd)
        self.assertEqual(2, len(moves))
        self.assertEqual("B4 -> D2", str(moves[0]))
        self.assertEqual("B4 -> D6", str(moves[1]))

if __name__ == '__main__':
        unittest.main()
from __future__ import annotations

import random
from abc import ABC, abstractmethod
from enum import Enum


class Color(Enum):
    BLACK = 1
    WHITE = 2

class Location:

    letters = list("_ABCDEFGHIJKLMNOP")
    board_size = 8

    def __init__(self, row:int, col:int):
        self.__row = row
        self.__col = col

    def __str__(self):
        return"%c%d" % (Location.letters[self.__row],self.__col)

    @property
    def row(self): return self.__row

    @property
    def col(self): return self.__col

    def upper_left(self) -> Location:
        return self.__move_by_vec(1,-1)

    def upper_right(self) -> Location:
        return self.__move_by_vec(1,1)

    def bottom_left(self) -> Location:
        return self.__move_by_vec(-1, -1)

    def bottom_right(self) -> Location:
        return self.__move_by_vec(-1, 1)

    def __move_by_vec(self, dr, dc):
        r, c = self.__row + dr, self.__col + dc
        if any([r < 1, c < 1, r > Location.board_size, c > Location.board_size]): return None
        return Location(r,c)

class Direction:
    def __init__(self,dr,dc):
        self.__dr = dr
        self.__dc = dc

    @property
    def dr(self): return self.__dr

    @property
    def dc(self): return self.__dc

Direction.UR = Direction(1,1)
Direction.UL = Direction(1,-1)
Direction.BR = Direction(-1,1)
Direction.BL = Direction(-1,-1)

class Move:
    def __init__(self, start:Location, stop:Location, captured:Location=None):
        self.start = start
        self.stop = stop
        self.captured = captured


class AbstractPiece(ABC):

    def __init__(self, color:Color):
        self.__color = color

    @property
    def color(self): return self.__color

    @abstractmethod
    def move_proposals(self, loc:Location, brd:Board): pass

class Piece(AbstractPiece):
    def __init__(self, color: Color):
        super().__init__(color)

    def move_proposals(self, loc:Location, brd: Board):
        moves = []
        # regular moves - diagonally by one
        if self.color == Color.BLACK:
            if l := loc.bottom_left():
                if brd[l] is None: moves.append(Move(loc, l))
                elif brd[l].color == Color.WHITE:
                    if ll := l.bottom_left():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
            if l := loc.bottom_right():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.WHITE:
                    if ll := l.bottom_right():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
        else:
            if l := loc.upper_left():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.BLACK:
                    if ll := l.upper_left():
                        if brd[ll] is None:
                            moves.append(Move(loc, ll, l))
            if l := loc.upper_right():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.BLACK:
                    if ll := l.upper_right():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
        return moves

class Queen(AbstractPiece):
    def __init__(self, color: Color):
        super().__init__(color)

class Player:
    def __init__(self, color: Color):
        self.__color = color

    @property
    def color(self) -> Color: return self.__color

    def move(self, brd:Board):
        my_pieces = brd.pieces(self.color)
        moves = []
        for loc, p in my_pieces:
            moves.extend(p.move_proposals(loc, brd))
        if len(moves) == 0: return None
        return random.choice(moves)


class Board:
    def __init__(self, size=8, n_pieces=12):
        self.__b = [[None for _ in range(size+1)] for _ in range(size+1)]
        Location.board_size = size
        blacks = self.locations(Color.BLACK)
        # n_pieces = self.__size//2*3
        if n_pieces > 0:
            for pos in blacks[:n_pieces]:
                self[pos] = Piece(Color.WHITE)

            for pos in blacks[-n_pieces:]:
                self[pos] = Piece(Color.BLACK)

    def pieces(self, color:Color) -> list[tuple(Location,Piece)]:
        ret = []
        for r in range(1, Location.board_size + 1):
            for c in range(1, Location.board_size + 1):
                piece = self.__b[r][c]
                if piece is not None and piece.color == color:
                    ret.append((Location(r, c), piece))
        return ret

    def make_move(self, m:Move):
        piece = self[m.start]
        self[m.start] = None
        self[m.stop] = piece
        if m.captured is not None:
            self[m.captured]=None

    def __str__(self):
        s = ""
        for r in range(1, Location.board_size+1):
            s += "|"
            for c in range(1, Location.board_size + 1):
                if self.__b[r][c] == None: s += " |"
                else:
                    if self.__b[r][c].color == Color.BLACK:
                        s += "*|"
                    else:
                        s += "o|"
            s += "\n"
        return s

    def locations(self, color: Color):
        all_loc = []
        for row in range(1, Location.board_size + 1):
            for col in range(1, Location.board_size + 1):
                if l := Location(row, col):
                    if Board.color(l) == color:
                        all_loc.append(l)
        return all_loc

    @staticmethod
    def color(loc:Location):
        if (loc.col + loc.row) % 2 == 0:
            return Color.BLACK
        else:
            return Color.WHITE

    def __getitem__(self, loc:Location):
        return self.__b[loc.row][loc.col]

    def __setitem__(self, loc:Location, item: AbstractPiece):
        self.__b[loc.row][loc.col] = item


if __name__ == "__main__":

    board = Board()
    board[Location(2,2)] = Piece(Color.WHITE)
    print(board)
    l=Location(1,1)
    print(l,l.row,l.col)
    p = Piece(Color.WHITE)

    whites = Player(Color.WHITE)
    blacks = Player(Color.BLACK)
    players = [whites, blacks]

    i = 0
    while True:
        move = players[i % 2].move(board)
        if move is None:
            print("Player", players[(i+1) % 2].color, "won!")
            print(board)
            break
        else:
            board.make_move(move)
        i+=1
from __future__ import annotations

from browser import document, timer
from checkers2 import *
from visualife.core import HtmlViewport


class CheckersGui:

    def __init__(self, width:float, board:Board, viewport: HtmlViewport):
        self.__n = Location.board_size
        self.__side = width/self.__n - 5
        self.__viewport = viewport
        self.__board = board
        self.draw_checkerboard()
        self.__n_click = 0
        self.__move_start = None
        self.__move_end = None

    def draw_piece(self, loc:Location, is_queen):

        # callback to store location of the move starting point: cut the last two characters to handle both "A1" and "p-A1" cases
        def move_start(evt): self.__move_start = evt.target.id[-2:]

        pc = self.__board[loc]
        x = (loc.col - 0.5) * self.__side
        y = (loc.row - 0.5) * self.__side
        r = self.__side/2 - 3
        if pc.color == Color.WHITE:
            color = "white"
            stroke = "darkgray"
            self.__viewport.define_binding(str(loc), "click", move_start)
        else:
            stroke = "white"
            color = "darkgray"
        self.__viewport.circle(str(loc), x, y, r, fill=color, stroke_width="2", stroke=stroke)
        if is_queen:
            self.__viewport.circle("p-"+str(loc), x, y, r/3.0, fill=color, stroke_width="3", stroke="black")
            self.__viewport.define_binding("p-"+str(loc), "click", move_start)

    def draw_checkerboard(self):

        # callback to store location of the move ending location
        def move_end(evt): self.__move_end = evt.target.id[2:]

        self.__viewport.clear()

        # --- the board itself
        for loc in self.__board.locations(Color.BLACK):
            x, y = (loc.col - 1) * self.__side, (loc.row - 1) * self.__side
            name = "b-" + str(loc)          # Every ID of a board square is a location string preceded with "b-"
            self.__viewport.define_binding(name, "click", move_end)
            self.__viewport.rect(name, x, y, self.__side, self.__side, fill="black", stroke="black", stroke_width="1")

        for loc, pc in self.__board.pieces(Color.WHITE):
            self.draw_piece(loc, isinstance(pc, Queen))

        for loc, pc in self.__board.pieces(Color.BLACK):
            self.draw_piece(loc, isinstance(pc, Queen))

        self.__viewport.close()
        self.__viewport.apply_binding()

    def get_move(self):
        if self.__move_start is not None and self.__move_end is not None:
            out = (self.__move_start, self.__move_end)
            self.__move_start, self.__move_end = None, None
            return out          # return a move given as the two end points
        return None             # None means no move ready yet!

    def message(self, msg):
        document["msg"].innerHTML = msg


class HumanPlayer(Player):
    def __init__(self, color:Color, gui:CheckersGui):
        super().__init__(color)
        self.gui = gui
        self.__move_ready = False
        self.__gui_lock = None

    def move(self, board:Board):

        m = self.gui.get_move()
        if m is None: return []                     # Empty list means move is not yet ready!
        moves = self.move_proposals(board)
        # --- check for capturing moves; they are mandatory!
        capt = [m for m in moves if m.captured is not None]

        move_from, move_to = m                      # unpack the move since it's not empty
        for m in moves:
            if str(m.start) == move_from and str(m.stop) == move_to:
                if m.captured is None and len(capt) > 0:
                    self.gui.message("you have a capturing move!")
                    return []
                self.gui.message("")                # successful move clears a previous error message
                return m
        self.gui.message("illegal move!")
        return []                                   # Return "move not ready" signal


def play_with_gui(players: list[Player], board: Board, gui: CheckersGui):

    global loop_handler, n_moves

    the_player = players[n_moves % 2]
    move = the_player.move(board)
    if move is None:
        msg = "Player %s won after %d moves!" % (str(Color.enemy_color(the_player.color)), n_moves + 1)
        gui.message(msg)
        timer.clear_interval(loop_handler)
    elif isinstance(move, list) and len(move) == 0:     # --- GUI player not ready
        pass
    else:
        print("MOVING: ", move)
        board.make_move(move)
        n_moves += 1
        gui.draw_checkerboard()


board_types = {"6x6": (6, 6), "8x8": (8, 12), "10x10": (10, 15)}

def restart(evt=None):

    global loop_handler, drawing
    board_type = document["select_board"].get(selector='input[name="board"]:checked')[0].id
    size, n_pieces = board_types[board_type]
    if loop_handler is not None:
        timer.clear_interval(loop_handler)
    drawing.clear()
    board = Board(size, n_pieces)
    game = CheckersGui(500, board, drawing)
    players = (HumanPlayer(Color.WHITE, game), Player(Color.BLACK))
    loop_handler = timer.set_interval(play_with_gui, 300, players, board, game)

document["restart"].bind("click", restart)
loop_handler = None
n_moves = 0
drawing = HtmlViewport(document['drawing'], 500, 500)
restart()

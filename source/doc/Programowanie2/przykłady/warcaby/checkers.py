from __future__ import annotations

import random
from abc import ABC, abstractmethod
from enum import Enum
from typing import List, Tuple


class Color(Enum):
    BLACK = 1
    WHITE = 2

class Location:

    letters = list("_ABCDEFGHIJKLMNOP")
    board_size = 8

    def __init__(self, row: int, col: int):
        self.__row, self.__col = row, col

    def __str__(self):
        return "%c%d" % (Location.letters[self.__row], self.__col)

    @staticmethod
    def set_board_size(new_size):
        Location.board_size = new_size

    @property
    def row(self): return self.__row

    @property
    def col(self): return self.__col

    def upper_left(self) -> Location:
        return self.__move_by_vector(1, -1)

    def upper_right(self) -> Location:
        return self.__move_by_vector(1, 1)

    def bottom_left(self) -> Location:
        return self.__move_by_vector(-1, -1)

    def bottom_right(self) -> Location:
        return self.__move_by_vector(-1, 1)

    def __move_by_vector(self, dr, dc):
        r, c = self.__row + dr, self.__col + dc
        if any([r < 1, r > Location.board_size, c < 1, c > Location.board_size]): return None
        return Location(r, c)


class PieceBase(ABC):
    def __init__(self, color:Color):
        self.__color = color

    @abstractmethod
    def is_queen(self): pass

    @property
    def color(self) -> Color: return self.__color

    @abstractmethod
    def move_proposals(self, loc:Location, brd:Board): pass


class Move:
    def __init__(self, start: Location, stop: Location, captured: Location = None):
        self.__start = start
        self.__stop = stop
        self.__captured = captured

    @property
    def start(self): return self.__start

    @property
    def stop(self): return self.__stop

    @property
    def captured(self): return self.__captured

    def __str__(self):
        return str(self.__start)+" -> "+str(self.__stop)

class Piece(PieceBase):
    def __init__(self, color: Color):
        super().__init__(color)

    def is_queen(self): return False

    def move_proposals(self, loc: Location, brd: Board):
        moves = []
        # regular moves - diagonally by one
        if self.color == Color.BLACK:
            if l := loc.bottom_left():
                if brd[l] is None: moves.append(Move(loc, l))
                elif brd[l].color == Color.WHITE:
                    if ll := l.bottom_left():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
            if l := loc.bottom_right():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.WHITE:
                    if ll := l.bottom_right():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
        else:
            if l := loc.upper_left():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.BLACK:
                    if ll := l.upper_left():
                        if brd[ll] is None:
                            moves.append(Move(loc, ll, l))
            if l := loc.upper_right():
                if brd[l] is None: moves.append(Move(loc,l))
                elif brd[l].color == Color.BLACK:
                    if ll := l.upper_right():
                        if brd[ll] is None:
                            moves.append(Move(loc,ll,l))
        return moves


class Queen(PieceBase):
    def __init__(self, color: Color):
        super().__init__(color)

    def is_queen(self): return True

    def move_proposals(self, loc:Location, brd:Board):
        pass


class Board:
    def __init__(self, size=8, n_pieces=12):
        # should check if size is even!
        Location.set_board_size(size)
        self.__size = size
        self.__b = [[None for _ in range(size+1)] for _ in range(size+1)]
        blacks = self.locations(Color.BLACK)
        # n_pieces = self.__size//2*3
        if n_pieces > 0:
            for pos in blacks[:n_pieces]:
                self[pos] = Piece(Color.WHITE)

            for pos in blacks[-n_pieces:]:
                self[pos] = Piece(Color.BLACK)

    def set(self, loc: Location, piece:PieceBase):
        self.__b[loc.row][loc.col] = piece

    def __getitem__(self, loc:Location):
        return self.__b[loc.row][loc.col]

    def __setitem__(self, loc:Location, item: Piece):
        self.__b[loc.row][loc.col] = item

    def pieces(self, color:Color) -> List[Tuple[Location,Piece]]:
        ret = []
        for r in range(1, self.__size+1):
            for c in range(1, self.__size + 1):
                piece = self.__b[r][c]
                if piece is not None and piece.color == color:
                    ret.append((Location(r, c), piece))
        return ret

    def make_move(self, m:Move):
        piece = self[m.start]
        self[m.start] = None
        self[m.stop] = piece
        if m.captured is not None:
            self[m.captured]=None
        print(str(m))
        print(self)

    def __str__(self):
        s = ""
        for r in range(1, self.__size+1):
            s += "|"
            for c in range(1, self.__size + 1):
                if self.__b[r][c] == None: s += " |"
                else:
                    if self.__b[r][c].color == Color.BLACK:
                        s += "*|"
                    else:
                        s += "o|"
            s += "\n"
        return s

    @staticmethod
    def color(loc: Location):
        if (loc.col + loc.row) % 2 == 0:
            return Color.BLACK
        else:
            return Color.WHITE

    def locations(self, color: Color):
        all_loc = []
        for row in range(1, self.__size + 1):
            for col in range(1, self.__size + 1):
                if l := Location(row, col):
                    if Board.color(l) == color:
                        all_loc.append(l)
        return all_loc

class Player:
    def __init__(self, color:Color):
        self.__my_color = color

    @property
    def color(self): return self.__my_color

    def move(self, board:Board):
        pcs = board.pieces(self.color)
        moves = []
        for loc, p in pcs:
            moves.extend(p.move_proposals(loc, board))
        if len(moves) == 0: return None
        return random.choice(moves)

if __name__ == "__main__":

    random.seed(1)
    board = Board(6, 6)
    print(board)
    players = (Player(Color.WHITE), Player(Color.BLACK))

    i = 0
    while True:
        move = players[i % 2].propose_direction(board)
        if move is None:
            print("Player", players[i % 2].color, "won!")
            print(board)
            break
        else:
            board.make_move(move)
        i+=1


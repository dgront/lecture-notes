Polimorfizm : biblioteka kształtów
---------------------------------------

Polimorficzne zachowanie klas bardzo często tłumaczone jest na przykładzie hierarchii klas opisujących figury geometryczne: koła, kwadraty, itp.
Każda z klas umie narysować swój kształt - każda inny. W tym przykładzie kształty będą rysowane w tekstowym formacie SVG, np
poniżej mamy narysowane koło i kwadrat:


.. raw:: html

    <object data="../../../_static/draw.svg" type="image/svg+xml"></object>

.. literalinclude:: draw.svg
   :language: xml
   :linenos:

Poniżej zdefiniowano klasę bazową ``Shape`` oraz dwie klast potomne: koło i kwadrat. Każda z nich umie `sama się narysować`,
zwracając odpowiedni fragment tekstu w formacie SVG (metoda ``draw()``):

.. literalinclude:: kształty.py
   :language: python
   :linenos:
   :lines: 1-45, 62-72

Zauważ, że zdefiniowana klasa bazowa przechowuje położenie środka kształtu (współrzędne x, y).
Zdefiniowana w klasie bazowej metoda ``draw()`` zwraca pusty string i w zasadzie w Pythonie nie jest potrzebna.

Skupmy się teraz nad klasą ``MultiShape``, która formalnie jest kształtem (dziedziczy po ``Shape``) ale jej obiekty same w sobie nie mają żadnego kształtu.
Obiekt klasy ``MultiShape`` **zawiera** inne kształty! Tak więc podczas gdy klasa ``Circle`` rysuje koło, to klasa
``MultiShape`` deleguje rysowanie do obiektów (kształtów), które zawiera.
Samo dodawanie obiektów ``Shape`` do obiektu ``MultiShape`` można zrobić na różne sposoby, np. definiując metodę ``insert_shape()``.
W tym przypadku przeciążono operator ``+=`` (linie 43-45).

Ponieważ ``MultiShape`` też jest typu  ``Shape``, jego też można gdzieś dodać.
I tak np. w linii 54 zdefiniowano *multi-multishape*, do którego w linii 55 dodawany jest *multishape*.

Wynikiem działania programu jest tekst w formacie SVG, plik ten niestety jednak nie będzie poprawnie wyświetlany.
Brakuje bowiem nagłówka i stopki (patrz przykład powyżej).
Typowym obiektowym rozwiązaniem tego problemu jest zdefiniowanie klasy odpowiedzialnej za poprawne sformatowanie pliku.
Poniżej uzupełniono program o klasę ``SvgFile``, której:

  - konstruktor otwiera plik o podanej nazwie i od razu zapisuje do pliku nagłówek SVG
  - przeciążony operator wywołania (*call operator*) dopisuje kształty do pliku w formacie SVG
  - destruktor zapisuje stopkę SVG i zamyka plik

.. literalinclude:: kształty.py
   :language: python


Kompletny program: interpreter Logo
====================================

Logo jest językiem programowania stworzonym w latach 60. XX wieku.
Jego głównym celem było umożliwienie dzieciom
nauki podstaw programowania poprzez interakcję z komputerem.
Programowanie w Logo sprowadza się do wydawania komend żółwiowi, kierując jego ruchem po ekranie.
Wynikiem programu jest obrazek - ślad narysowany przez poruszającego się żółwia.

Poniższej znajdziesz opis prostego interpretera tego języka. Rozróżnia on następujące komendy:

 - **L n** - obróć żółwia o `n` stopni w lewo, np ``L 10`` obraca żółwia w lewo o 10 stopni
 - **R n** - obróć żółwia o `n` stopni w prawo, np ``R 15`` obraca żółwia w prawo o 15 stopni
 - **UP** - podnieś rysik
 - **DOWN** - opuść rysik
 - **F n** - przesuń żółwia naprzód o n; żółw narysuje linię, o ile rysik jest aktualnie opuszczony
 - **JUMP x y** - przenieś żółwia w położenie `x, y` na ekranie, np ``JUMP 100 100`` przenosi żółwia w położenie `100, 100`
 - **REPEAT n [commands]** – powtarza `n` razy komendy podane w nawiasie, np ``REPEAT 4 [F 10; R90]`` rysuje kwadrat o boku 10
 - **DEF name := [commands]** – definiuje procedurę o podanej nazwie, np ``DEF kwadrat := [ REPEAT 4 [F 10; R90] ]``
 - **FASTER n** - dodaje `n` do długości kroku żółwia; początkowa długość kroku to 1.0
 - **SLOWER n** - odejmuje `n` od długości kroku żółwia

Interpreter pozwala na zagnieżdżanie procedur w pętlach; nie jest jednak możliwe deklarowanie zmiennych.
Zauważ, że jedynie komenda ``F`` (forward) powoduje rysowanie, i to o ile rysik jest opuszczony. Poniższy program w Logo
rysuje kwadrat o boku 10:

.. code-block:: Logo

   DOWN
   F 10
   R 90
   F 10
   R 90
   F 10
   R 90
   F 10

Żółw rysuje cztery odcinki, każdy o długości 10 (``F 10``), za każdym razem odpowiednio obraca się w prawo o 90 stopni ``R 90``.
Ten sam program można zapisać, wykorzystując pętlę:

.. code-block:: Logo

   DOWN
   REPEAT [ F 10; R 90 ]


Kompletny program
---------------------------------------

.. literalinclude:: logo.py
   :language: python
   :lines: 1-230


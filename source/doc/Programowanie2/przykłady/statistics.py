class AbstractStatistCalculator:

    def __call__(self, *args):
        raise NotImplemented("to be implemented in by a derived class")

    def name(self, *args):
        raise NotImplemented("to be implemented in by a derived class")


class Sum(AbstractStatistCalculator):

    def __call__(self, *args):
        if len(args) == 1: args = args[0]
        s = 0
        for v in args: s+=v
        return s

    def name(self, *args):
        return "             sum"

class Average(AbstractStatistCalculator):

    def __call__(self, *args):
        if len(args) == 1: args = args[0]
        s = 0
        for v in args: s+=v
        return s/len(args)

    def name(self, *args):
        return "         average"


class Variance(AbstractStatistCalculator):

    def __call__(self, *args):
        if len(args) == 1: args = args[0]
        s, s2 = 0, 0
        for v in args:
            s += v
            s2 += v * v
        s = s/len(args)
        return s2/len(args) - s*s

    def name(self, *args):
        return "        variance"


class SumVariance(AbstractStatistCalculator):

    def __init__(self):
        self.__avg = Sum()
        self.__var = Variance()

    def __call__(self, *args):
        return self.__avg(*args), self.__var(*args)

    def name(self, *args):
        return "sum and variance"


if __name__ == "__main__":

    from random import random

    sum = Sum()
    avg = Average()
    var = Variance()

    stats = [sum, avg, var, SumVariance()]
    input_data = [random() for i in range(100)]

    for calc in stats:
        print(calc.name()+" :",calc(input_data))

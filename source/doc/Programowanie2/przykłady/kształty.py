class Shape:

    def draw(self): return ""

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Circle(Shape):

    def __init__(self, r, x, y):
        super().__init__(x, y)
        self.r = r

    def draw(self):
        return '<circle cx="%.1f" cy="%.1f" r="%.1f" />\n' % (self.x, self.y, self.r)


class Square(Shape):

    def __init__(self, a, x, y):
        super().__init__(x, y)
        self.a = a

    def draw(self):
        return '<rect x="%.1f" y="%.1f" width="%.1f" height="%.1f" />\n' % \
               (self.x - self.a / 2, self.y - self.a / 2, self.a, self.a)


class MultiShape(Shape):

    def __init__(self):
        super().__init__(0,0)
        self.shapes = []

    def draw(self):
        str = ""
        for s in self.shapes:
            str += s.draw()
        return str

    def __iadd__(self, shape):
        self.shapes.append(shape)
        return self


class SvgFile:

    def __init__(self, filename, width=300, height=300):
        self.__f = open(filename, "w")
        self.__f.write('<svg width="%d" height="%d" xmlns="http://www.w3.org/2000/svg" version="1.1">\n' % (width, height))

    def __call__(self, *args, **kwargs):
        for a in args:
            self.__f.write(a.draw())

    def __del__(self):
        self.__f.write('</svg>')
        self.__f.close()


if __name__ == "__main__":

    sq = Square(6.0,4.0,12.0)
    cr = Circle(3.0,15.0,9.0)
    ms = MultiShape()
    ms += sq
    ms += cr
    mms = MultiShape()
    mms += ms
    print(mms.draw())

    svg = SvgFile("draw.svg", 20, 20)
    svg(mms)

import sys
sys.path.append("../programy")
import math
from random import uniform as rnd
from Atom import Atom
from abc import ABC, abstractmethod

def rnd_atom(id: int, name: str) -> Atom:
    return Atom(id, name, rnd(-10.0, 10.0),rnd(-10.0, 10.0),rnd(-10.0, 10.0))


class Energy(ABC):

    @abstractmethod
    def evaluate_component(self, r:float) -> float:
        pass

    def evaluate_total(self, atoms: list[Atom]) -> float:
        total = 0.0
        for i in range(1, n_atoms):
            for j in range(i):
                d = atoms[i].distance_to(atoms[j])
                total += self.evaluate_component(d)
        return total

class LennardJones(Energy):

    def __init__(self, sigma, eps):
        self.__sigma = sigma
        self.__eps = eps

    def evaluate_component(self, r:float) -> float:
        sr6 = (self.__sigma/r)**6
        return self.__eps*(sr6**2 - sr6)

class Slater(Energy):

    def __init__(self, sigma, A):
        self.__sigma = sigma
        self.__S = A

    def evaluate_component(self, r:float) -> float:
        return self.__A*math.exp(-r/self.__sigma)

if __name__ == "__main__":
    n_atoms = 10
    atoms = [rnd_atom(i, "C") for i in range(n_atoms)]

    lj = LennardJones(2.3, 0.02)
    total = lj.evaluate_total(atoms)
    print(total)
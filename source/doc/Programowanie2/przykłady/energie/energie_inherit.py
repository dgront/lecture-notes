import sys
sys.path.append("../programy")
from random import uniform as rnd
from Atom import Atom

def rnd_atom(id: int, name: str) -> Atom:
    return Atom(id, name, rnd(-10.0, 10.0),rnd(-10.0, 10.0),rnd(-10.0, 10.0))


class Energy:
    def evaluate_total(self, atoms: list[Atom]) -> float:
        total = 0.0
        for i in range(1, n_atoms):
            for j in range(i):
                d = atoms[i].distance_to(atoms[j])
                # ... and here we evaluate energy for a given distance
        return total

if __name__ == "__main__":
    n_atoms = 10
    atoms = [rnd_atom(i, "C") for i in range(n_atoms)]

    e = Energy()
    e.evaluate_total(atoms)
import math
from random import uniform as rnd

n_atoms = 10
coords = [[rnd(-10.0, 10.0),rnd(10.0, 10.0)] for i in range(n_atoms)]
for i in range(1, n_atoms):
    for j in range(i):
        dx = coords[i][0] - coords[j][0]
        dy = coords[i][1] - coords[j][1]
        r = math.sqrt(dx*dx + dy*dy)
        # ... and here we evaluate energy for a given distance
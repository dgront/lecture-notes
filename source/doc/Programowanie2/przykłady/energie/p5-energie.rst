Dziedziczenie *vs* zawieranie
================================

Poniższy przykład pokaże Ci, czym w praktyce różni się zawieranie od dziedziczenia.

W zadaniu tym należy zaimplementować liczenie energii układu N atomów:
:math:`E = \sum_{i=1}^{N} \sum_{j<i} E(r_{ij})`.

Jak widzisz z powyższego wzoru, energia całkowita jest sumą oddziaływań pomiędzy parami atomów :math:`i` oraz :math:`j`,
których wartość zależnych jedynie od odległości pomiędzy tymi atomami :math:`r_{ij}`. Oddziaływania par
mogą być różne; najważniejsze z punktu widzenia fizyki to:

 - elektrostatyczne :math:`E_{Q}(r_{ij}) = k\frac{q_i q_j}{r_{ij}}`
 - Lennard-Jonesa :math:`E_{LJ}(r_{ij}) = \epsilon \bigg[ \bigg(\frac{\sigma}{r}\bigg)^{12}  - \bigg(\frac{\sigma}{r}\bigg)^{6} \bigg]`
 - Slatera :math:`E_{S}(r_{ij}) = A e^{-r_{ij}/\sigma}`

choć lista możliwych oddziaływań (i co tym idzie, wzorów do zaimplementowania) jest dość długa.

Na początek współrzędne atomów zapiszmy w dwuwymiarowej liście. Dla uproszczenia przyjmijmy, że układ jest dwuwymiarowy - każdy atom ma współrzędną
:math:`x` oraz :math:`y`. Nasza lista ma zatem rozmiar `[N][2]` gdzie `N` to liczba atomów w układzie.
Współrzędne zainicjalizujmy losowymi wartościami:

.. literalinclude:: energie_1.py
   :language: python
   :linenos:
   :lines: 1-5

Kolejny krok to napisanie pętli, które dla każdej pary atomów policzą energię całkowitą:

.. literalinclude:: energie_1.py
   :language: python
   :linenos:
   :lines: 6-11

W ostatniej linii należy zaimplementować odpowiedni wzór liczący energię, jeden z kilku do wyboru.
Policzenie kilku rodzajów energii (np elektrostatycznej oraz Lennarda Jonesa) dla tego samego układu
wymaga **powtórzenia kodu z pętlami biegnącymi po parach atomów**

Rozwiązanie obiektowe
---------------------

Zastanówmy się, jak ten problem rozwiązać, programując obiektowo. Po pierwsze,
warto wykorzystać to, co już mamy: klasę ``Atom``, opisaną w rozdziale :ref:`Dziedziczenie klas<../Dziedziczenie.rst>`.
Tworzenie losowych atomów może wyglądać np tak:

.. literalinclude:: energie_inherit.py
   :language: python
   :linenos:
   :lines: 3-8, 18-21

w czym pomaga nam zdefiniowana tu funkcja ``rnd_atom()``. Stworzymy też klasę ``Energy``, której metoda ``evaluate_total()``
obliczać będzie energię całkowitą układu:

.. literalinclude:: energie_inherit.py
   :language: python
   :linenos:
   :lines: 10-17

Jak widzisz, jej treść jest w zasadzie kopią rozwiązania opisanego powyżej. Dotarliśmy jednak do momentu,
w który musimy zdecydować, *jak rozwiążemy obliczanie energii dla par atomów*.

Dziedziczenie
--------------

Rozwiązując powyższy problemu z wykorzystaniem **dziedziczenia**, należy **wydzielić kod wspólny** dla wszystkich
obliczeń **do klasy bazowej**, a **kod różny** dla różnych energii **zdefiniować w klasach potomnych**. W naszym przypadku
wspólna część kodu to pętle po atomach, czyli aktualna treść metody ``evaluate_total()``;
kod różniący poszczególne klasy to implementacje różnych wzorów na :math:`E(r_{ij})`.

Klasa ``Energy`` będzie klasą bazową, dostarczajacą metodę ``evaluate_total()``. Wewnątrz pętli wywoływać będziemy metodę
``evaluate_component(r: float)`` obliczającą energię poszczególnych wkładów. Klasa ``Energy``
nie ma jednak żadnego konkretnego wzoru, to jedynie *abstrakcyjne pojęcie energii całkowitej układu*.
Dlatego metodę ``Energy.evaluate_component(r: float)`` defniniujemy jako :term:`metodę abstrakcyjną <metoda abstrakcyjna>`, co spowoduje, że
cała klasa ``Energy`` stanie się :term:`klasą abstrakcyjną <klasa abstrakcyjna>`.

.. literalinclude:: energie_inherit_2.py
   :language: python
   :linenos:
   :lines: 3-24

Tak więc klasa  ``Energy`` umie policzyć energię całkowitą, ale nie umie policzyć poszczególnych składowych.
Klasy potomne zaś będą wiedziały, jak policzyć składowe, umiejętność ich posumowania zaś odziedziczą z klasy bazowej.
Klasy ``LennardJones`` i ``Slater`` mogą na przykład wyglądać nastepująco:

.. literalinclude:: energie_inherit_2.py
   :language: python
   :linenos:
   :lines: 26-43

W części głównej programu można teraz stworzyć obiekty ``LennardJones`` i/lub ``Slater`` i policzyć energię.

.. literalinclude:: energie_inherit_2.py
   :language: python
   :linenos:
   :lines: 45-51

Zauważ, że ponieważ metoda ``evaluate_component()`` jest publiczna, można ją wywołać w głównym programie, np tak:

.. code-block:: Python

   lj.evaluate_component(4.36)

Gdybyś chciał temu zapobiec, metodę tę (zarówno w klasie bazowej jak i w klasach potomnych)
należy uczynić *chronioną*, czyli zmienić jej nazwę na ``_evaluate_component()``

Zawieranie
--------------

Wykorzystując **zawieranie**, należy schować funkcjonalność, która do tej pory dostarczana była przez metody
``evaluate_component(r: float)`` do środka
obiektu klasy ``Energy`` - jako jego składową. W tym celu należy powołać do istnienia
klasy, które będą implementować poszczególne wzory. Zaczniemy od wpólnej dla nich abstrakcyjnej klasy bazowej ``EnergyFormula``:

.. literalinclude:: energie_compos.py
   :language: python
   :linenos:
   :lines: 11-15

... a następnie stworzymy dwie jej konkretne implementacje: jedną dla wzoru Lennard-Jonesa i drugą dla wzoru Slatera

.. literalinclude:: energie_compos.py
   :language: python
   :linenos:
   :lines: 17-34

Tak przygotowany obiekt odpowiedzialny za przeliczanie odległości na energię należy **włożyć** do obiektu klasy ``Energy``,
która obecnie wygląda następująco:

.. literalinclude:: energie_compos.py
   :language: python
   :linenos:
   :lines: 36-47

Zauważ, że klasa ``Energy`` już nie jest abstrakcyjna a wszystkie jej metody są konkretne (zaimplementowane)
Część główna programu wygląda bardzo podobnie do wersji z dziedziczeniem:

.. literalinclude:: energie_compos.py
   :language: python
   :linenos:
   :lines: 49-55

Podsumowanie
---------------------

Elementem kluczowym dla zrozumienia różnicy między **dziedziczeniem** a **zawieraniem**
jest ta linia kodu, która oblicza poszczególne energie: ``total += self.evaluate_component(d)``
w przypadku dziedziczenia oraz ``total += self.__fomula.evaluate_component(d)`` dla zawierania.
W pierwszym przypadku wywoływana jest własna metoda obiektu (``self.moja_metoda()``),
w drugim zaś metoda dostarczona przez inny obiekt (``self.moje_pole.jego_metoda()``)

Z punktu widzenia użytkownika (czyli sekcji ``__main__``) jedyna różnicą
jest konieczność stworzenia obiektu ``EnergyFormula`` i dostarczenie go konstruktorowi
klasy ``Energy``
class SimpleGraph:

    def __init__(self):
        pass

    def add_edge(self,vi,vj):
        pass

    def are_connected(self,vi,vj):
        pass

    def get_edge(self, vi, vj):
        pass

    def get_edges(self, vi):
        pass

    def get_vertices(self,vi):
        pass

    def add_vertex(self,vi):
        pass

if __name__ == "__main__":

    g = SimpleGraph()
    for vi in ['A','B','C','D','E'] :
        g.add_vertex(vi)
    g.add_edge('A','B', "blue")
    g.add_edge(4, 3, "red")
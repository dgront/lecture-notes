class SimpleGraph:

    def __init__(self):
        self.__edges = []
        self.__e_val = []
        self.__vertc = []

    def bind_vertices(self, vi, vj, edge):
        self.bind_indexes(self.__index_for_vertex(vi), self.__index_for_vertex(vj), edge)

    def bind_indexes(self, i, j, edge):

        if self.are_connected(i, j): return
        self.__edges[i].append(j)
        self.__edges[j].append(i)
        self.__e_val[i].append(edge)
        self.__e_val[j].append(edge)

    def are_connected(self, i, j):
        return j in self.__edges[i]

    def get_edge(self, vi, vj):
        pass

    def get_edges(self, vi):
        return self.__e_val[self.__index_for_vertex(vi)]

    def get_vertices(self, vi):
        pass

    def add_vertex(self, vi):
        self.__vertc.append(vi)
        self.__edges.append([])
        self.__e_val.append([])
        return len(self.__vertc) - 1

    def __index_for_vertex(self, vi):
        return self.__vertc.index(vi)

if __name__ == "__main__":

    g = SimpleGraph()
    for ve in ['A','B','C','D','E'] :
        g.add_vertex(ve)
    g.bind_vertices('A','B',"blue")
    g.bind_indexes(4,3, "red")
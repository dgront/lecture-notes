class Vec3 :

  def __init__(self,*args) :
    
    if len(args) == 3 :                # support for Vec3(1.2,3.8,0.1)
      self.x, self.y, self.z = args[0], args[1], args[2]
    elif len(args) == 1 :            
      if isinstance(args[0], Vec3) :   # support for Vec3(v)
      	self.x, self.y, self.z = args[0].x, args[0].y, args[0].z
      elif isinstance(args[0], list) : # support for Vec3( [1.2,3.8,0.1] )
      	self.x, self.y, self.z = args[0][0], args[0][1], args[0][2]
      else :                           # support for Vec3(0)
      	self.x, self.y, self.z = args[0], args[0], args[0]


if __name__ == "__main__" :

	v1 = Vec3(0)
	v2 = Vec3(v1)
	v3 = Vec3(0,1,2)
	v4 = Vec3([0,1,2])	
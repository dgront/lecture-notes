class Vec3 :

  def __init__(self,*args) :
    
    if len(args) == 3 :                # support for Vec3(1.2,3.8,0.1)
      self.__x, self.__y, self.__z = args[0], args[1], args[2]
    elif len(args) == 1 :            
      if isinstance(args[0], Vec3) :   # support for Vec3(v)
      	self.__x, self.__y, self.__z = args[0].__x, args[0].__y, args[0].__z
      elif isinstance(args[0], list) : # support for Vec3( [1.2,3.8,0.1] )
      	self.__x, self.__y, self.__z = args[0][0], args[0][1], args[0][2]
      else :                           # support for Vec3(0)
      	self.__x, self.__y, self.__z = args[0], args[0], args[0]

  def get_x(self) : return self.__x

  def set_x(self,x) : self.__x = x

  def get_y(self) : return self.__y

  def set_y(self,y) : self.__y = y

  def get_z(self) : return self.__z

  def set_z(self,z) : self.__z = z

if __name__ == "__main__" :

	v1 = Vec3(0)
	v2 = Vec3(v1)
	v3 = Vec3(0,1,2)
	v4 = Vec3([0,1,2])
	

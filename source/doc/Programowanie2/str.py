class Vec3:

    def __init__(self, x,y,z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self) :
    	return "%.3f,%.3f,%.3f" % (self.x, self.y, self.z)
    
if __name__ == "__main__" :
    v1 = Vec3(1.2, 3.4, 2.6)
    print(v1)


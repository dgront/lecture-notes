from sys import argv
from Atom import Atom


class AtomSelector :
    """ Base class for atom selectors
    """
    def __call__(self, atom):
        raise NotImplementedError


class SelectCA(AtomSelector) :
    """ Selects alpha carbons
    """
    def __call__(self, atom):
        if atom.name == " CA " : return True
        return False


class SelectHeavyBB(AtomSelector) :
    """ Selects backbone heavy atoms
    """
    def __call__(self, atom):
        if atom.name == " N  " or atom.name == " CA " \
            or atom.name == " C  " or atom.name == " O  " : 
            return True
        return False


class InvertSelector(AtomSelector) :

    def __init__(self,selector) :
        self.__selector = selector

    def __call__(self, atom):
        return not self.__selector(atom)


class LogicORSelector(AtomSelector) :

    def __init__(self) :
        self.__selectors = []

    def add_selector(self, sel):
        self.__selectors.append(sel)

    def __call__(self, atom):
        for s in self.__selectors :
            if s(atom): return  True
        return False


if __name__ == "__main__" :

    # Read atoms from a PDB file
    atoms = []
    i = 1
    for line in open(argv[1]) : # e.g. "2gb1.pdb"
        if line.startswith("ATOM  ") :
            atoms.append( Atom(i, line[12:16], line[76:78]) )
    
    # Create some selectors
    is_ca = SelectCA()
    is_bb = SelectHeavyBB()
    is_sc = InvertSelector(SelectHeavyBB())
    print("name   is_CA  is_BB  is_SC")
    print("--------------------------")
    for ai in atoms :
        print("%4s   %5s  %5s  %5s" %(ai.name,is_ca(ai), is_bb(ai), is_sc(ai)))



# ---------- The version with functions (no objects at all) ----------
def woof():
    print("woof")


def purr():
    print("purr")


say_sth = {"dog": woof, "cat": purr}

for animal in ["dog", "dog", "cat"]:
   say_sth[animal]()


# ---------- The version with  objects ----------

class Woof:
    def __call__(self):
        print("woof, woof")


class Purr:
    def __call__(self):
        print("purr, purr")


say_sth = {"dog": Woof(), "cat": Purr()}

for animal in ["dog", "dog", "cat", "dog"]:
    say_sth[animal]()

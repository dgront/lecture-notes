class Paper:
    def name(self): return "Paper"

class Rock:
    def name(self): return "Rock"

class Scissors:
    def name(self): return "Scissors"


outcomes = { (Paper, Paper) : "draw",
             (Scissors, Scissors): "draw",
             (Rock, Rock): "draw",
             (Rock, Paper): "player 2 won",
             (Paper, Rock): "player 2 won",
             (Rock, Scissors): "player 1 won",
             (Scissors, Rock): "player 2 won",
             (Scissors, Paper): "player 1 won",
             (Paper, Scissors): "player 1 won"}

def play(player1, player2):
    return outcomes[(player1.__class__, player2.__class__)]

if __name__ == "__main__":
    p1 = Scissors()
    p2 = Paper()
    print(p1.name(),"versus",p2.name()," : ",play(p1,p2))



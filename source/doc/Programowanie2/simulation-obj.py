class Harmonic1DSystem:

    def __init__(self):
        self.x = 0
        self.v = 0
        self.k = 5.5
        self.x0 = 10
        self.mass = 1

    @property
    def a(self):
        return -self.k * (self.x - self.x0) / (self.mass * 2.0)


class EulerIntegrator:

    def __init__(self, my_system):
        self.system = my_system
        self.observers = []
        self.dt = 0.001

    def add_observer(self, o):
        self.observers.append(o)

    def run(self, n_inner_steps, n_outer_steps):

        for j in range(n_outer_steps):
            for i in range(n_inner_steps):
                self.system.v += self.system.a * self.dt
                self.system.x += self.system.v * self.dt
            for o in self.observers:
                o.observe()


class Observer:

    def observe(self):
        pass

    def finalize(self):
        pass


class ObserveToScreen (Observer):

    def __init__(self, object):
        self.o = object

    def observe(self):
        print(self.o.x)


class ObserveToFile (Observer):

    def __init__(self,object, fname):
        self.o = object
        self.__file = open(fname,"w")

    def observe(self):
        print(self.o.x, file=self.__file)

    def finalize(self): self.__file.close()


class MinMaxObserver (Observer):

    def __init__(self, object):

        self.o = object
        self.min_x = self.o.x
        self.max_x = self.o.x

    def observe(self):
            if self.o.x > self.max_x : self.max_x = self.o.x
            if self.o.x < self.min_x : self.min_x = self.o.x


if __name__ == "__main__":

    system = Harmonic1DSystem()
    ene = ObserveToScreen(system)
    #    ene = ObserveToFile(system, "energie")
    mmx = MinMaxObserver(system)
    s = EulerIntegrator(system)
    s.add_observer(ene)
    s.add_observer(mmx)
    s.run(10, 10)
    print(mmx.min_x, mmx.max_x)

    for obs in s.observers: obs.finalize()


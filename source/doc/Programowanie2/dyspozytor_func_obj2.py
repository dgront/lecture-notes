class Speak:
    def __init__(self):
        self.n = 0


class Woof(Speak):
    def __call__(self):
        self.n += 1
        print("woof, woof", self.n)


class Purr(Speak):
    def __call__(self):
        self.n += 1
        print("purr, purr", self.n)


say_sth = {"dog": Woof(), "cat": Purr(), "retriever": Woof()}

for animal in ["dog", "cat", "cat", "retriever"]:
    say_sth[animal]()

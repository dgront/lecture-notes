from sys import argv

def process_files(file_name) :

  if file_name.endswith(".pdb") :
  	print("processing a PDB file: ",file_name)
  	# ... many, many lines of code that are necessary to parse PDB
  elif file_name.endswith(".fasta") :
  	print("processing a FASTA file: ",file_name)
  	# ... many, many lines of code that are necessary to parse FASTA
  elif file_name.endswith(".aln") :
  	print("processing MSA data in ALN format from file: ",file_name)
  	# ... many, many lines of code that are necessary to parse ALN
  else:
  	print("Unknown file format: ", file_name)

if __name__ == "__main__" :
 	for fname in argv[1:] : process_files(fname)


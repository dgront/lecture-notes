class Vec2:
    def __init__(self, x, y): self.x, self.y = x, y

class Circle:
    def __init__(self, x, y, r):
        self.pos = Vec2(y,y)
        self.r = r

class Polygon(Circle):
    def __init__(self):
        super().__init__(0, 0, 0)
        self.nodes = []

    def node(self, *args):
        if isinstance(args[0], Vec2):
            self.nodes.append(args[0])
            return self
        elif len(args) == 2 :
            self.nodes.append(Vec2(args[0],args[1]))
            return self
        else:
            raise ValueError("Can't create a vertex from an object of type: ")


def draw(o):
    if isinstance(o, Polygon):
        for i in range(len(o.nodes)):
            print('<line x1="%d" y1="%d" x2="%d" y2="%d" stroke="black" />'
                  % (o.nodes[i-1].x, o.nodes[i-1].y,o.nodes[i].x, o.nodes[i].y))
    elif isinstance(o, Circle):
        print('<circle cx="%d" cy="%d" r="%d" stroke="red" />' % (o.pos.x, o.pos.y, o.r))


if __name__ == "__main__":
    c = Circle(10,12,6)
    p = Polygon()
    p.node(6, 0).node(10, 0).node(Vec2(8, 5))

    for o in [c, p] : draw(o)

import sys 

class PdbElement :

	def __init__(self, pdb_line) :
		raise NotImplementedError


class PdbElementMaker :

    def make_instance(self, pdb_line):
		raise NotImplementedError

class Atom(PdbElement):
    def __init__(self,pdb_line):
        self.__element_type = "ATOM"
        self.__atom_id = pdb_line[6:11]
        self._atom_name = pdb_line[12:16]
        self.__atom_res_name = pdb_line[17:20]
        self.__atom_x = float(pdb_line[30:38])
        self.__atom_y = float(pdb_line[38:46])
        self.__atom_z = float(pdb_line[46:54])
    
    def __str__(self):
        string = '%s %s %s %s%8.3f%8.3f%8.3f' % (self.__element_type, self.__atom_id, self._atom_name, self.__atom_res_name, self.__atom_x, self.__atom_y, self.__atom_z)
        return string

class AtomMaker(PdbElementMaker):
    def make_instance(self, pdb_line):
        return Atom(pdb_line)


class SSBond(PdbElement) :
	
	def __init__(self,pdb_line) :
		self.__element_type = "SSBOND"
		self.__atom_i = pdb_line[11:14]
		self.__atom_j = pdb_line[25:28]
		self.__i_resid = int(pdb_line[17:21])
		self.__j_resid = int(pdb_line[31:35])
		self.__distance = float(pdb_line[73:78])

	def __str__(self):
		napis='%s %s %d %s %d %5.3f' % (self.__element_type,self.__atom_i,self.__i_resid,self.__atom_j,self.__j_resid, self.__distance)
		return napis

class SSBondMaker(PdbElementMaker) :

	def make_instance(self,pdb_line) : return SSBond(pdb_line)


class Header(PdbElement) :
	
	def __init__(self,pdb_line) :
		self.__element_type = "HEADER"

class HeaderMaker(PdbElementMaker) :

	def make_instance(self,pdb_line) : return Header(pdb_line)

class PdbElementFactory:

	def __init__(self) :
		self.__production_line = {}

	def register_product(self, field_name,maker) :
		self.__production_line[field_name] = maker

	def static_produce(self, pdb_lines) :
		output = []
		for line in pdb_lines :
			if line.startswith("ATOM") : output.append( Atom(line) )
			elif line.startswith("SSBOND") : output.append( SSBond(line) )
			else : raise Exception("UNKNOWN PDB FIELD TYPE !!")

	def produce(self, pdb_lines) :
		output = []
		for line in pdb_lines :
			field_key = line[0:6]
			if field_key in self.__production_line:
				output.append( self.__production_line[field_key].make_instance(line) )
		print("production done")
		return output

if __name__ == "__main__" :
	factory = PdbElementFactory()
	factory.register_product("ATOM  ", AtomMaker())
	factory.register_product("HEADER", HeaderMaker())
	factory.register_product("SSBOND", SSBondMaker())
	f=open(sys.argv[1])

   for line in f : factory.produce([line])

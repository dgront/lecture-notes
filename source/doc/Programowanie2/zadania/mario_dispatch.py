game_actors = [Turtle(20, 5), Coin(24, 8),
                      Coin(26, 8), Cherry(30, 2)]

if __name__ == "__main__":

    # in the main loop of the game you update Mario's position
    mario = Mario(0, 0)
    # register collision action in a dispatch:
    collisions = CollisionDispatch()
    collisions.register(Coin.class, MarioHitsCoin)

    # check for collisions:
    for actor in game_actors:
        # dispatch actions accordingly to the actor's type
        if mario.if_crashes_into(actor):
            collisions.dispatch(actor)


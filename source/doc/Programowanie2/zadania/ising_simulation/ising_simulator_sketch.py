from abc import ABC, abstractmethod

class Observer(ABC):
    @abstractmethod
    def observe(self): pass

# 0) finish the  Simulator class to make it running
# 1) implement the following observers:
#     a. ToFileObserver <- EnergyToFile
#     b. ObserveHistogram <- EnergyHistogram, MagnetisationHistogram

class AbstractModel:pass
class IsingModel(AbstractModel): pass

class Simulator:

    def __init__(self, system: AbstractModel):
        self.__system = system
        self.beta = 1.0
        self.__frequent_observers = []
        self.__rare_observers = []


    def simulate(self, n_inner:int , n_outer: int):
        for i in range(n_outer):
            for j in range(n_inner):
                self.__system.mc_sweep(self.beta)
                for o in self.__frequent_observers:
                    o.observe()
            for o in self.__rare_observers:
                o.observe()
                
    def add_rare_observer(self, o: Observer):
        self.rare_observers.append(o)


# --------------- the following should work:
if __name__ == "__main__":
    # create a modelled system
    ising_model = Ising(100,100)

    # what do we measure during simulation
    en_m = EnergyMeasurement(ising_model)
    m_m = MagnetMeasurement(ising_model)

    # observations
    en_file = ToFileObserver(en_m)
    m_his = ObserveHistogram(m_m)

    # the simulation itself
    sim = Simulator(ising)
    sim.add_frequent_observer(m_his)
    sim.add_rare_observer(en_file)
    sim.temperature = 1.25
    sim.simulate(100,1000)


# --- a variant with simulated annealing:
    for temperature in [3.0, 2.75, 2.5, 1.3,1.2,1.1, 1.0, 0.9]:
        sim.temperature = 1.25
        sim.simulate(100,100)
        
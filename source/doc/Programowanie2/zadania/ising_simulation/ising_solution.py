from abc import ABC, abstractmethod
from random import random

import numpy as np


class AbstractMCModel:pass
class IsingModel(AbstractMCModel):
    def __init__(self, n: int):
        ''' generates a random spin configuration for initial condition'''
        self.__state = 2 * np.random.randint(2, size=(n, n)) - 1
        self.N = n
        self.__jj = 1.0

    def calc_energy(self):
        energy = 0
        config = self.__state
        for i in range(len(config)):
            for j in range(len(config)):
                S = config[i, j]
                nb = config[(i + 1) % self.N, j] + config[i, (j + 1) % self.N] + config[(i - 1) % self.N, j] + config[i, (j - 1) % self.N]
                energy += -nb * S * self.__jj
        return energy / 2.

    def spins(self): return self.__state

    def calc_mag(self):
        '''Magnetization of a given configuration'''
        mag = np.sum(self.__state)
        return mag

    def mc_sweep(self, beta: float):
        config = self.__state
        for i in range(self.N):
            for j in range(self.N):
                a = np.random.randint(0, self.N)
                b = np.random.randint(0, self.N)
                s = config[a, b]
                nb = config[(a + 1) % self.N, b] + config[a, (b + 1) % self.N] + config[(a - 1) % self.N, b] + config[a, (b - 1) % self.N]
                cost = 2 * s * nb * self.__jj
                if cost < 0:
                    s *= -1
                elif random() < np.exp(-cost * beta * self.__jj):
                    s *= -1
                config[a, b] = s

class Measurement(ABC):
    @abstractmethod
    def measure(self): pass

class Observer(ABC):
    @abstractmethod
    def observe(self): pass

class EnergyMeasurement(Measurement):
    def __init__(self, ising: IsingModel):
        self.__ising = ising

    def measure(self):
        return self.__ising.calc_energy()


class Histogram:
    def __init__(self, bin_width: float):
        self.__data = {}
        self.__bin_width = bin_width

    def record(self, x: float):
        bin_index = int(x / self.__bin_width)
        if bin_index not in self.__data:
            self.__data[bin_index] = 1
        else:
            self.__data[bin_index] += 1

    def count(self, bin_idx) -> int:
        return self.__data[bin_idx] if bin_idx in self.__data else 0;

    def print(self):
        bins = list(sorted(self.__data.keys()))
        for indx in bins:
            print(indx, self.__data[indx])

class HistogramObserver(Observer):
    def __init__(self, histogram: Histogram, measurement: Measurement):
        self.__histogram = histogram
        self.__measurement = measurement

    def observe(self):
        x = self.__measurement.measure()
        self.__histogram.record(x)

    def print(self): self.__histogram.print()

class StateObserver(Observer):

    def __init__(self, ising: IsingModel):
        self.__state = ising

    def observe(self):
        n = self.__state.N
        for i in range(n):
            for j in range(n):
                s = '#' if self.__state.spins()[i,j]==1 else ' '
                print(s, end='')
            print()
        print()
class MagMeasurement(Measurement):
    def __init__(self, ising: IsingModel):
        self.__ising = ising

    def measure(self):
        return self.__ising.calc_mag()

class ToFileObserver(Observer):
    def __init__(self, fname: str, m: Measurement):
        self.__fname = fname
        self.__m = m
        f = open(self.__fname, "w")
        print(file=f)
        f.close()


    def observe(self):
        f = open(self.__fname, "a")
        print(self.__m.measure(), file=f)
        f.close()

class ToScreenObserver(Observer):
    def __init__(self, m: Measurement):
        self.__m = m

    def observe(self):
        print(self.__m.measure())

class Simulator:

    def __init__(self, system: AbstractMCModel):
        self.__system = system
        self.beta = 1.0
        self.__frequent_observers = []
        self.__rare_observers = []

    def simulate(self, n_inner: int, n_outer: int):
        for i in range(n_outer):
            for j in range(n_inner):
                self.__system.mc_sweep(self.beta)
                for o in self.__frequent_observers:
                    o.observe()
            for o in self.__rare_observers:
                o.observe()

    def add_rare_observer(self, o: Observer):
        self.__rare_observers.append(o)

    def add_frequent_observer(self, o: Observer):
        self.__frequent_observers.append(o)

def main():
    ising = IsingModel(10)
    mag = MagMeasurement(ising)
    mag_to_file = ToFileObserver("magn.txt", mag)
    measure_en = EnergyMeasurement(ising)
    print_energy = ToScreenObserver(measure_en)
    sim = Simulator(ising)

    # sim.add_rare_observer(mag_to_file)
    sim.add_rare_observer(print_energy)
    # sim.add_rare_observer(StateObserver(ising))
    en_histogram = HistogramObserver(Histogram(1.0), measure_en)
    sim.add_frequent_observer(en_histogram)

    sim.beta = 0.25
    sim.simulate(100, 100)
    en_histogram.print()

if __name__ == "__main__":
    main()

W tym ćwiczeniu powinniście Państwo:

1) przerobić kod Ising.py na obiekt: powinien mieć metody
   a) mc_sweep(beta) wykonującą jeden cykl MC w temperaturze odwrotnej = beta
   b) energy() liczącą energię układu
   c) magnet() liczącą magnetyzację układu

2) Stworzyć klasę bazową Measurement która dostarcza bazowej metody measure()

3) Stworzyć dwie klasy potomne po Measurement EnergyMeasurement oraz MagnetMeasurement, mierzących energię i magnetyzację

2) Stworzyć klasę bazową Observer która dostarcza bazowej metody observe()

3) Dokończyć klasę Simulator tak, aby:
  - można było dodawać obserwacje (obiekty Observer) do częstych i rzadkich obserwacji
  -

4) Stworzyć klasy potomne: ToFileObserver i ObserveHistogram, które:
 - wywołują measure() dla obiektu Measurement, który zawierają
 - nagrywają liczbę do pliku lub dodają do histogramu


Przykład działania (sekcja main):
```
if __name__ == "__main__":
    # create a modelled system
    ising_model = Ising(100,100)
    # what do we measure during simulation
    en_m = EnergyMeasurement(ising_model)
    m_m = MagnetMeasurement(ising_model)
    # observations
    en_file = ToFileObserver(en_m)
    m_his = ObserveHistogram(m_m)
    # the simulation itself
    sim = Simulator(ising)
    sim.add_frequent_observer(m_his)
    sim.add_rare_observer(en_file)
    sim.temperature = 1.25
    sim.run(100,1000)
    
```
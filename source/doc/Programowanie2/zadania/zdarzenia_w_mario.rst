Dyspozytor zdarzeń - gra *Mario*
================================

Piszesz grę platformową wzorowaną na świecie *Mario*. Postać,
którą kieruje gracz, może zderzyć się z różnymi ruchomymi obiektami na planszy,
taki jak moneta, żółw (Mario ginie), czy wisienka (supermoc). Aktualnie Twój kod prezentuje się następująco:

.. literalinclude:: mario_if.py
   :language: python
   :linenos:
   :lines: 1-18

Powyższy kod jest nieoptymalny z wielu powodów:

 1. Reprezentowanie aktorów gry (moneta, żółw) jako tuple trójelementowe: (x, y, nazwa) jest
    proszeniem się o kłopoty: kod jest nieczytelny i łatwo pomylić indeks ``[0]`` z ``[1]``.
    Dodatkowo rozbudowywanie aktorów o dodatkowe zmienne (np liczba monet zebranych przez Mario)
    stwarza dodatkowe problemy z indeksowaniem.
 2. instrukcja wielokrotnego wyboru, która podejmuje decyzję o reakcji na zdarzenie
    jest trudna w utrzymaniu / rozbudowie

Przepisz powyższy kod tak, aby:

 - każdy z aktorów dziedziczył po abstrakcyjnej klasie ``Actor``, zawierającej jego położenie na planszy
 - klasa ``Mario`` powinna zawierać też aktualną liczbę żyć gracza, liczbę zebranych monet oraz flagę logiczną
   włączającą supermoce (po zderzeniu z wisienką)
 - dla każdej z tych klas napisz też klasę-akcję, która modyfikuje obiekt ``Mario``
   odpowiednio do zdarzenia; klasy te powinny dziedziczyć po abstrakcyjnej klasie bazowej ``AbstractAction``
 - zaimplementu dyspozytor, który każdej z możliwych klas ``Actor`` przypisze odpowiedni obiekt ``AbstractAction``

Pierwsze dwie kropki z powyżej listy rozwiązują problem (1) - obiektową reprezentację aktorów. Dwie kolejne kropki
powinny wyeliminować instrukcję wielokrotnego wyboru (punkt (2)). Poprawnie napisany
kod powinien działać z poniższą sekcją ``__main__``:

.. literalinclude:: mario_dispatch.py
   :language: python
   :linenos:
   :lines: 1-16

Rozwiązanie zadania znajduje się poniżej:

.. raw:: html

   <details>
   <summary>(rozwiń kod  rozwiązania)</summary>

.. literalinclude:: mario_dispatch_full.py
   :language: python
   :linenos:

.. raw:: html

   </details>

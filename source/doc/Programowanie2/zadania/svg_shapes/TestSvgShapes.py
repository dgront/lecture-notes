import io
import re
import unittest

from svg_shapes2 import Circle, Rectangle, Group, Line, SVGPainter


class TestSvgShapes(unittest.TestCase):

    def setUp(self):
        self.expected = """<svg height="15" width="15" xmlns="http://www.w3.org/2000/svg">

<rect id="r2" fill="black" x="5" y="5" width="5" height="5" />
<circle id="c1" fill="black" r="4" cx="5" cy="5" />
<circle id="c2" fill="black" r="4" cx="10" cy="10" />
<g>
	<line id="l0" x1="1" y1="1" x2="1" y2="14" stroke="black"/>
	<line id="l1" x1="1" y1="1" x2="14" y2="1" stroke="black"/>
	<line id="l2" x1="14" y1="14" x2="1" y2="14" stroke="black"/>
	<line id="l3" x1="14" y1="14" x2="14" y2="1" stroke="black"/>
</g>
</svg>
"""

    def test_output(self):
        shapes = [Circle("c1", 5, 5, 4), Circle("c2", 10, 10, 4)]
        r = Rectangle("r2", 5, 5, 5, 5)

        group = Group("g1")
        group.add(Line("l0", 1, 1, 1, 14))
        group.add(Line("l1", 1, 1, 14, 1))
        group.add(Line("l2", 14, 14, 1, 14))
        group.add(Line("l3", 14, 14, 14, 1))

        file_obj = io.StringIO()
        painting = SVGPainter(file_obj, 15, 15)
        painting.draw(r)
        painting.draw(shapes)
        painting.draw(group)
        painting.close()

        expected = re.sub(r'\s+', '', self.expected)
        actual = re.sub(r'\s+', '', file_obj.getvalue())
        self.assertEqual(actual, expected)


if __name__ == '__main__':
        unittest.main()
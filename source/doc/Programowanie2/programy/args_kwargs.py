def one_argv_with_default(n=3) :
  print("wartosc:", n)

def many_args(*lista) :
  print("lista:", lista)

def args_by_keyword(**slownik) :
  print("słownik:", slownik)

one_argv_with_default()
one_argv_with_default(9)

many_args(3,4)
many_args(3,4,5,6,7,8)

args_by_keyword(r=5.0, center=[4.5,4.9], color="0xFFaF1C")
from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum

import random


board_drawed = """
#################################
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
# #### #### #### #### #### #### #
#                               #
#################################
"""


class Direction(Enum):
    Left = (-1, 0)
    Right = (1, 0)
    Down = (0, 1)
    Up = (0, -1)
    Stop = (0, 0)               # to say we aren't walking at all
    Nonsense = (1000, 1000)     # to mark nonsense direction

moves = [Direction.Right, Direction.Left, Direction.Up, Direction.Down]


@dataclass
class Sprite(ABC):
    __name: str
    __x: int
    __y: int

    @property
    def name(self): return self.__name

    @property
    def x(self): return self.__x

    @property
    def y(self): return self.__y

    @x.setter
    def x(self, x): self.__x = x

    @y.setter
    def y(self, y): self.__y = y

    def __str__(self): return f"{self.name}: {self.x} {self.y}"


class SpriteMover(ABC):

    """ Strategy used to propose a move direction for a sprite"""
    @abstractmethod
    def propose_direction(self, x, y, board: Board) -> Direction: pass


class CollisionAction(ABC):
    @abstractmethod
    def resolve(self, intruder:Sprite, where_to: Cell): pass


def eat_food(intruder:PacMan, target: Food, target_cell: Cell):
    intruder.points += target.food
    target_cell.my_sprites().remove(target)
    print("Food eaten")


def pacman_hits_ghost(intruder: PacMan, target: Ghost, target_cell: Cell):
    print("Your score:",intruder.points)
    intruder.n_lives -= 1
    if intruder.n_lives == 0:
        print("GAME OVER !!!")
    else:
        intruder.points = 0
        #  we should we jump the pacman to the starting point here?


class TravellingSprite(Sprite):
    def __init__(self, name: str, x: int, y: int, mover: SpriteMover):
        super().__init__(name, x, y)
        self._m: SpriteMover = mover
        self._resolvers = {}

    def add_resolver(self, sprite_type, func):
        self._resolvers[sprite_type] = func

    def move(self, b: Board):
        direction = self._m.propose_direction(self.x, self.y, b)
        nx = self.x + direction.value[0]
        ny = self.y + direction.value[1]
        # --- detect collisions here

        # --- if no collisions, make the move
        b.remove(self)
        self.x = nx
        self.y = ny
        b.insert(self)


class Cell(ABC):

    @abstractmethod
    def my_sprites(self) -> list[Sprite]:
        pass

    @abstractmethod
    def is_wall(self) -> bool: pass


@dataclass
class Board:
    __cells: list[list[Cell]]

    def at(self, x:int, y:int) -> Cell:
        return self.__cells[y][x]

    def insert(self, sprite:Sprite):

        if sprite.x >= self.width():
            raise Exception(f"X coordinate for sprite {sprite.name} is too large: {sprite.x}")
        if sprite.y >= self.height():
            raise Exception(f"Y coordinate for sprite {sprite.name} is too large: {sprite.y}")
        self.at(sprite.x, sprite.y).my_sprites().append(sprite)

    def remove(self, sprite:Sprite):
        self.at(sprite.x, sprite.y).my_sprites().remove(sprite)

    def width(self): return len(self.__cells[0])

    def height(self): return len(self.__cells)

    def is_wall(self, x: int, y: int):
        if x < 0 or y < 0: return True
        if x >= self.width() or y >= self.height(): return True
        return isinstance(self.__cells[y][x], Wall)

    def random_cell(self):
        x, y = 0, 0
        while self.is_wall(x, y):
            x = random.randint(1, self.width() - 2)
            y = random.randint(1, self.height() - 2)
        return x, y

    def available_moves(self, x: int, y: int):
        ret = []
        for d in moves:
            dx, dy = d.value
            nx = x + dx
            ny = y + dy
            if not self.is_wall(nx, ny): ret.append(d)

        return ret

    @staticmethod
    def board_from_asci(content: str) -> Board:
        # cell_factory = {' ': lambda : Passable(), '#': lambda : Wall()}
        cell_factory = {' ': lambda : Passable(), '#': make_wall }

        board = Board([])       # board.__cells = []

        # --- parse data into a list of cells
        for line in content.splitlines():
            row = []
            if len(line)==0: continue
            for cc in line.strip():
                row.append(cell_factory[cc]())
            board.__cells.append(row)

        return board


def make_wall(): return Wall()


class RandomWalk(SpriteMover):

    def propose_direction(self, x: int, y: int, board: Board) -> Direction:
        return random.choice(board.available_moves(x, y))


class PersistentWalk(SpriteMover):

    def __init__(self):
        self.__recent_direction = Direction.Nonsense

    def propose_direction(self, x: int, y: int, board: Board) -> Direction:
        possibilities = board.available_moves(x, y)
        print(possibilities)
        if self.__recent_direction not in possibilities:
            self.__recent_direction = random.choice(possibilities)
        return self.__recent_direction


class ManualWalk(SpriteMover):

    def __init__(self):
        self.the_direction = Direction.Stop

    def propose_direction(self, x: int, y: int, board: Board) -> Direction:

        last = self.the_direction
        self.the_direction = Direction.Stop
        return last


class Ghost(TravellingSprite):
    def __init__(self, name: str, x: int, y: int, mover: SpriteMover):
        super().__init__(name, x, y, mover)


class CrazyGhost(TravellingSprite):
    def __init__(self, name: str, x: int, y: int, movers: SpriteMover):
        super().__init__(name, x, y, movers[0])
        self.__movers = movers
        self.__cnt = 0

    def move(self, b: Board):
        self.__cnt += 1
        if self.__cnt % 100 == 0:
            self._m = random.choice(self.__movers)
        super().move(b)



@dataclass
class PacMan(TravellingSprite):
    points: int
    n_lives: int
    x_start: int
    y_start: int
    def __init__(self, name: str, x: int, y: int, mover: SpriteMover):
        super().__init__(name, x, y, mover)
        self.points = 0
        self.n_lives = 3
        self.x_start = x
        self.y_start = y


@dataclass
class Food(Sprite):
    food: int
    def __init__(self, name: str, x: int, y: int):
        super().__init__(name, x, y)
        self.food = random.randint(1,5)


class Wall(Cell):
    def my_sprites(self) -> list[Sprite]: return []

    def is_wall(self): return True


class Passable(Cell):

    def __init__(self):
        self._sprites = []

    def my_sprites(self) -> list[Sprite]: return self._sprites

    def is_wall(self): return False


def draw_asci_board(board: Board):
    for row in range(board.height()):
        out = f"{row:2} "
        for pos in range(board.width()):
            if isinstance(board.at(pos, row), Wall): out += "#"
            else:
                if len(board.at(pos, row).my_sprites()) > 0:
                    out += "o"
                else:
                    out += " "
        print(out)


if __name__ == "__main__":
    board = Board.board_from_asci(board_drawed)
    sprites = [
        Ghost("g1", *board.random_cell(), RandomWalk()),
        Ghost("g2", *board.random_cell(), RandomWalk()),
        Ghost("g3", *board.random_cell(), PersistentWalk()),
        Ghost("g4", *board.random_cell(), PersistentWalk()),
    ]
    for s in sprites: board.insert(s)

    draw_asci_board(board)

    for i in range(10):
        for i in range(1):
            for s in sprites:
                s.move(board)
                # print(s)
        draw_asci_board(board)

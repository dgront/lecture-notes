x, y = 0, 0

def move(step: str):
    global x, y
    if step == "N": y += 1  # go north
    elif step == "S": y -= 1  # go south
    elif step == "W": x -= 1  # go west
    elif step == "E": x += 1  # go east


walk_around = ["W", "W", "E", "N"]
for w in walk_around:
    move(w)
    print("current pos:", x, y)

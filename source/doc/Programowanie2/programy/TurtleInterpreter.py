class Turtle:
    def __init__(self, turtle=None):
        """Default turtle constructor or a copy c-tor, when a source turtle was provided"""
        if turtle:
            self.x, self.y = turtle.x, turtle.y
        else:
            self.x, self.y = 0, 0

    def __str__(self): return "%d %d" % (self.x, self.y)


class TurtleCommand:

    def __init__(self, n_steps=1):
        self.__n_steps = int(n_steps) # just to be sure it's not a string
        self.__last_turtle = None

    def __call__(self, turtle):
        self.__last_turtle = turtle

    @property
    def last_turtle(self):
        """a deep copy of the most recent turtle state"""
        return Turtle(self.__last_turtle)

    @property
    def n_steps(self): return self.__n_steps

    def undo(self, turtle):
        raise NotImplemented

    def __str__(self):
        raise NotImplemented


class TurtleCommandMaker:

    def __call__(self, *args): pass


class R(TurtleCommand):

    def __call__(self, turtle, *args):
        super().__call__(turtle, *args)  # delegate to the base class
        self.last_turtle.x += self.n_steps

    def undo(self):
        if self.last_turtle: self.last_turtle.x -= self.n_steps

    def __str__(self): return "R %d" % self.n_steps


class MakeR(TurtleCommandMaker):

    def __call__(self, *args): return R(*args)


class L(TurtleCommand):

    def __call__(self, turtle, *args):
        super().__call__(turtle, *args)
        self.last_turtle.x -= self.n_steps

    def undo(self):
        self.last_turtle.x += self.n_steps

    def __str__(self): return "L %d" % self.n_steps


class MakeL(TurtleCommandMaker):

    def __call__(self, *args): return L(*args)


class U(TurtleCommand):

    def __call__(self, turtle, *args):
        super().__call__(turtle, *args)
        self.last_turtle.y += self.n_steps

    def undo(self):
        self.last_turtle.y -= self.n_steps

    def __str__(self): return "U %d" % self.n_steps


class MakeU(TurtleCommandMaker):

    def __call__(self, *args): return U(*args)


class D(TurtleCommand):

    def __call__(self, turtle, *args):
        super().__call__(turtle, *args)
        self.last_turtle.y -= self.n_steps

    def undo(self):
        self.last_turtle.y += self.n_steps

    def __str__(self): return "D %d" % self.n_steps


class MakeD(TurtleCommandMaker):

    def __call__(self, *args): return D(*args)


class TurtleInterpreter:

    def __init__(self):
        self.__commands_factory = {}
        self.__turtle = Turtle()
        self.__cmd_stack = []
        self.__n_steps_default = 2

    def add_command(self, cmd_name, cmd_obj):
        self.__commands_factory[cmd_name] = cmd_obj

    @property
    def turtle(self): return self.__turtle

    def __call__(self, cmd_string, *args, **kwargs):

        if len(args) == 0: # the case when the whole command is a single string, like "U 3" or just "U"
            cmd = cmd_string.split()
            cmd_string = cmd[0]

            if len(cmd) > 1:
                args = cmd[1:]
            else:
                args = [self.__n_steps_default]
        cmd = self.__commands_factory[cmd_string](*args)
        cmd(self.turtle)
        self.__cmd_stack.append(cmd)

    def undo(self):
        cmd = self.__cmd_stack.pop()
        cmd.undo()

if __name__ == "__main__":

    interpreter = TurtleInterpreter()
    interpreter.add_command("D", MakeD())
    interpreter.add_command("U", MakeU())
    interpreter.add_command("R", MakeR())
    interpreter.add_command("L", MakeL())

    path = "U; L 3; D 3; R 4; U 2; D 6"
    for cmd in path.split(';') :
        interpreter(cmd)
        print(interpreter.turtle, cmd)

    interpreter.undo()
    interpreter.undo()
    print(interpreter.turtle)

    d = D(5)
    d(turtle)
    r = L(6)
    l(turtle)


class Vec2:

    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, x):
        self.__x = x


if __name__ == "__main__":
    v1 = Vec2(1.2, 3.4)
    v1.x = 8.2     # --- Here w e use @property
    print(v1.x)    # --- ... and here @x.setter

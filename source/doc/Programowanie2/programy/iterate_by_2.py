class EvenNumbers:

    def __init__(self,stop = 1000, start = 0) : 
        self.__i = start - 2
        self.__n = stop

    def __iter__(self) :
        return self

    def __next__(self):
        if self.__i < self.__n:
            self.__i += 2
            return self.__i
        else : raise StopIteration()

if __name__ == "__main__" :

    for k in EvenNumbers() : print(k)
x, y, = 0, 0                # initial position of a turtle

class TurtleCommand:

    def __init__(self, n_steps=1):
        """Base class for a turtle command"""
        self.__n_steps = int(n_steps)           # just to be sure it's not a string

    @property
    def n_steps(self): return self.__n_steps

class R(TurtleCommand):
    def __init__(self, n_steps):
        super().__init__(n_steps)

    def __call__(self):
        global x, y
        x += self.n_steps     # go right, turtle!

class L(TurtleCommand):
    def __init__(self, n_steps):
        super().__init__(n_steps)

    def __call__(self):
        global x, y
        x -= self.n_steps     # go left, turtle!

class F(TurtleCommand):
    def __init__(self, n_steps):
        super().__init__(n_steps)

    def __call__(self):
        global x, y
        y += self.n_steps     # go forward, turtle!

class B(TurtleCommand):
    def __init__(self, n_steps):
        super().__init__(n_steps)

    def __call__(self):
        global x, y
        y -= self.n_steps     # go back, turtle!


rectangle = [R(3), F(2), L(3), B(2)]
square = [R(2), F(2), L(2), B(2)]
for cmd in rectangle:
    cmd()
    print("current position:", x, y)


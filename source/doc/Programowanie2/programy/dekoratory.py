def greetings():

  return "Hello!"

def capitalize(func):

    def upercase_decorator():
        text = func()
        return text.upper()
    return upercase_decorator

uppercase_greetings = capitalize(greetings)
print("regular:  ", greetings())
print("decorated:", uppercase_greetings())

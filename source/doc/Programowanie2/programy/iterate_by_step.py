class EvenlySpacedFloats:

    __slots__ = [ '__from','__to','__step','__x']

    def __init__(self, begin, step, end):
        self.__from = begin
        self.__to = end
        self.__step = step
        self.__x = begin - self.__step 

    def __iter__(self) :
        return self

    def __next__(self):
        self.__x += self.__step 
        if self.__x < self.__to:
            return self.__x
        else : raise StopIteration()


if __name__ == "__main__" :
    for x in EvenlySpacedFloats(0.0,0.1,1.0) : 
        print(x)

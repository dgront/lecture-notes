x, y, = 0, 0                # initial position of a turtle

class R:
    def __call__(self):
        global x, y
        x += 1     # go right, turtle!

class L:
    def __call__(self):
        global x, y
        x -= 1     # go left, turtle!

class F:
    def __call__(self):
        global x, y
        y += 1     # go forward, turtle!

class B:
    def __call__(self):
        global x, y
        y -= 1     # go back, turtle!

class R2:
    def __call__(self):
        global x, y
        x += 2     # go right faster, turtle!


user_typed_keys = "RRFLB"
r = R()
l = L()
b = B()
f = F()
r = R2()                        # Actually, let's go right faster
for key in user_typed_keys:
    if key == 'R': r()
    elif key == 'L': l()
    elif key == 'F': f()
    elif key == 'B': b()
    else: print("unused key:", key)
    print("current position:", x, y)


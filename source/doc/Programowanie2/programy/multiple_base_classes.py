class BaseOne:
    def __init__(self):
        print("BaseOne")
        # super().__init__()
        self.__e = 1

    def __str__(self):
        return "One has: "+str(self.__e)

    def e(self): return self.__e


class BaseTwo:
    def __init__(self):
        print("BaseTwo")
        super().__init__()
        self.__e = 2
        # print(dir(self))

    def __str__(self):
        return "One has: "+str(self.__e)

    def e(self): return self.__e


class Derived(BaseTwo, BaseOne):
    def __init__(self):
        print("zaczynam Derived")
        super().__init__()
        self.__e = 12
        print("koniec Derived")

    def __str__(self):
        return "Derived has: "+str(self.__e)

    def e(self): return self.__e

d = Derived()
# print(BaseOne.e(d))
# print(BaseTwo.e(d))
# print(d.e())
# print(d)
print(dir(d))
d.f = 5
print(d.f)
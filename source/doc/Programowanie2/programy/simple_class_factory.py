class Product:
    def product_feature(self, *args):
        raise NotImplemented

class Asterix(Product):

    def __init__(self, n):
        self.__n = n

    def product_feature(self, *args):
        print("*" * self.__n)

class Dash(Product):

    def __init__(self, n):
        self.__n = n

    def product_feature(self, *args):
        print("-" * self.__n)

class ProductFactory:

    def produce(self, what, params):
        if what == '*': return Asterix(params)
        elif what == '-': return Dash(params)
        else:
            print("unknown product")
            return None

if __name__ == "__main__":

    factory = ProductFactory()

    products = []
    for obj, cnt in [('*', 5), ('-', 2), ('-', 1), ('*', 5), ('-', 2)]:
        products.append(factory.produce(obj,cnt))

    for p in products: p.product_feature()

def capitalize(func):
    def upercase_decorator(name):
        text = func(name)
        return text.upper()

    return upercase_decorator

@capitalize
def name_yourself(name: str) :
  return "Hello, my name is " + name

# uppercase_greetings = capitalize(name_yourself)

print(name_yourself("Jacek"))
print(name_yourself("Agatka"))

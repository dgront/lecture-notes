from abc import ABC, abstractmethod

class EncryptionEngine(ABC):

    @abstractmethod
    def cipher(self, message: str) -> str: pass

    @abstractmethod
    def decipher(self, message: str) -> str: pass

class Encryptor:

    def __init__(self, standard_encryption: EncryptionEngine):
        self.__strategy = standard_encryption

    @property
    def encryption_engine(self) -> EncryptionEngine:
        return self.__strategy

    @encryption_engine.setter
    def encryption_engine(self, new_engine: EncryptionEngine):
        self.__strategy = new_engine

    def cipher(self, message: str):
        m = message.replace(" ","stop").upper()
        return self.__strategy.cipher(m)

    def decipher(self, message: str):
        m = self.__strategy.decipher(message)
        return m.replace("STOP", " ")


class Plaintext(EncryptionEngine):

    def cipher(self, message: str): return message

    def decipher(self, message: str): return message


class CeasarCipher(EncryptionEngine):

    def __init__(self,key):
        self.__key = key

    def __process(self, message: str, key):
        result = ""
        for s in message:
            result += chr((ord(s) + key - 65) % 26 + 65)
        return result

    def decipher(self, message: str):
        return self.__process(message, self.__key)

    def cipher(self, message: str):
        return self.__process(message, -self.__key)


if __name__ == "__main__":

    msg = "THIS IS TOP SECRET"
    eng = Encryptor(Plaintext())
    code = eng.cipher(msg)
    print(msg, "->", code, "->", eng.decipher(code))

    eng.encryption_engine = CeasarCipher(4)
    code = eng.cipher(msg)
    print(msg, "->", code, "->", eng.decipher(code))


from typing import List

def every_second_element[T, G](data: List[T]) -> List[G]:
    output = [elem for (i, elem) in enumerate(data) if i % 2 == 0]
    return output

if __name__ == "__main__":
    out = every_second_element([1, 2, 3, 4, 5, 6])
    out = every_second_element([1.0, 2.0, 3.0, 4.0, 5.0, 6.0])
    dodaj(4.0, 3.0)
    print(out)

from typing import TypeVar, Generic

T = TypeVar('T')


class Registry(Generic[T]):
    def __init__(self):
        self.__objects = []

    def register(self, elem: T) :
        self.__objects.append(elem)

    def get_element(self, indx: int) -> T:
        return self.__objects[indx]

    def count(self) -> int:
        return len(self.__objects)


if __name__ == "__main__":
    r = Registry()
    r.register("Grzybowa")
    r.register("Rosół")
    r.register(3.0)
    print(r.count())
#include <iostream>
#include <vector>


std::vector<double> every_second_element(const std::vector<double>& data) {
    std::vector<int> output;
    for (int i = 0; i < data.size(); i += 2) {
        output.push_back(data[i]);
    }
    return output;
}

std::vector<float> every_second_element(const std::vector<float>& data) {
    std::vector<float> output;
    for (int i = 0; i < data.size(); i += 2) {
        output.push_back(data[i]);
    }
    return output;
}

std::vector<int> every_second_element(const std::vector<int>& data) {
    std::vector<int> output;
    for (int i = 0; i < data.size(); i += 2) {
        output.push_back(data[i]);
    }
    return output;
}

int main() {
    std::vector<int> out1 = every_second_element({1, 2, 3, 4, 5, 6});
    std::vector<float> out2 = every_second_element({1.0, 2.0, 3.0, 4.0, 5.0, 6.0});

    for (int i : out1) { std::cout << i << " "; }
    std::cout << std::endl;

    return 0;
}

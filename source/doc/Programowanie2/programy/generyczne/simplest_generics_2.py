from typing import List, TypeVar, Any

T = TypeVar('T')

def every_second_element(data: List[T]) -> List[T]:
    output = [elem for (i, elem) in enumerate(data) if i % 2 == 0]
    return output

def every_second_element_any(data: List[Any]) -> List[Any]:
    output = [elem for (i, elem) in enumerate(data) if i % 2 == 0]
    return output


if __name__ == "__main__":
    out = every_second_element([1, 2, 3, 4, 5, 6])
    out = every_second_element([1.0, 2.0, 3.0, 4.0, 5.0, 6.0])
    print(out)

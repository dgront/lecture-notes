#include <iostream>
#include <vector>

template<typename T>
std::vector<T> every_second_element(const std::vector<T>& data) {
    std::vector<T> output;
    for (int i = 0; i < data.size(); i += 2) {
        output.push_back(data[i]);
    }
    return output;
}

int main() {
    std::vector<int> out1 = every_second_element<int>({1, 2, 3, 4, 5, 6});
    std::vector<float> out2 = every_second_element<float>({1.0, 2.0, 3.0, 4.0, 5.0, 6.0});
//    std::vector<double> out3 = every_second_element<double>({1.0, 2.0, 3.0, 4.0, 5.0, 6.0});

    for (int i : out1) { std::cout << i << " "; }
    std::cout << std::endl;

    return 0;
}

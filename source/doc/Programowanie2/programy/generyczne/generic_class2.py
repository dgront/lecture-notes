from abc import ABC, abstractmethod
from typing import TypeVar, Generic

T = TypeVar('T')


class Vehicle(ABC):
    def __init__(self, vin: str):
        self.__vin = vin

    def vin(self) -> str: return self.__vin


class Bus(Vehicle):

    def __int__(self, vin: str):
        super.__init__(vin)
        self.__max_passengers = 30

    @property
    def max_passengers(self) -> int: return self.__max_passengers


class Truck(Vehicle):

    def __int__(self, vin: str):
        super.__init__(vin)
        self.__capacity = 10.0


    @property
    def capacity(self) -> float: return self.__capacity


V = TypeVar('V', bound=Vehicle)

class Registry(Generic[V]):
    def __init__(self):
        self.__objects = []

    def register(self, elem: V):
        self.__objects.append(elem)

    def get_element(self, indx: int) -> V:
        return self.__objects[indx]

    def count(self) -> int:
        return len(self.__objects)


if __name__ == "__main__":
    r = Registry()
    r.register("Grzybowa")
    truck = Truck("VIN number")
    r.register(truck)
    print(r.count())
template<unsigned int S>
struct PointNDim {
    double coordinates[S];
    double dot(PointNDim<4> &p) { return 0.0; }
};

int main() {
    // pass 4 as argument.
    PointNDim<4> xyzt = {1, 2, 3, 4};
    PointNDim<3> xyz = {1, 2, 3};

    std::vector<float> point4{0,0,0,0};

    return 0;
}
from typing import TypeVar, Generic

T = TypeVar('T')

class Fraction(Generic[T]):
    def __init__(self, n: T, d: T):
        self.__nominator = n
        self.__denominator = d

    @property
    def nominator(self) -> T:
        return self.item

    @property
    def denominator(self) -> T:
        return self.item

    def __str__(self):
        return f"{self.__nominator} / {self.__denominator}";


if __name__ == "__main__":

    f = Fraction(2,4)
    print(f)
def intro(func):
    def intro_decorator(name):
        return "And now let us welcome our new colleague... " \
               + name + "\n\t" + func(name)

    return intro_decorator

def capitalize(func):
    def upercase_decorator(name):
        text = func(name)
        return text.upper()

    return upercase_decorator

@intro
@capitalize
def name_yourself(name: str) :
  return "Hello, my name is " + name

# uppercase_greetings = capitalize(name_yourself)

print(name_yourself("Jacek"))
print(name_yourself("Agatka"))

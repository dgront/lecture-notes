import random

class ValidElementIterator:

    def __init__(self, elements):
        self.__internal_list = elements
        self.__find_next(0)      # Initialise the iterator so it points at the very first element to return

    def __next__(self):
        if self.__next_element_index is None:
            raise StopIteration()
        else:
            ret = self.__internal_list[self.__next_element_index]
            if self.__next_element_index == len(self.__internal_list) - 1 :
                self.__next_element_index = None
            else:
                self.__find_next(self.__next_element_index + 1)
            return ret

    def __find_next(self, start):

        # --- continue the search until a valid element is found
        while self.__internal_list[start] is None:
            start += 1
            if start == len(self.__internal_list):
                self.__next_element_index = None
                return
        self.__next_element_index = start


class VeryBigContainer:
    __slots__ = ['__internal_list', '__elements', '__current', '__next_element_index']

    def __init__(self):
        self.__internal_list: list[str] = []
        self.__next_element_index = None

    def insert(self, new_element: str):
        self.__internal_list.append(new_element)

    def __iter__(self):
        return ValidElementIterator(self.__internal_list)


if __name__ == "__main__":
    letters = "ABCDEFGHIJKLMNOP"

    # ---------- Create a container with letters mixed with None values
    data = VeryBigContainer()
    for i in range(15):
        if random.choice([True, False]):
            data.insert(random.choice(letters))
        else:
            data.insert(None)

    print("# --- iterating in double-nested loop")
    # ---------- iterate over the not-None elements of the very big container
    for el_i in data:
        for el_j in data:
            print(el_i, el_j)

class CustomOrderIterator:
    __slots__ = ['__source_list', '__elements', '__current']

    def __init__(self, source_list, elements):
        self.__source_list = source_list
        self.__elements = elements
        self.__current = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.__current += 1
        if self.__current <= len(self.__elements):
            return self.__source_list[self.__elements[self.__current-1]]
        else:
            raise StopIteration()


if __name__ == "__main__":
    letters = list("ABCDEFGHIJKLMNOP")
    order = [2, 3, 4, 1, 8, 0, 2, 2]
    for x in CustomOrderIterator(letters, order):
        print(x)

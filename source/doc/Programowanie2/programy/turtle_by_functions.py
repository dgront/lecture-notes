x, y, = 0, 0                # initial position of a turtle

def r():
    global x, y
    x +=1     # go right, turtle!

def l():
    global x, y
    x -=1     # go left, turtle!

def f():
    global x, y
    y +=1     # go forward, turtle!

def b():
    global x, y
    y -=1     # go back, turtle!

user_typed_keys = "RRFLB"
for key in user_typed_keys:
    if key == 'R': r()
    elif key == 'L': l()
    elif key == 'F': f()
    elif key == 'B': b()
    else: print("unused key:", key)
    print("current position:", x, y)

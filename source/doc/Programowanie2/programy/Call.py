class FunctionLike:
    def __call__(self, a):
        print("I got called with %r!" % (a))

fn = FunctionLike()
fn(10)

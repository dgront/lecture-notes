def residue_no(line:str, beg_pos:int, end_pos: int) -> int:
    return int(line[beg_pos:end_pos].strip())


def read_ter_record(ter_line: str):
    res_id = residue_no(ter_line, 22, 26)
    res_name = ter_line[17:20]
    chain_id = ter_line[21]
    return chain_id,res_name,res_id


if __name__ == "__main__":
    ter = read_ter_record("TER    1070      ARG A1141")
    print(ter)
    ter = read_ter_record("TER                       ")

from typing import Tuple


class MoveN:
    def __call__(self, x, y): return x, y+1

class MoveS:
    def __call__(self, x, y): return x, y-1

class MoveW:
    def __call__(self, x, y): return x-1, y

class MoveE:
    def __call__(self, x, y): return x+1, y

class MoveSW:
    def __call__(self, x, y): return x-1, y-1

dispatch = {"N": MoveN(), "E": MoveE(), "S": MoveS(), "W": MoveW(), "SW": MoveSW(), "WS": MoveSW()}

def move(step: str, location: Tuple[int, int]):
    if step in dispatch:
        return dispatch[step](location[0],location[1])
    else:
        print("Unknown direction!")

walk_around = ["W", "W", "E", "N", "SW", "WS"]
loc = (0, 0)
for w in walk_around:
    loc = move(w, loc)
    print("current pos:", loc)

class LJ:

    def __init__(self,xy_i, xy_j, epsilon, sigma) :
        self.xy_i = xy_i
        self.xy_j = xy_j
        self.epsilon4 = epsilon * 4
        self.sigma = sigma
        self.sigma2 = sigma * sigma

    def evaluate(self):
        d = self.xy_i[0] - self.xy_j[0]
        d2 = d*d
        d = self.xy_i[1] - self.xy_j[1]
        d2 += d*d
        d2 = self.sigma2 / d2
        d6 = d2*d2*d2
        d12 = d6*d6

        return self.epsilon4 * (d12 - d6)

xy_1 = [0.0,0.0]
xy_2 = [10.0,0.0]
en = LJ(xy_1, xy_2, 0.004, 3.4)

for i in range(0,70) :
    xy_1[0] += 0.15
    print(en.evaluate())

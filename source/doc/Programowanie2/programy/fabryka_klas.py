class Komenda:
    def __init__(self,n):
        self.n = n

    def do_it(self):
        pass

class Maker:

    def make(self):
        pass

class Gora(Komenda):
    def __init__(self,n):
        super().__init__(n)

    def do_it(self):
        print("do gory o",self.n)

    def __str__(self):
        return "U"+str(self.n)

class ZrobWGore(Maker):
    def __init__(self):
        print("Wczytuje dlugi plik o tym, jak isc w gore")

    def make(self, n):
        return Gora(n)

class Dol(Komenda):
    def __init__(self,n):
        super().__init__(n)

    def do_it(self):
        print("do dolu o",self.n)

    def __str__(self):
        return "D"+str(self.n)


class ZrobWDol(Maker):
    def __init__(self):
        print("Wczytuje dlugi plik o tym, jak isc w dol")

    def make(self, n):
        return Dol(n)


class Fabryka:
    def __init__(self):
        self.dozwolone_komendy = {}

    def rejestruj(self, nazwa, akcja):
        nazwa = nazwa.upper()
        self.dozwolone_komendy[nazwa] = akcja

    def make(self, nazwa, n):
            return self.dozwolone_komendy[nazwa].make(n)


d = Fabryka()
d.rejestruj("U", ZrobWGore())
d.rejestruj("D", ZrobWDol())

akcje = []
for nazwa in ['U','D']:
    for i in range(1,6):
        akcje.append(d.make(nazwa, i))

for a in akcje:
    print(a)

# akcje = {"U": Gora(), "D": Dol(), "L": Lewo()}
# akcje["r"] = Prawo()
#
# komendy = "UUDULRRLUDRDRDRUULLD"
#
# for cmd in komendy:
#     akcje[cmd].do_it()

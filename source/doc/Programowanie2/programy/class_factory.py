class Product:
    def product_feature(self, *args):
        raise NotImplemented

class Asterix(Product):

    def __init__(self, n):
        self.__n = n

    def product_feature(self, *args):
        print("*" * self.__n)

class Dash(Product):

    def __init__(self, n):
        self.__n = n

    def product_feature(self, *args):
        print("-" * self.__n)

class ObjectMaker:
    def make(self, *args):
        raise NotImplemented

class AsterixMaker(ObjectMaker):

    def make(self, *args):
        print("AsterixMaker called", file=stderr)
        if len(args) > 0 : return Asterix(args[0])
        else: return Asterix(1)

    def __str__(self): return "AsterixMaker"

class DashMaker(ObjectMaker):

    def make(self, *args):
        print("DashMaker called", file=stderr)
        if len(args) > 0: return Dash(args[0])
        else: return Dash(1)

    def __str__(self): return "DashMaker"

class ProductFactory:

    def __init__(self):
        self.__makers = {}

    def register_product(self, name, maker):
        self.__makers[name] = maker

    def produce(self, what, params):
        return self.__makers[what].make(params)


if __name__ == "__main__":

    factory = ProductFactory()
    factory.register_product("-", DashMaker())
    factory.register_product("*", AsterixMaker())

    products = []
    for obj, cnt in [('*', 5), ('-', 2), ('-', 1), ('*', 5), ('-', 2)]:
        products.append(factory.produce(obj,cnt))

    for p in products: p.product_feature()

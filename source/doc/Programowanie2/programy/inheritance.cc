#include <iostream>
#include <vector>

class BaseOne {
public:
    BaseOne() { n_ = 1;}
    int n() { return n_; }
private:
    int n_;
};

class BaseTwo {
public:
    BaseTwo() { n_ = 2;}
    int n() { return n_; }
private:
    int n_;
};

class Derived : public BaseOne, public BaseTwo {
public:
    Derived() { n_ = 12;}
    int n() { return n_; }
private:
    int n_;
};

int main(void) {
  BaseTwo b2;
  BaseOne b1;
  Derived d;

//  std::cout << b1.n() << " "<<b2.n() << " "<<d.n()
//  << " "<<d.BaseOne::n()<< " "<<d.BaseTwo::n()<<"\n";
//
//  BaseTwo & b2_copy = d;
//  std::cout <<b2_copy.n() <<"\n";

  std::vector<BaseOne> lista;
  lista.push_back(b1);
//  lista.push_back(b2);
  lista.push_back(d);
  for(auto & li:lista)
    std::cout <<li.n() <<"\n";
}
class Rnd :

  def __init__(self, start = 1) :
    self.__a = 8121   # Paramers from Wikipedia, random0 set
    self.__c = 28411
    self.__m = 134456
    self.start = start

  def eval(self) :
    self.start = (self.__a * self.start + self.__c) % self.__m
    return self.start
    
  def rand_max(self) : return self.__m - 1

if __name__ == "__main__" :
  rnd = Rnd()
  for i in range(10) : print(rnd.eval())
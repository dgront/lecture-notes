.. _Pytonowe1:

(Niezbyt) oczywiste elementy Pythona
----------------------------------------

Niniejszy kurs zakłada podstawową znajomość języka Python. Czytelnik nie znajdzie tu opisu jak tworzyć pętle
czy instrukcje warunkowe. Aby uzupełnić wiedzę w tym zakresie polecam lekturę moich notatek
do :ref:`Podstaw programowania w języku Python <programowanie_1>`
Na początku jednak przypomnę pewne mniej oczywiste elementy tego języka, które będą bardzo przydatne przy omawianiu programowania obiektowego.

`args` i `kwargs`
^^^^^^^^^^^^^^^^^^^^^

Jednym z ważnych mechanizmów programowania obiektowego jest **przeciążenie** funkcji. Zaczniemy to wykorzystywać
już niedługo. Jedynym sposobem implementacji tego mechanizmu w Pythonie jest wykorzystanie `args` i `kwargs`.
Opisano je :ref:`na tej stronie <Pythonalia>`


Zmienne istnieją od momentu pierwszego przypisania
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To w zasadzie powinno być oczywiste, stanowi podstawę programowania w języku Python. Dla przypomnienia jednak powtórzmy:
w odróżnieniu od języków kompilowanych takich jak Java czy C++, w Pythonie nie deklarujemy zmiennych. Zmienne tworzymy
przez nadawanie wartości.

Zmienne globalne i lokalne
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Zmienne mogą być lokalne lub globalne. Róznice między nimi opisano :ref:`na tej stronie <zmienne_lokalne_i_globalne>`



Wszystko jest tym czym jest ...
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`... no chyba, że jest referencją tego czegoś`

Przeanalizuj poniższy przykład, który `w zasadzie` powinien stworzyć macierz 3x3 z elementami 1, 2 i 3:

 .. raw:: html

  <div id="referencje"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw2 = ScriptWidget("referencje.py","referencje", height=130)   
  </script>

Co ten program wydrukuje na ekranie? Dlaczego? Co się zmieni po odkomentowaniu linii nr 4?

Referencje bywają jednak bardzo przydatne. Dzięki temu zachowanie danego obiektu może zależeć od stanu innego obiektu. W poniższym przykładzie obiekt klasy ``LJ``, obliczającego energię Lennard-Jonesa, *śledzi* położenia przemieszczających się atomów.
Dzięki temu nie trzeba przekazywać ich jako argumentów funkcji przy każdym wywołaniu.
 
 .. raw:: html

  <div id="LJ_simpler"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw3 = ScriptWidget("LJ_simpler.py","LJ_simpler", height=440)
  </script>

Funkcja tak jak zmienna
^^^^^^^^^^^^^^^^^^^^^^^^^

Funkcję w języku Python można przekazać jako argument, jak na przykład poniżej:

 .. raw:: html

  <div id="Call"></div>
  <script type="text/python">
    from ScriptWidget import ScriptWidget
    sw4 = ScriptWidget("Call.py","Call", height=120)
  </script>

Funkcje można trzymać w listach i słownikach tak, jak inne zmienne. Dzięki temu możliwe jest np pokazane poniżej 
podejście do zorganizowania bloku decyzyjnego (ang *dispatch*). Najpierw "klasyczne" podejście, z mega-długim
blokiem ``if ... else``:

  .. literalinclude:: process_files_dumb.py
     :language: python
     :linenos:
     :caption: process_files_dumb.py
     :name: process_files_dumb-py

A teraz ze słownikiem funkcji:

  .. literalinclude:: process_files.py
     :language: python
     :linenos:
     :caption: process_files.py
     :name: process_files-py

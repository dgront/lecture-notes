from math  import exp
from random import random

from SSPrediction import SSPrediction
from MutateSSMover import MutateSSMover
from HELengthScore import HELengthScore

ss_pred = SSPrediction("MTYKLILNGKTLKGETTTEAVDAATAEKVFKQYANDNGVDGEWTYDDATKTFTVTE")
mover = MutateSSMover(ss_pred)
energy = HELengthScore("HE-len.dat",ss_pred)

temperature = 0.00001

for i in range(100) :
    e_before = energy()
    mover()
    print(ss_pred)
    e_after = energy()
    delta_e = e_after - e_before
    if delta_e > 0 and random() > exp(-delta_e/temperature) :
    	mover.undo()

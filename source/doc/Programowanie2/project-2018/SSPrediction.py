class SSPrediction:

    def __init__(self,aa_sequence) :
        self.__sequence = aa_sequence
        self.__predicted_ss = "C" * len(aa_sequence)

    @property
    def sequence(self) : return self.__sequence
    
    @property
    def predicted_ss(self) : return self.__predicted_ss

    @predicted_ss.setter
    def predicted_ss(self, predicted_ss) : self.__predicted_ss = predicted_ss
    
    def __str__(self) :
      return self.__sequence + "\n" + self.__predicted_ss
    
    
if __name__ == "__main__" :

  sspred = SSPrediction("AIKLISALKG")
  print(sspred)

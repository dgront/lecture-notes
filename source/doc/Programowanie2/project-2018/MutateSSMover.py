import random
from SSPrediction import SSPrediction

class MutateSSMover:

    known_ss_types = ['H','E','C']
    
    def __init__(self, ss_prediction):
      self.__ss_prediction = ss_prediction
      self.__ss_str_before = ss_prediction.predicted_ss
      
    def __call__(self) :
      k = random.randint(0, len(self.__ss_prediction.sequence) - 1)
      n = random.randint(0,len(MutateSSMover.known_ss_types)-1)
      self.__ss_prediction.predicted_ss = \
        self.__ss_prediction.predicted_ss[0:k] + MutateSSMover.known_ss_types[n] + self.__ss_prediction.predicted_ss[k+1:]
    
    def undo(self) :
      self.__ss_prediction.predicted_ss  = self.__ss_str_before
      
if __name__ == "__main__": 

  sspred = SSPrediction("AIKLISALKG")
  print(sspred)
  
  mover = MutateSSMover(sspred)
  for i in range(10) : mover()
  print(sspred)

from math import log

from ScoreBase import ScoreBase

class HELengthScore(ScoreBase):

    def __init__(self,plik,predicted_ss):
        ScoreBase.__init__(self, predicted_ss)
        self.__count_H=[]
        self.__count_E=[]
        self.__predicted_ss=predicted_ss
        self.__probability_l=[]
        for line in open(plik):       #tworzenie list o indeksie rownym dlugosci i wartosi rownej liczbie wystapien
            if line[0] == '#': continue
            tokens=line.strip().split()
            self.__count_H.append(int(tokens[1]))
            self.__count_E.append(int(tokens[2]))
            
    
    def __call__(self) :

        self.probability(self.__predicted_ss.predicted_ss)
        total = 0
        for i in range (len(self.__probability_l)):
            total -= log(self.__probability_l[i][0])
        print(total)
        return total

    def probability (self,predicted_ss):
        omega_H = 0
        omega_E = 0
        for i in range (len(self.__count_H)):      #omegi do prawdopodobienstwa
            omega_H += self.__count_H[i]
            omega_E += self.__count_E[i]
        l_ss=list(predicted_ss)
        lista_1=[]
        ss_final=[]
        for i in range(len(l_ss)):      #tworzenie listy, ktrej elementami sa ciagi takich samych liter wystepujacych po sobie
            if i==0 or l_ss[i]==l_ss[i-1]:
                lista_1.append(l_ss[i])
        
            else:
                ss_final.append("".join(lista_1))
                lista_1=[]
                lista_1.append(l_ss[i])
                
        for i in range(len(l_ss)):      #tworzenie liaty o lementach [ prawdopodobienstwo ciagu o danej dlugosci danego typu, typ drugorzedowy, dlugosc ciagu]
            if l_ss[i][0]=="H":
                self.__probability_l.append([self.__count_H[len(l_ss[i])]/omega_H,"H",len(l_ss[i])])
            elif  l_ss[i][0]=="E" :
                self.__probability_l.append([self.__count_E[len(l_ss[i])]/omega_E,"E",len(l_ss[i])])
            else:
                pass

        
        
        

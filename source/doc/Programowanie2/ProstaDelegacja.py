class InnerClass:

    def my_method(self):
        return "metoda wewnętrzna"

class SimpleDelegation:

    def __init__(self,inner):
        self.__inner = inner

    def my_method(self):
        return "nadpisana " + self.__inner.my_method()


if __name__ == "__main__" :

    ic = InnerClass()
    sd = SimpleDelegation(ic)
    for obct in [ic,sd] :
        print(obct.my_method())


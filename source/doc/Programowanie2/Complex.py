class Complex:

    __slots__ = [ '__re','__im']

    def __init__(self, re,im):
        self.__re = re
        self.__im = im
    
    def __str__(self) :
        return "%f + %fi" % (self.__re, self.__im)

    def __iadd__(self,c2) :
        
        if isinstance(c2, Complex) : # the interesting case when c2 is complex
            self.__re += c2.__re
            self.__im += c2.__im
        else :
            self.__re += c2 # the case when c2 is just a real value
        return self

if __name__ == "__main__" :
    c1 = Complex(1.2, 3.0)
    c2 = Complex(0.8, -2.0)
    c1 += c2
    print(c1)


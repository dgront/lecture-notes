Singleton
-------------------------------------

Wzorzec ten wymusza istnienie tylko jednego obiektu danej klasy. Jeżeli nie stworzono ani jednego, wywoływany jest konstruktor. W przeciwnym przypadku zwracana jest referencja do tego jedynego, który już istnieje:

.. literalinclude:: Singleton.py
  :language: python
  :linenos:

Powtórne wywołanie konstruktora powoduje podniesienie wyjątku. W *prawdziwie obiektowych* językach jak Java albo C++ możliwe jest zadeklarowanie konstruktora jako prywatnej składowej klasy. Wtedy to jedynie metoda ``get_instance()`` jest wstanie wywołać konstruktor.

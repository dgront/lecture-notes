class TajnePoleW :

  def __init__(self) :
    self.v = 5    # składowa publiczna
    self.__w = 7  # składowa prywatna
  
  def print_w(self, other_c) :
    print(other_c.__w)

# Metoda nadpisuje składową v
  def set_v(self, v) : self.v = v

# Metoda nadpisuje składową w
  def set_w(self, w) : self.__w = w

if __name__ == "__main__" :
  c = TajnePoleW()
  print(c.v)    # To działa, gdyż pole v jest publiczne
#  print(c.__w) # TO NIE ZADZIAŁA, pole __w jest prywatne

# obiekt d jest tej samej klasy co c, może odczytać jego wartość __w
  d = TajnePoleW()
  d.print_w(c)


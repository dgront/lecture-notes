from sys import argv
from os.path import splitext

# Actually, thi function may be impored from a separate module devised for PDB processing
def process_pdb(file_name) : 
  	print("processing a PDB file: ",file_name)
  	# ... many, many lines of code that are necessary to parse PDB

def process_fasta(file_name) : # Also might be imported 
  	print("processing a FASTA file: ",file_name)
  	# ... many, many lines of code that are necessary to parse FASTA

def process_aln(file_name) : # Also might be imported 
  	print("processing MSA data in ALN format from file: ",file_name)
  	# ... many, many lines of code that are necessary to parse ALN

dispatch = { ".pdb" : process_pdb, ".fasta" : process_fasta, ".aln" : process_aln }

if __name__ == "__main__" :
 	for fname in argv[1:] : 
 		name, extension = splitext(fname)
 		if extension in dispatch : dispatch[extension](fname)
 	else : print("Unknown file format: ",extension)


Komenda (*Command Pattern*)
==============================================

.. note::
   Komenda (Polecenie) to obiektowy odpowiednik wywołania funkcji

Komenda (zwana też poleceniem) to obiekt, który przechowuje w sobie dane potrzebne do wykonania swojego kodu (czyli komendy).
Komendy - jak każde obiekty - można przechowywać w liście. Wzorzec ten umożliwia np. elastyczną zmianę działania programu
w trakcie jego trwania.

Aby zademonstrować ten wzorzec (a także i kilka innych, na kolejnych stronach), napiszmy program, który przemieszcza żółwia
w dwóch wymiarach. Zacznijmy od czegoś prostego: połozenie żółwia zapisane jest w globalnych zmiennych ``x`` i ``y``.
Może się on przemieszczać w jednym z czterech kierunków: do przodu, do tyłu, w prawo i w lewo, ruchy te są realizowane
odpowiednio przez funkcje ``f()``, ``b()``, ``r()`` i ``l()``:

.. literalinclude:: programy/turtle_by_functions.py
  :language: python
  :linenos:

Załóżmy, że użytkownik wcisnął sekwencję klawiszy jak zdefiniowano w zmiennej ``user_typed_keys`` (linia 19),
a żółw poruszył się stosownie do komend. Program działa poprawnie, jest to jednak podejście mało rozwojowe.


Obiekt komenda
---------------

Stwórzmy oddzielny obiekt dla każdej możliwości ruchu żółwia:

.. literalinclude:: programy/turtle_by_objects.py
  :language: python
  :linenos:

Na pierwszy rzut oka wydaje się, że jedyne, co osiągnęliśmy, to wydłużenie programu, który robi to samo, co poprzedni.
Zauważ jednak, że w linii 34 podmieniamy komendę ruchu w prawo na inną: ``r = R2()``, dzięki czemu żółw w prawo porusza
się dwa razy szybciej niż w innych kierunkach.

Obiekty mogą coś, czego nie mogą funkcje: przechowywać dane. Poniżej przedstawiam drugie podejście do komend żółwia;
tym razem każda komenda wie, o ile kroków ma się ruszyć żółw. Tym samym klasa ``R2`` przestała być potrzebna.
Ponieważ *każda* komenda posiada tą nową funkcjonalność, została ona dodana do klasy bazowej ``TurtleCommand``, po której
dziedziczą wszystkie komendy.

.. literalinclude:: programy/turtle_by_objects2.py
  :language: python
  :linenos:

Zauważ dodatkowo, że komendy-obiekty można łatwo przechowywać w listach.

class Paper:
    def name(self): return "Paper"

class Rock:
    def name(self): return "Rock"

class Scissors:
    def name(self): return "Scissors"

def play(player1, player2):
    if isinstance(player1, Rock) and isinstance(player2, Rock):
        return "draw"
    elif isinstance(player1, Paper) and isinstance(player2, Paper):
        return "draw"
    elif isinstance(player1, Scissors) and isinstance(player2, Scissors):
        return "draw"
    elif isinstance(player1, Rock) and isinstance(player2, Paper):
        return "player 2 won"
    elif isinstance(player1, Paper) and isinstance(player2, Rock):
        return "player 1 won"
    elif isinstance(player1, Scissors) and isinstance(player2, Rock):
        return "player 2 won"
    elif isinstance(player1, Rock) and isinstance(player2, Scissors):
        return "player 1 won"
    elif isinstance(player1, Scissors) and isinstance(player2, Paper):
        return "player 1 won"
    elif isinstance(player1, Paper) and isinstance(player2, Scissors):
        return "player 2 won"
    else: return "UNKNOWN PLAYER"

if __name__ == "__main__":
    p1 = Scissors()
    p2 = Paper()
    print(p1.name(),"versus",p2.name()," : ",play(p1,p2))



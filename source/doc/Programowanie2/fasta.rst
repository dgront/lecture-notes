Implementacja algorytmu FASTA 
-------------------------------

FASTA to heurystyczny algorytm znajdujący statystycznie istotne uliniowienia lokalne pomiędzy sekwencjami białek bądź kwasów nukleinowych.
Metoda ta opiera się na znajdowaniu identycznych bądź prawie identycznych podciągów znaków (oligopeptydów). Na ich podstawie budowane jest następnie ulinowienie lokalne wg zmodyfikowanego algorytmu Smitha-Watermana. Wyszukiwanie pasujących fragmentów sekwencji (ang. *match*) zaczyna się od znalezienia identycznych fragmentów k-literowych (*k-tuple*), najczęściej k = 3

Przykładowy plik z danymi wejściowymi można pobrać z `tego miejsca <https://bitbucket.org/dgront/bioshell/raw/57ba8b52b5e759e327431dd28a80d5632a3cc39b/doc_examples/example-inputs/fasta/unique100.fasta>`_.

Etapy algorytmu i ich implementacja
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Wczytanie bazy sekwencji 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Wczytaj  sekwencje wejściowe z pliku w formacie ``.fasta``. Przyjmij, że każda sekwencja zapisana jest w dokładnie jednej linii. Sekwencje wczytaj do listy jako dwuelementowe krotki ``(nagłówek, sekwencja)``. Przykład:

.. code-block:: python
  
  sequences = [("3w12B","CGSMLAVEALYLVCGE"), ("2czyB", "APQLIMLANVALYTCGE")]


2. Lista k-tupli dla każdej sekwencji 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Stwórz listę list, gdzie każda wewnętrzna lista przechowuje wszystkie k-literowe słowa które można utworzyć z danej sekwencji. Konieczne jest także przechowywanie indeksu w sekwencji skąd dana *k-tupla* pochodzi. Dla przykładu, lista 3-tupli powstała dla dwóch sekwencji z przykładu powyżej, obie struktury danych powinny się przedstawiać następująco:

.. code-block:: python

  tuples_list = [["CGS","GSM","SML","MLA","LAV","AVE","VEA",
                  "EAL","ALY","LYL","YLV","LVC","VCG","CGE], 
                 ["APQ","PQL","QLI","LIM","IML","MLA","LAN",
                  "ANV","NVA","VAL","ALY","LYT","YTV","TVG","CGE"]]

  tuples_map = [{"CGS" : [0],"GSM" : [1],"SML" : [2],"MLA" : [3],
                 "LAV" : [4],"AVE" : [5],"VEA" : [6],"EAL" : [7],
                 "ALY" : [8],"LYL" : [9],"YLV" : [10],"LVC" : [11],
                 "VCG" : [12],"CGE" : [13]},
                {"APQ" : [0],"PQL" : [1],"QLI" : [2],"LIM" : [3],
                 "IML" : [4],"MLA" : [5],"LAN" : [6],"ANV" : [7],
                 "NVA" : [8],"VAL" : [9],"ALY" : [10],"LYT" : [11],
                 "YTV" : [12],"TVG" : [13],"CGE" : [14} ]


Lista ``tuples_list`` przechowuje tyle list *k-tupli* ile sekwencji wczytano z pliku. Każda z tych list zawiera ``N-k+1`` k-literowych wyrazów dla sekwencji o długości ``N`` aminokwasów. Lista ``tuples_map`` z kolei zawiera tyle słowników, ile sekwencji wczytano z pliku. Każdy słownik zawiera jeden lub więcej indeksów na których pojawiła się dana tupla.


3. Funkcja ``find_seeds(i,j)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Funkcja ta powinna przyjmować dwie liczby całkowite - indeksy sekwencji w bazie - i znajdować identyczne *3-tuple*. Wynikiem jej działania powinien być słownik przechowujący znalezione zalążki (identyczne *3-tuple*) zapisane jako dwu-krotka ``(k,l)`` poindeksowane różnicą ``k-l``. Dla przykładu, powiedzmy że *3-tupla* "HLV" pojawia się w sekwencji ``i`` na pozycji 23 (czyli ``k = 23``) a w sekwencji ``j`` na pozycji 29 (czyli ``l=29``). Dodatkowo, *3-tupla* "VEA" pojawia się w sekwencji ``i`` na pozycji 26 (czyli ``k = 26``) a w sekwencji ``j`` na pozycji 32 (czyli ``l=32``). Jeżeli są to jedyne dwie *3-tuple* wspólne dla sekwencji ``i``  oraz ``j``, to wywołanie funkcji ``find_seeds(i,j)`` powinno w tym przypadku zwrócić słownik:

.. code-block:: python
  
  seeds = {-6: [(23,29),(26,32)]}

Z kolei dla wspomnianych powyżej dwóch sekwencji, wywołanie ``find_seeds(0,1)`` powinno zwrócić:

.. code-block:: python
  
  seeds = {-2: [(3,5),(8,10)], -1:[(13,14)]}


4. Łączenie tupli 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Kolejnym i ostatnim etapem algorytmu jest scalanie tupli w *match*. Łączenia dokonujemy wtedy, kiedy *k-tuple* leżą na jednej diagonali w macierzy uliniowień (a więc mają taką samą wartość ``k-l``) oraz leżą niedaleko od siebie, czyli kiedy ``|ki - kj|`` jest małe (zauważ, że ``|li - lj|`` powinno dać taki sam wynik). Wartości ``ki`` oraz ``kj`` oznaczają kolejne elementy listy ze znalezionymi wynikami, czyli np ``seeds[-6]``. Przyjmij, że scalanie następuje, kiedy ``|ki - kj| < 9``, co oznacza, że jeden zalążek dzieli od drugiego 6 aminokwasów. Warunek ten oznacza, że na odcinku o 12 aminokwasach: 3 identyczne + 6 aminokwasów wstawki + 3 identyczne jest 6 identycznych aminokwasów a zatem minimum 50% identyczności sekwencyjnej. 

Ostatecznym wynikiem programu powinny być pary pasujących fragmentów sekwencji. Dla naszego przykładu porównującego białko **3w12B** z **2czyB** program powinie wypisać:

.. code-block:: python
  
  3w12B 3  2czyB 5 : MLAVEALY MLANVALY

gdyż **3** i **5** to indeksy pod którymi znajdziemy owe dwa pasujące do siebie  fragmenty sekwencji.

Dość dobrym podejściem do implementacji p. 4 (scalania zalążków) jest wstawienie pierwszego zalążka na listę wyników jako trójelementową listę ``matches = [[i_start, j_start, n_pos]]``. Następnie należy iterować po kolejnych zalążkach próbując je scalić z ostatnim jej elementem, czyli z ``matches[-1]``. Jeżeli scalenie jest możliwe, to po prostu zwiększamy odpowiednio wartość ``n_pos``. W przeciwnym przypadku dodajemy ów zalążek na koniec listy ``matches``. W kolejnym przebiegu pętli to on będzie ``matches[-1]`` 




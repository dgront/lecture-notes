.. _wstep:

Wstęp
--------------------------------------

Strona, którą właśnie zacząłeś czytać, zawiera wirtualny kurs języka Python. Takich kursów jest mnóstwo w internecie, także wiele po polsku. Ten akurat związany jest z wykładem **Wprowadzenie do programowania w naukach przyrodniczych**, prowadzonym na Wydziale Chemii UW. Konkretnym celem tego wykładu jest przygotowanie studentów do samodzielnej analizy danych, np. eksperymentalnych, potrzebnej w ich pracach dyplomowych. Dlatego też, oprócz samego programowania, poruszam zagadnienia zwiazane m.in. metodami obliczeniowymi. Wykład ten jednak ma też ogólnieszy cel - wprowadzić Cię w świat programowania w języku Python na tyle łagodnie, na ile to tylko możliwe.

Zazwyczaj we wstępie do internetowych kursów programowania znaleźć można opis, jak zorganizować sobie środowisko pracy: skąd wziąć Pythona, jak zainstalować edytor, itd. Ten kurs jest inny - jest w **pełni interaktywny**. Zawiera wiele przykładów, które możesz i **powinieneś** uruchomić i *pobawić* się nimi - zmodyfikować, zastanowić się co wyjdzie, a potem uruchomić ponownie. Aby nauka przyniosła jakieś efekty, należy też wykonać zadania domowe.

.. image:: ../../_static/okno_programu.png
   :width: 600
   :alt: Screenshot: widget do uruchamiania programu w Pythonie  

Powyższa grafika przedstawia element strony, w którym możesz pisać i uruchamiać program. Wyniki pojawią się w białym panelu (konsoli) po prawej lub poniżej. To tylko zrzut ekranu, sam skrypt `znajduje się tutaj <./Pętle.html#drukuj5>`__. Przycisk *Wyczyść konsolę* usuwa wszystko, co do tej pory program wydrukował a  *Zresetuj skrypt*  przywraca oryginalną wersję programu; spowoduje to nadpisanie wszystkich Twoich zmian, które zostały wprowadzone do programu.
Algorytmy i ich schematy blokowe
--------------------------------

Sama znajomość języka programowania (np. Pythona) to jeszcze nie wszystko - trzeba jeszcze wiedzieć, co pisać. Innymi słowy - trzeba najpierw zastanowić się nad algorytmem, a potem go zapisać w języku programowania (czyli *zaimplementować*). Skąd mam wiedzieć, jakiego algorytmu użyć? To rzeczywiście wymaga specyficznego myslenia, ale bardzo pomocne jest analizowanie klasycznych algorytmów. To trochę jak nauka gry w szachy poprzez analizowanie klasycznych partii. Do graficznej prezentacji algorytmów wykorzystujemy *schematy blokowe*, bardzo pomocne w ich analizowaniu. Te poniżej  dodatkowo są interaktywne, co powinno ułatwić Wam pracę. Przycisk **Restart** uruchamia program, klikając **Next step** można wykonywać go instrukcja po instrukcji.

.. toctree::
   :maxdepth: 1
   
   Algorytmy-przeliczanie

.. toctree::
   :maxdepth: 1
   
   Algorytmy-szereg

.. toctree::
   :maxdepth: 1
   
   Algorytmy-bisekcja

.. toctree::
   :maxdepth: 1
   
   Algorytmy-sortowanie

   


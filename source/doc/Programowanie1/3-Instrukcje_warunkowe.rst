Instrukcje warunkowe
--------------------------------------

Instrukcja warunkowa ``if`` wykonuje *to* albo *tamto* w zależności od warunku, jak widać na poniższym przykładzie:

.. raw:: html

  <div id="to_tamto"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("to_tamto.py","to_tamto", height=100, index="3.1", title="to - tamto")   
  </script>

Na instrukcję warunkową składa sie słowo kluczowe ``if``, po którym następuje *warunek logiczny*. Blok kodu wykonywanego warukowo tradycyjnie otwiera dwukropek. Instrukcja warunkowa może być bardzo rozbudowana, o operacjach na zmiennych logicznych przeczytasz :ref:`tutaj <operacje_logiczne>`

Oczywiście wykorzystany powyżej warunek ``2==2`` jest zawsze prawdziwy i program ten za każdym razem wykonuje to samo (równie dobrze można napisać: ``if True:``). Poniższy przykład jest już nieco ciekawszy - sprawdza, czy podana wartość jest parzysta:

.. raw:: html

  <div id="czy_parzysta"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("czy_parzysta.py","czy_parzysta", height=100, index="3.2", title="liczby parzyste")   
  </script>

Teraz przejdźmy do trudniejszego przykładu. Mamy 3 liczby: ``x1``, ``x2`` oraz ``x3`` i chcemy dowiedzieć się, która z nich jest największa.

.. raw:: html

  <div id="max3"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("max3.py","max3", height=210, index="3.3", title="największa z trzech")   
  </script>

Program ten wykorzystuje podwójnie zagnieżdżone warunki. Najpierw sprawdza, czy ``x1 > x2``. Jeżeli dodatkowo ``x1>x3``, to ``x1`` musi być największa. Jeżeli pierwszy warunek jest spełniony a drugi nie, to największe musi być ``x3``. W Pythonie istnieje również instrukcja ``elif``, która pozwoli nam nieco inaczej zapisać powyższy program:

.. raw:: html

  <div id="max3elif"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("max3elif.py","max3elif", height=200, index="3.4", title="największa z trzech (elif)")   
  </script>

Operator trójargumentowy
"""""""""""""""""""""""""""""""""""""

W niektórych językach programowania dostępny jest też tak zwany *operator trójargumentowy* który w zasadzie jest zwięźle zapisaną instrukcją ``if``. Operator ten zwraca pierwszą wartość, jeżeli podany warunek jest spełniony a drugą wartość - jeżeli nie jest. W Pythonie wyrażenie to ma następującą, dość intuicyjną postać: ``wynik = wartosc_1 if warunek else wartosc_2``. Zauważ, że w odróżnieniu od instrukcji warunkowej, tu nie pojawia się znak ``:``. Możemy teraz wykorzystać operator trójargumentowy do rozwiązania naszego problemu znalezienia maksymalnej wartości z pośród 3 zmiennych:

.. raw:: html

  <div id="max3_ternary"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("max3_ternary.py","max3_ternary", height=200, index="3.4", title="największa z trzech (elif)")   
  </script>

Powyższe rozwiązanie jest zwięźlejsze od `poprzedniego <#max3>`__. Wykorzystaliśmy tu pewien trick: tworzymy tymczasową zmienną ``max_val``, która przechowuje wartść maksymalną znalezioną w pierszym porównaniu dwóch zmiennych. Następnie porównujemy tą (chwilowo) maksymalną wartość z trzecią wartością. Trick ten bardzo się przydaje, kiedy liczb do porównania jest znacznie więcej, np cała lista:

.. raw:: html

  <div id="max_of_list"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("max_of_list.py","max_of_list", height=200, index="3.5", title="największa z listy")   
  </script>


Instrukcje wielokrotnego wyboru
"""""""""""""""""""""""""""""""""""""

Pojedyncza instrukcja ``if`` sprawdza jeden warunek a dalszy bieg programu może pójść tylko dwiema drogami: tą w sekcji ``if`` albo tą w sekcji ``else``. Trzeciej drogi nie ma. Co zrobić, jeżeli mamy więcej możliwości do wyboru? Wiele języków programowania oferuje instrukcję ``switch ... case``, w Pythonie jej jednak nie ma. Musimy sobie radzić, stosując wielokrotnie ``elif``.  [#]_ W poniższym przykładzie rozpatrujemy 4 możliwości ``L`` (jak lewo), ``P`` (prawo), ``G`` (góra) i ``D`` (dół). Może to być np. fragment gry, w której użytkownik porusza swoim awatarem wciskając odpowiednie klawisze na klawiaturze:

.. raw:: html

  <div id="plgd"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("plgd.py","plgd", height=200, index="3.6", title="prawo-lewo-góra-dół")
  </script>

Początkowe położenie gracza to ``0,0``. Ruch w górę zwiększa ``y`` o 1, ruch w lewo zmniejsza ``x`` o jeden i tak dalej. `Przykład 3.6 <#plgd>`__ rozwiązuje tylko problem jednego kroku, rozbudujemy go niedługo. 

.. admonition:: Uwaga
  :class: def

  Pisząc instrukcję wielokrotnego wyboru powinniśmy pamiętać o rozpatrzeniu "nieprzewidzianego" przypadku - w tym przypadku litery innej niż ``L``, ``P``, ``G`` czy ``D``.


Instrukcje ``break``, ``continue``, ``pass``
"""""""""""""""""""""""""""""""""""""""""""""""""""""

Czasem zdarza się, że chcemy przerwać pętlę przed końcem zaplanowanych iteracji. W zależności od potrzeb, stosujemy w takim przypadku instrukcje ``break`` bądź ``continue``. 

.. _break:

.. rubric:: Instrukcja ``break``

Instrukcja ``break``  definitywnie kończy pętlę w której się znajduje i tylko tą. Instrukcja ta jest bardzo przydatna w przypadku, kiedy z góry nie można okreslić końca programu. Rozbudujmy `przykład 3.6 <#plgd>`__, umożliwiając chodzenie po planszy:

.. raw:: html

  <div id="ruch_po_planszy"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("ruch_po_planszy.py","ruch_po_planszy", height=200, index="3.7", title="Ruch po planszy 2D")
  </script>

W linii 8 pojawiła się nowa możliwość - klawisz ``"Q"`` przerywa pętlę za pomocą instrukcji ``break``.
Zauważ też, że program działa w *nieskończonej pętli* ``while True:``.

.. _continue:

.. rubric:: Instrukcja ``continue``

Instrukcja ``continue``  kończy tylko bieżące okrążenie pętli i natychmiast zaczyna kolejne. Instrukcje znajdujące się poniżej ``continue`` nie zostaną wykonane. Ilustruje to poniższy przykład:

.. raw:: html

  <div id="trojkat_continue"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("trojkat_continue.py","trojkat_continue", height=200, index="3.8", title="wybrakowany trójkąt")
  </script>



.. _instrukcja_pass:

.. rubric:: Instrukcja ``pass``

A co robi instrukcja ``pass``? Absolutnie nic! Przyjrzyj się poniższemu przykładowi:


.. raw:: html

  <div id="pass"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("pass.py","pass", height=80, index="3.9", title="pass czyli nic")
  </script>


Instrukcję ``pass`` stosujemy najczęściej wtedy, kiedy nie mamy pomysłu na nic lepszego. Na przykład,
kiedy masz już napisaną pierwszą linijkę pęli (czyli instrukcję ``for``) i chcesz koniecznie sprawdzić, czy ona działa.
Coś jednak trzeba w tej pętli wpisać - pętla musi zawierać co najmniej jedną instrukcję. Może to być właśnie
``pass``, (choć akurat tu można np wpiać ``print("hello")``)

.. admonition:: Praca domowa
  :class: def

  Napisz program, który będzie sprawdzał, czy podana liczba jest rokiem przestępnym czy nie. Jak łatwo się dowiedzieć z `Wikipedii <https://pl.wikipedia.org/wiki/Rok_przest%C4%99pny>`__, latami przestępnymi są te, których numeracja:

    - jest podzielna przez 4 i niepodzielna przez 100 lub
    - jest podzielna przez 400

  Dotychczas według tej reguły lata 1600 i 2000 były przestępnymi, a lata 1700, 1800, 1900 nie. W przyszłości rok 2100 nie będzie rokiem przestępnym. 


  .. raw:: html

    <div id="rok_przestepny"></div>
    <script type="text/python">
        from ScriptWidget import ScriptWidget
        sw2 = ScriptWidget("rok_przestepny.py","rok_przestepny", height=300)
    </script>


.. rubric:: Przypisy

.. [#] Innym, bardzo eleganckim rozwiązaniem problemu wielokrotnego wyboru jest zastosowanie słownika, opisanego w rozdziale o :ref:`strukturach danych <slownik_slownikow>` 

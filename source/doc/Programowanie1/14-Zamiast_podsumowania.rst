.. _zamiast_podsumowania:

Zamiast podsumowania
--------------------------------------

Poniżej zebrałem kilka stron, które pozwoli Ci poszerzyć wiedzę o Pythonie:

  - Python Tips: https://book.pythontips.com/
  - Automatyzacja Pythonem: https://automatetheboringstuff.com/

.. _totolotek:

Totolotek
""""""""""""""""""""""""""""

Załóżmy, że właśnie dostałeś od wujka stertę historycznych wyników z totolotka:
tysiące zestawów a w każdym z nich 7 liczb od 1 do 49 włącznie. Wujek ma podejrzenia,
że kule w maszynie losującej są znaczone i pewne liczby trafiają się częściej niż inne.
Ponieważ nie uśmiecha Ci się liczenie tego ręcznie, napiszesz do tego odpowiedni program.

Rozwiązanie naiwne
--------------------------------------

Na początek wypróbujmy rozwiązanie bez wykorzystania tablic; dozwolone są jedynie
*zwykłe* zmienne, instrukcja warunkowa oraz lista.


.. raw:: html

  <div id="totolotek_the_hard_way"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw1 = ScriptWidget("totolotek_the_hard_way.py","totolotek_the_hard_way",
        height=500, alignment="top-bottom", console_height="70px")
  </script>

Bez tablic nie mamy innego wyjścia: zliczenia każdej z kul musimy trzymać w oddzielnej zmiennej.
Kompletny program byłby bardzo długi; w powyższym listingu są jedynie 4 zmienne ``k1`` - ``k4``,
zliczające kule z liczbami od 1 do 4. Kompletny program miałby ich 49 a do tego bardzo rozbudowaną
konstrukcję ``elif``.

Wykorzystanie tablic
--------------------------------------

Wykorzystanie tablic umożliwia dramatycznie uprościć ten program:

.. raw:: html

  <div id="totolotek_with_array"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("totolotek_with_array.py","totolotek_with_array", height=310, alignment="top-bottom", console_height="70px")
  </script>

Teraz, zamiast 49 oddzielnych zmiennych mamy jedną tablicę. Wartość liczbowa zapisana na kuli
wykorzystywana jest do indeksowania odpowiedniej "przegódki" w tablicy.
.. _importowanie:

Moduły i ich importowanie
--------------------------------------

Język Python to nie tylko instrukcje potrzebne do tworzenia programów. Instalacja Pythona dostarcza również rozbudowaną bibliotekę standardową, umożliwiającą jego stosowanie do wielu zadań. Biblioteka ta podzielona jest na moduły, które mogą dalej dzielić się na podmoduły. Najbardziej przydatne moduły, które przydają się już na samym początku nauki Pythona to ``math`` i ``sys``.

Aby skorzystać z funkcji bibliotecznej w swoim programie, należy ją *zaimportować*, co czynimy komendą ``import``.

.. raw:: html

  <div id="import"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("import.py","import", height=140)   
  </script>

Generalnie mamy 3 możliwości:

  - importowanie całego modułu, np. ``import math``. W takim przypadku do zawartości tego modułu (funkcji, zmiennych) musimy się odwoływać pisząc nazwę modułu (``math.sin()``)
  - importowanie pojedynczego składnika modułu, np ``from sys import argv``; polecenie to udostępnia nam listę ``argv``
  - importowanie modułu / pojedynczego składnika modułu z jednoczesną zmianą jego nazwy, co przydaje  w przypadku krótkich nazw, które mogłyby się powtarzać. Zmiana nazwy przydaje się też w przypadku długich nazw, których nie chce się nam często pisać, np: ``import matplotlib as mplt``

Lista ``argv`` z modułu sys  jest szczególnie użyteczna - zawiera ona parametry wywołania programu, podane z linii poleceń. 

.. rubric:: Polecenia ``dir()`` i ``help()``

Python umożliwia wyswietlenie zawartości każdego zaimportowanego modułu, np:

.. code-block:: python

  import math
  dir(math)

wydrukuje na ekranie co następuje:

.. code-block:: bash

  ['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'pi', 'pow', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']

dla każdej funkcji możemy wyświetlić jej opis poleceniem ``help()``, np: ``help(math.sin)`` 


.. _wlasne_moduly:

Tworzenie modułów
""""""""""""""""""""""""""""

Każdy może sobie stworzyć własny moduł w Pythonie. Wystarczy zebrać napisane przez siebie funkcje w jednym pliku. Dla przykładu weźmy plik ``chem_utils.py`` (poniżej), w którym zawarliśmy funkcję ``rozpisz_wzor()``. Funkcja ta przyjmuje jako argument wzór sumaryczny związku chemicznego, np ``"C2H5O1H1"`` (koniecznie z jedynkami) a zwraca słownik zliczający liczbę atomów poszczególnych pierwiastków w molekule, np ``{'C': 2, 'H': 6, 'O': 1}`` dla etanolu. Funkcja ta doskonale pasuje do funkcji ``masa_molowa()``, która przyjmowała by  taki właśnie słownik a zwracała masę molową związku chemicznego. [#]_ 

.. raw:: html

  <div id="chem_utils"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("chem_utils.py","chem_utils", height=300)   
  </script>

Jeżeli chcemy wykorzystać tą funkcję w innym naszy programie, dajmy na to w ``licz_masy.py``, to:

 -   w pliku ``licz_masy.py`` musimy zaimportować naszą funkcję, np tak: ``import chem_utils`` albo ``from chem_utils import rozpisz_wzor``
 - plik ``licz_masy.py`` musi znajdować się w tym samym katalogu co ``licz_masy.py``
 - ostatecznie możemy wywołać funkcję z naszego modułu: ``chem_utils.rozpisz_wzor("H2O1")`` bądź ``rozpisz_wzor("H2O1")``, zależnie od tego, jak ją zaimportowaliśmy

.. _main:

Standardowa struktura programu
"""""""""""""""""""""""""""""""""

Wszystkie instrukcje modułu muszą być schowane w jakiejś fukncji; inaczej będą one wykonywane za każdym razem, kiedy moduł jest importowany; na przykład dotyczy to linii 18: ``print(rozpisz_wzor("C2H5 O1 H1"))``. Czasem chcemy mieć program, który działa *sam z siebie*, a jednocześnie importować z niego funkcje w innym programie.  W tym przypadku na częście można zjeść ciastko i wciąż je mieć. Trzeba tylko napisać sekcję ``__main__``, jak w poniższym przykładzie. Warunek ``__name__ == "__main__"`` jest prawdą tylko wtedy, kiedy ten plik jest wykonywany.  


.. code-block:: python

  if __name__ == "__main__" :
    if len(sys.argv) > 1 :
      for arg in sys.argv[1:]:
        print(rozpisz_wzor(arg))
    else:
      print(rozpisz_wzor("C2H5 O1 H1"))

Przykład ten pokazuje również wykorzystanie listy argumentów ``sys.argv``. Jeżeli uruchomimy ten program w konsoli (Linux, MacOS) następująco:

.. code-block:: bash

  python3 chem_utils.py C2H5O1H1 H2O1
    
to słowa ``"C2H5O1H1"`` oraz ``"H2O1"`` znajdą się w liście  ``sys.argv``, zaraz za nazwą programu. Jeżeli więc lista ta ma rozmiar większy niż 1, to sekcja ``__main__`` przetwarza te argumenty. W przeciwnym przypadku - kiedy nie podano żadnych argumentów, program (w ramach testu) przetwarza wzór ``"C2H5O1H1"``.

.. admonition:: W skrócie
  :class: def

  *Nazwa pliku skryptu (programu) jest zawsze pierwszym (a często jedynym) elementem listy ``sys.argv``* 


.. rubric:: Przypisy

.. [#] Funkcja ta jest pracą domową z wykładu o strukturach danych


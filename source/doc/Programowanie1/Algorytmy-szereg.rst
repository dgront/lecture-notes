Suma szeregu raz jeszcze - wariant z ``while``
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Kolejny przykład to liczenie sumy szeregu programem, który ilustruje działanie pętli ``while``. Omawialiśmy już go  `tutaj <./Pętle.html#szereg_while2>`__. Pierwsze trzy prostokąty to inicjalizacja zmiennych - te instrukcje mogą się odbyć w dowolnej kolejności. Następnie zaczyna się pętla while (od 5 linii kodu programu), która na diagramie reprezentowana jest rombem w którym zapisano warunek. Dopóki warunek ten jest spełniony, pętla powtarza trzy instrukcje po prawej i ponownie wraca, aby sprawdzić warunek. Kiedy warunek nie jest spełniony, program drukuje wynik i kończy działanie.

.. raw:: html

  <div id="szereg_while2"></div>
  <script type="text/python">
      from ScriptDiagramWidget import ScriptDiagramWidget
      from szereg_while2_diagram import create_diagram as create_diagram2

      sw2 = ScriptDiagramWidget("szereg_while2.py","szereg_while2", create_diagram2, height=170, console_height="80px", alignment="top-bottom")
  </script> 
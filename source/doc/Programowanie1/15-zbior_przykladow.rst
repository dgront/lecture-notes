.. _zbior_przykladow

Zbiór przykładów
==================

Poniżej znajdziesz zbiór prostych przykładów do samodzielnej pracy. Polecam rozwiązywać je kolejno, gdyż ułożone są od najprostszych.
Często do zrobienia zadania wystarczy nieco przerobić rozwiązanie zadania poprzedniego.

Zmienne i proste obliczenia
--------------------------------------
#. Stwórz zmienną całkowitą, rzeczywistą oraz napisową tak, aby w każdej z nich była zawarta liczba siedemnaście
#. Wypisz na ekran kwadraty i sześciany liczb 2, 3 i 11

Pętle i warunki
--------------------------------------

#. Zadeklaruj zmienną ``N=15`` a następnie wydrukuj N znaków ``#`` korzystając z pętli
#. Korzystając z pętli wydrukuj N znaków: ``_`` dla parzystych
   a ``#`` dla nieparzystych wartości indeksu pętli
#. Powyższy wydruk powinien być w jednej linii
#. Korzystając z podwójne zagnieżdżonej pętli wydrukuj na ekranie prostokąt NxN znaków ``#``
#. Korzystając z podwójne zagnieżdżonej pętli wydrukuj na ekranie tabliczkę mnożenia od 1 do 10



Listy i słowniki
--------------------------------------

#. Stwórz pustą listę i wypełnij ją dziesięcioma zerami
#. Stwórz listę liczb od 0 do 10 włącznie
#. W powyższej liście zamień miejscami elementy 5-ty i 7-my
#. Wylosuj 20 liczb i dopisz do listy
#. Zadeklaruj zmienną napisową zawierającą słowo "konstantynopolitańczykowianeczka"; stwórz pętlę, która po kolei
   wypisze wszystkie litery tego wyrazu.
#. Wypisz litery powyższego wyrazu w odwrotnej kolejności (od ostatniej do pierwszej).
#. Wypisz 10 losowych liter z tego wyrazu.
#. Stwórz słownik zawierający wartość 12 dla klucza "C".
#. Dodaj do słownika wartości 14 i 16 odpowiednio dla kluczy "N" i "O".
#. Wydrukuj na ekranie wszystkie klucze z tego słownika.
#. (*) Stwórz dwie listy: ``lista_i = []``, do której wstawisz ``K`` (np ``K=20``) losowych liczb całkowitych z przedziału :math:`[0,N)`,
   oraz ``lista_x = []``,do której wstawisz N losowych liczb rzeczywistych. Następnie policz sumę liczb wskazaną indeksami.
   Przykład: Dla ``K=5`` i ``N=3`` możemy wylosować ``lista_i = [0, 1, 2, 1, 1]`` i ``lista_x = [0.34, 0.12, 0.98]``. Wtedy
   poprawna suma to ``0.34 + 0.12 + 0.98 + 0.12 + 0.12 = 1.68``

Przetwarzanie plików
--------------------------------------

#. Utwórz plik "dane.txt", zapisz do niego 10 liczb losowych i zamknij go.
#. Wczytaj plik "dane.txt" i wydrukuj zawartość na ekranie
#. Wczytaj plik "dane.txt" linia po linii i wydrukuj na ekranie te linie,
   które zaczynają się na 1
#. Wczytaj plik "dane.txt" i oblicz średnią z liczb w tym pliku
#. Wczytaj plik "dane.txt" linia po linii i oblicz średnią z liczb w tym pliku
#. Wczytaj plik "lotr1.txt" linia po linii i wypisz liczbę wyrazów w każdej linijce
#. (*) policz, który wyraz powtarza sie najczęściej w "Lord of the rings"
#. Wydrukuj pliki z bieżącego katalogu
#. Wydrukuj bieżący katalog
#. (*) policz, ile plików o rozszerzeniu ".jpg" jest we wszystkich podkatalogach

Własne funkcje
--------------------------------------

#. Stwórz funkcję, która dodaje dwie liczby
#. Stwórz funkcję, która rozwiązuje równanie kwadratowe;
   jej argumentami powinny być współczynniki a, b i c równania.


.. _zmienne_i_operacje:

Zmienne i operacje na nich
--------------------------------------

Zmienne i ich drukowanie
"""""""""""""""""""""""""

Naukę programowania zaczyna się zazwyczaj od pojęcia **zmiennej**. Można powiedzieć, że zmienna, to takie **nazwane** pudełko do przechowywania wartości, np. liczb. Oczywiście w komputerze nie ma pudełek. Zmienna to jakieś miejsce w pamięci komputera, które komputer odnajduje po odpowiednim numerze, np. 8048366. Numery są dla nas niewygodne, dlatego nadajemy zmiennym nazwy i za pomocą tych nazw się do nich odwołujemy. Poniższy przykład pokazuje, jak utworzyć zmienne o różnych nazwach, przypisując im różne wartości.

.. raw:: html

  <div id="tworz_zmienne"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("tworz_zmienne.py","tworz_zmienne", height=100, index="1.1", title="stwórz zmienne")   
  </script>

Wykorzystujemy w tym celu operator przypisania ``=``. Tworzona zmienna znajduje się po lewej stronie operatora, po prawej stronie zapisujemy zaś przypisywaną do niej wartość, która może zawierać wyrażenie, na przykład 2 + 2, a nawet inne zmienne. Kolejno od góry powstają zmienne: liczba całkowita ``k``, liczba rzeczywista ``z``, napis oraz zmienna logiczna. Ta ostatnia może przyjmować jedynie wartości ``False`` albo ``True``. Nazwy zmiennych muszą składać się z liter, cyfr i znaku podkreślenia, przy czym cyfra nie może rozpoczynać nazwy. Python rozróżnia wielkie i małe litery, zatem ``Ala`` i ``ala`` to dwie różne zmienne.


.. admonition:: Uwaga!
  :class: def

  Zapis programu w Pythonie musi być **odpowiednio wyjustowany**. W przypadku skryptów na tym wykładzie oznacza to na razie tyle, że każda linijka musi zaczynać się w pierwszej kolumnie.


Program powyżej może działa, a może nie ... dowodu na to nie ma, gdyż po uruchomieniu nic się nie dzieje (o ile nie ma błędu). Nie bez powodu kolejnym elementem wprowadzanym podczas nauki programowania jest instrukcja ``print()``

.. raw:: html

  <div id="drukuj_zmienne"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("drukuj_zmienne.py","drukuj_zmienne", height=150, index="1.2", title="instrukcja print()")   
  </script>

Zauważmy kilka rzeczy:

 - nie trzeba wkładać liczby do zmiennej, aby ją wydrukować; można od razu napisać ``print(7.8)``
 - do napisów zawsze potrzebny jest cudzysłów; np ``zmienna`` to nazwa zmiennej, a ``"zmienna"`` to tylko napis
 - można wydrukować kilka rzeczy na raz, oddzielając je przecinkiem, np ``print(k,z)``
 - pozwala to np na: ``print("wartość k wynosi",k)``
 - instrukcja ``print()`` sama dostawia znak końca linii (mówiąc kolokwialnie: *sama wciska enter*). Możemy też sami go wydrukować, stosując specjalny kod: ``'\n'``.

Fakt, że instrukcja ``print()`` sama przechodzi do nowej linii, powoduje czasem pewne komplikacje. W przyszłości będziemy chcieli wykorzystać ją aby wielokrotnie pisać w jednej linii. Na szczęście ten *feature* można łatwo wyłączyć, wystarczy dopisać ``end=""`` o tak:

.. raw:: html

  <div id="print_end"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("print_end.py","print_end", height=150, index="1.3", title="drukowanie końca linii")   
  </script>

.. admonition:: Ćwiczenie 1

  Poeksperymentuj różnymi znakami wpisanymi pomiędzy ``"`` a ``"`` we fragmencie ``end=""`` i porównaj oczekiwane wyniki z tym, które uzyskałaś/uzyskałeś.


Więcej o napisach
^^^^^^^^^^^^^^^^^^

Stworzona powyżej zmienna napisowa musi mieścić się w jednej linijce programu. Jeżeli napis ma mieć dwie linijki, musimy wstawić w środku znak końca linii czyli ``\n``, np: ``napis = "Pierwsza linia\nDruga linia"``. Wpisywanie w ten sposób większych fragmentów tekstu jest niewygodne. Na szczęście Python udostępnia nam dużo prostszy sposób - trzeba skorzystać z potrójnych znaków ``"``. Napis zawarty w takich potrójnych cudzysłowiach [#]_ może wyglądać, jak *prawdziwy* kawałek tekstu: z akapisami, pustymi liniami, wcięcia,i itp. Pokazuje to przykład poniżej:

.. raw:: html

  <div id="dlugi_napis"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("dlugi_napis.py","dlugi_napis", height=150, index="1.4", title="napisy o wielu liniach")   
  </script>

Więcej o liczbach
^^^^^^^^^^^^^^^^^^

Nie zawsze zmienne inicjujemy *ładnymi* wartościami, jak np. ``r=2.0``.

  - Zmienne rzeczywiste możemy zapisać w `notacji naukowej <https://pl.wikipedia.org/wiki/Notacja_naukowa>`__, np.
    ``kB = 1.380649e-23`` (stała Boltzmanna) lub ``nA = 6.02214076e23`` (liczba Avogadro)

  - Liczby w układzie szestnastkowym poprzedzamy dwoma znakami ``0x``, np. liczba ``0xFF`` to dziesiętnie 255;
    cyfry układu szestnastkowego A-F można podawać też małymi literami tzn zapis ``0xff`` również oznacza 255


Operacje arytmetyczne
"""""""""""""""""""""""""

Teraz możemy już przejść do operacji arytmetycznych.

.. raw:: html

  <div id="arytmetyczne"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("arytmetyczne.py","arytmetyczne", height=150, index="1.5", title="operacje arytmetyczne")
  </script>

Fachowo rzecz nazywając, mamy tu *zmienne* i *operatory*, np operator ``+``. Inne "oczywiste" operatory to ``-``, ``/`` oraz ``*``. Nieoczywistym operatorem jest ``%``, który oblicza resztę z dzielenia, np. ``13%4`` da jako wynik liczbę całkowitą 1. Innym, nieoczywistym operatorem matematycznym (wprowadzonym do Pythona w wersji 3.0) jest operator całkowitego dzielania bez reszty ``//``, np. ``10//3`` da jako wynik liczbę całkowitą 3. 
Operator ten stosuje się także dla liczb rzeczywistych. Wynikiem takiej operacji jest część całkowita (cecha, podłoga) z wyniku dzielenia.

Oczywiście możemy ... a nawet powinniśmy stosować nawiasy ``(`` oraz ``)``. **Uwaga:** w operacjach arytmetycznych nie używamy żadnych innych nawiasów, np kwadratowych. Mają one zupełnie inne znaczenie, podobnie z resztą jak nawiasy klamrowe.


.. raw:: html

  <div id="nawiasy"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("nawiasy.py","nawiasy", height=150, index="1.6", title="stosujmy nawiasy")   
  </script>


.. admonition:: Ćwiczenie 2

  Poeksperymentuj z nawiasami w powyższym przykładzie i ustal, jak Python traktuje kolejność / priorytet wykonywania działań. Na przykład fragment ``/2+3`` zmień na ``/(2+3)``.


Jak już wspomniałem, operator ``=`` nazywamy *operatorem przypisania*, ponieważ przypisuje zmiennej, zapisanej po jego lewej stronie wynik wyrażenia, zapisany po stronie prawej. Kolejność zapisu jest więc nieco inna od tej, do której jesteśmy przyzwyczajeni. Zazwyczaj piszemy :math:`2 + 3 = 5`, w Pythonie napiszemy jednak ``wynik = 2+3``. Najpierw wykonywane jest działanie po prawej stronie znaku równości, a następnie wynik umieszczany w zmiennej wymienionej po lewej stronie ``=``.

Kolejnym zapisem, który może sprawiać kłopoty jest konstrukcja ``x = x + 2``. Oznacza ona, że najpierw odczytujemy *aktualną* wartość zmiennej ``x``, zwiększamy ją o ``2``, a na końcu wynik z powrotem zapisujemy w ``x``. Poprzednia wartość ``x`` jest już stracona. I tak, dla przykładu, w poniższym programie w linii 1 wkładamy do zmiennej ``x`` wartość 12. Następnie w linii 3 wyjmujemy tą wartość, dodajemy do niej liczbę ``3`` co daje ``15`` i wynik zapisujemy w zmiennej ``x``. Liczba ``12`` zostaje nadpisana.

.. raw:: html

  <div id="nadpisanie"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("nadpisanie.py","nadpisanie", height=100, index="1.7", title="nadpisywanie wartości")   
  </script>

.. admonition:: Ćwiczenie 3

  Zakomentuj linię 1, stawiając na jej początku znak ``#`` i sprawdź, co się stanie.

Jak zauważyłeś, zmienna **musi** zostać utworzona przed jej pierwszym użyciem. Powyższy program nie narzeka na brak zmiennej ``y``, ponieważ jest ona utworzona specjalnie po to, aby schować do niej wynik obliczeń. Po zakomentowaniu linii 1 natomiast zmienna ``x`` pojawia się od razu *po prawej stronie* równania, co jest niedozwolone.

Język Python pozwala także na kilka innych mniej intuicyjnych operacji, jak na przykład:

.. raw:: html

  <div id="inne_operacje"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("inne_operacje.py","inne_operacje", height=230, index="1.8", title="inne operacje arytmetyczne")   
  </script>

Instrukcje w powyższym programie  to:
  - w linii pierwszej tworzymy od razu trzy zmienne: ``x``, ``y`` oraz ``z`` i nadajemy im wartości
  - w linii 2 używamy operatora ``+`` do sklejenia dwóch napisów, dodając je do siebie.
  - w linii 3 zwiększamy ``x`` o 4
  - w linii 7 zamieniamy wartościami zmienne x oraz y: *'stara'* wartość z ``x`` będzie w ``y`` i na odwrót
  - w linii 10 mamy natomiast **operator potęgowania**; tak, ``3**2`` jest równe ``9``
  - w linii 13 dowiadujemy się, że można **pomnożyć napis przez liczbę** - wynikiem tej operacji jest ten sam napis powielony wielokrotnie	 

.. _operacje_logiczne:

Operacje logiczne
"""""""""""""""""""""""""

Operowanie na zmiennych logicznych, czyli takich, które mogą przyjmować tylko dwie wartości: prawda (``True``) i fałsz (``False``) są często spotykane w programowaniu. Wykorzystujemy je do podejmowania decyzji i sterowania przebiegiem biegu programu. Wartości te są najczęściej wynikiem porównania, np. ``a>4`` albo ``b==2``. Możemy też wprost napisać ``a=False``. Operację :math:`\le` zapisujemy ``<=``, a :math:`\ge` jako ``>=``. Do zaprzeczania służy operator ``not``. Operator  :math:`\ne` oznaczamy jako ``!=``, choć możemy również napisać ``not a==b``.

.. admonition:: Uwaga

  Operator ``=`` przypisuje lewej stronie wartość po prawej, natomiast operator ``==`` sprawdza, czy obie strony równania są równe. Ta instrukcja  ``w = (x==3)`` używa obu tych operatorów. 

Na zmiennych logicznych również możemy wykonywać operacje - operacje logiczne. Operację koniungcji (*logiczne i*) zapisujemy ``and``, alternatywę (*logiczne lub*) zaś jako ``or``.
Wykorzystajmy wiedzę o operacjach logicznych i sprawdźmy czy drugie prawo de Morgana (negacja alternatywy jest równoważna koniunkcji negacji) jest prawdziwe:

.. raw:: html

  <div id="prawo_de_Morgana"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("prawo_de_Morgana.py","prawo_de_Morgana", height=100, alignment="top-bottom", index="1.9", title="operacje logiczne")
  </script>

Typy zmiennych i ich przemiany
""""""""""""""""""""""""""""""""

Powyżej już wspomniałem, że zmienne mogą być różnych typów. Typ zmiennej wynika ze sposobu jej zapisu w komputerze. Zmienna *musi* być *jakiegoś typu*.  Inaczej zapisane są liczby rzeczywiste, inaczej całkowite a napisy jeszcze inaczej. W niektórych językach programowania trzeba napisać (*zadeklarować*) jakiego typu jest dana zmienna. W Pythonie nie deklarujemy zmiennych. Typ zmiennej ustalany jest na  podstawie włożonej do niej wartości. Na przykład kiedy ``k=4`` to ``k`` jest całkowite, kiedy ``k = "Ala ma kota"`` - jest napisem, a gdy ``k=4.0`` to ``k`` jest liczbą rzeczywistą. Wynik działań zazwyczaj zgadza się z naszymi oczekiwaniami i tak ``k=3.8 + 7`` też jest liczbą rzeczywistą.  Zawsze możemy zmienić typ zmiennej na rzeczywistą stosując polecenie ``float()`` lub na całkowitą za pomocą ``int()``.

.. admonition:: Uwaga
  :class: def

  W niektórych wersjach Pythona wynik dzielenia dwóch liczb całkowitych też jest liczbą całkowitą! Dla przykładu instrukcja ``print(3/4)`` wydrukuje 0. Kiedy jednak napiszemy ``print(3/4.0)`` wynik będzie zgodny z naszymi oczekiwaniami.

Podane powyżej instrukcje, które służą do zamieniania typów zmiennych, działają też dla napisów. Wbrew pozorom przydaje się to dość często, gdyż dane które nasz program wczyta z klawiatury lub z pliku będą zawsze napisem. 

W poniższym programie operacja ``float("4.4")`` zwraca *liczbę* ``4.4``. Jak widzisz, napisów nie można przez siebie dzielić, trzeba najpierw zamienić napisy na liczby:

.. raw:: html

  <div id="dzielne_napisy"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("dzielne_napisy.py","dzielne_napisy", height=120, index="1.10", title="liczby z napisów")   
  </script>

.. admonition:: Ćwiczenie 4

  Odkomentuj zapisaną powyżej linię nr 3 usuwając znak '#' i uruchom program (*a może jednak zadziała...*)



.. rubric:: Wczytywanie wartości z klawiatury

Dotychczas wszystkie przykłady wykorzystywały wartości już wpisane do programu. Gorąco zachęcam Was do eksperymentowania i zmieniania tych liczb na inne, a nóż stanie się coś ciekawego! Warto jednak *najpierw* zastanowić się, jaki będzie wynik programu dla nowych wartości zmiennych, a potem uruchomić go i sprawdzić wynik z własnymi przewidywaniami. Jeżeli wyszło coś innego, musimy jeszcze raz przeanalizować kod programu.

Często jednak chcemy, aby program działał dla dowolnych danych, podanych przez użytkownika.
Najprostszym sposobem wprowadzenia własnych wartości wejściowych jest wczytanie ich z klawiatury. Służy do tego Pythonowa instrukcja ``input()``. Jej wersja *w przeglądarce* otwiera małe okienko w które należy wpisać wartość. Wartość ta będzie dostępna w programie jako *napis*. Poniższy przykład wczytuje wartość temperatury wyrażoną w stopniach Fahrenheita i przelicza ją na stopnie Celsjusza, wykorzystując `powszechnie znany wzór <https://pl.wikipedia.org/wiki/Skala_Fahrenheita#Przeliczanie>`__:

.. raw:: html

  <div id="fahr_2_cels"></div>
  <script type="text/python">
      from ScriptWidget import ScriptWidget
      sw2 = ScriptWidget("fahr_2_cels.py","fahr_2_cels", height=100, index="1.11", title="ze stopni Fahrenheita na Celsjusza")   
  </script>

Przykład ten, choć bardzo krótki, wart jest jednak przeanalizowania:
  - w linii pierwszej wczytujemy wartość w stopniach Fahrenheita jako napis, np ``"87.3"``
  - w linii 2 zamieniamy napis na liczbę, np. na ``87.3``; zauważ, że wynik możemy przechować w tej samej zmiennej (w tym samym pudełku), gdyż napisu tak na prawdę nie potrzebujemy
  - w linii 3 przeliczamy jedne stopnie na drugie
  - w linii 4 drukujemy wynik


.. rubric:: Przypisy

.. [#] muszą być podwojone: ``"``, te pojedyncze ``'`` nie będą działać






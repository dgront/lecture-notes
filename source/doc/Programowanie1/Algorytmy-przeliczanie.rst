Przeliczanie stopni Fahrenheita na stopnie Celsjusza
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Powróćmy najpierw do `przykładu 1.11 <./Zmienne_i_operacje.html#fahr_2_cels>`__ , w którym przeliczaliśmy stopnie Fahrenheita na stopnie Celsjusza. Przykład ten był stosunkowo prosty, posłuży nam do zapoznania się ze schematami. Mamy tu 4 prostokąty odpowiadające 4 instrukcjom programu. Zauważ, że *muszą* być ułożone w tej kolejności. Jakakolwiek zamiana spowoduje, że program przestanie działać.

.. raw:: html

  <div id="fahr_2_cels"></div>
  <script type="text/python">
      from ScriptDiagramWidget import ScriptDiagramWidget
      from fahr_2_cels_diagram import create_diagram

      sw1 = ScriptDiagramWidget("fahr_2_cels.py","fahr_2_cels", create_diagram, height=80, console_height="80px", alignment="top-bottom")
  </script>

Wykonując program kolejno instrukcje możesz prześledzić, jak zmieniają się wartości zmiennych.

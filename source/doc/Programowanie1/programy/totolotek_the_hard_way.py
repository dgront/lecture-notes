import random

N = 10000           # Tyle "kuponów" losujemy
k1 = 0              # --- "1" na kuli
k2 = 0              # --- "2" na kuli
k3 = 0              # --- "3" na kuli
k4 = 0              # --- "4" na kuli
inna = 0
for j in range(N):                      # --- pętla po losowaniach
    for i in range(6):                  # --- 6 kul w każdym losowaniu
        kula = int(random.random()*49) + 1

# ---------- Tu zliczamy krotność kul
        if kula == 1: k1 += 1           # --- linie mogą być krótsze
        elif kula == 2:                 # --- lub dłuższe
            k2 += 1
        elif kula == 3:
            k3 = k3 + 1
        elif kula == 4:
            k4 = k4 + 1
        else:
            inna = inna + 1

# ---------- A tu drukujemy wyniki
print("statystyki:")
print("'1' : ",k1)
print("'2' : ",k2)
print("'3' : ",k3)
print("'4' : ",k4)
# --- dalsze 45 linii wydruku

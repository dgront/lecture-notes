from math import sin, cos, pi as PI

x, y = 0.4, 1.5
theta = 30.0*PI/180.0
xr = x * cos(theta) - y * sin(theta)
yr = x * sin(theta) + y * cos(theta)
print(xr,yr)
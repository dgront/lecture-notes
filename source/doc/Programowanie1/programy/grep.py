import re
from sys import argv


info = """\ngrep.py: szuka wzorca w pliku

Użycie:
	python3 grep.py plik_we "wzorzec" plik_wy\n
"""

def grep(plik_we, wyrazenie, plik_wy):
	regex = re.compile(wyrazenie)
	with open(plik_we) as plik_we, open(plik_wy,'w') as plik_wy:
		for linia in plik_we:
			if regex.search(linia) : 
				plik_wy.write(linia)

if __name__ == "__main__":
	if len(argv) != 4 : print(info)
	else:
		grep(argv[1], argv[2], argv[3])

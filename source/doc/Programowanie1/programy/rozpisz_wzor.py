def rozpisz_wzor(wzor) :

  kawalki = []
  ostatni = 0
  odstepy = [0]
  for i in range(1,len(wzor)) :
    if (wzor[i-1].isupper() and wzor[i].isupper()) or \
      (not wzor[i-1].isnumeric() and wzor[i].isnumeric()) or \
      (wzor[i-1].isnumeric() and not wzor[i].isnumeric()) or \
      (wzor[i-1].islower() and wzor[i].isupper()) :

      kawalki.append(wzor[ostatni:i])
      ostatni = i
  kawalki.append(wzor[ostatni:len(wzor)])

  i=1
  wzor_sumaryczny = {}
  while i<len(kawalki) :
  	element = kawalki[i-1]
  	if not element in wzor_sumaryczny: 
  		wzor_sumaryczny[element] = 0
  	if kawalki[i].isdigit() :
  		wzor_sumaryczny[element] += int(kawalki[i])
  		i+=2
  	else:
  		wzor_sumaryczny[element] += 1
  		i+=1

  if not kawalki[-1].isdigit() : 
  	wzor_sumaryczny[kawalki[-1]] += 1

  return wzor_sumaryczny

def masa_molowa():
	pass

if __name__ == "__main__" :
	print(rozpisz_wzor("C2H5OH"))

import math

T = {}
cnt = 0
with open('../../_static/chains_from_db-uniq1000.fasta') as file:
    for linia in file:
        linia = linia.strip()
        if len(linia) > 0 and linia[0] != ">":
            # --- tu tniemy sekwencje na kawalki po 4 aminokwasy (4 litery)
            for i in range(len(linia)-3):
                pept = linia[i:i + 4]   # --- 4 literowy fragment
                if not (pept in T):     # --- czy mamy taki w słowniku?
                    T[pept] = 0         # --- nie, wstawiamy z licznikiem 0
                T[pept] = T[pept] + 1   # --- zwiększamy zliczenia o 1
                cnt += 1                # --- liczba wszystkich czwórek

# --- Teraz przejdziemy słownik raz jeszcze i poszukamy najpopulaniejszych tetrapeptydów
max_v = 0               # --- największa wartość w słwniku
max_k = ""              # --- klucz odpowiadający największej wartości
for k, v in T.items():  # --- iterujemy po parach klucz-wartość
    if v > max_v:
        max_v = v
        max_k = k
print(max_k, max_v/cnt)
# print(T)



import math

class Vec3:

  def __init__(self, id, *args):
    self.id = id
    if not args:
      self.x, self.y, self.z = 0, 0, 0
    elif len(args) == 1:
      self.x = args[0]
      self.y = args[0]
      self.z = args[0]
    elif len(args) == 3:
      self.x = args[0]
      self.y = args[1]
      self.z = args[2]

  def __str__(self):
    return "%d: %.3f %.3f %.3f" % (self.id, self.x, self.y, self.z)

v1 = Vec3()
v2 = Vec3(1)
v3 = Vec3(-1.0,1.2,2.4)
print(v1,"\n",v2,"\n",v3)
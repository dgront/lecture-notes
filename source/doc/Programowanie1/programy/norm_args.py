from math import sqrt

def norm(*args):
	sum = 0.0
	for val in args:
		sum += val*val
	return sqrt(sum)

print(norm(2.3, -1.2))
print(norm(2.3, -1.2, 0.1))
print(norm(2.3, -1.2, 5.3, 0.4))

from visualife.core.HtmlViewport import HtmlViewport
from visualife.diagrams.Diagram import Point
from visualife.diagrams.InteractiveDiagram import InteractiveDiagram

def create_diagram(dom_containing_element, diagram_name):
    drawing = HtmlViewport(dom_containing_element,0,0,345,500)

    int_dia   = InteractiveDiagram(drawing, diagram_name)
    int_dia.declare_variables(tFahr=0, tCels=0)


    start, _  = int_dia.add_start(center_at=Point(170,30))

    dane, _   = int_dia.add_box(["czytaj wartość", "zmiennej tFahr"], 150, 50, 
        "tFahr = input('podaj temperaturę w stopniach F')", below=start.node, lines="1")
    start.next_command = dane

    to_val, _ = int_dia.add_box("zamień napis tFahr na liczbę", 150, 50, 
        "tFahr = float(tFahr)", below=dane.node, lines="2")
    dane.next_command = to_val

    convert, _ = int_dia.add_box(["przelicz stopnie","Fahrenheita na Celsiusze"], 150, 50, 
        "tCels = 5/9.0 * (tFahr-32)", below=to_val.node, lines="3")
    to_val.next_command = convert

    print_it, _ = int_dia.add_box("wydrukuj wynik", 150, 50, 
        """print("Temperatura w st. Celsjusza:", tCels)""", below=convert.node, lines="4")
    convert.next_command = print_it

    stop, _ = int_dia.add_stop(below=print_it.node)
    print_it.next_command = stop

    int_dia.connect(start.node.bottom, dane.node.top)
    int_dia.connect(dane.node.bottom, to_val.node.top)
    int_dia.connect(to_val.node.bottom, convert.node.top)
    int_dia.connect(convert.node.bottom, print_it.node.top)
    int_dia.connect(print_it.node.bottom, stop.node.top)

    drawing.close()
    return int_dia

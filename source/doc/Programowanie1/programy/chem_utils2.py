import re, sys

def rozpisz_wzor(wzor):
	wzor_sumaryczny = {}
	wyniki = re.findall("[A-z]+\d+",wzor)
	for w in wyniki:
		m = re.match("([A-z]+)(\d+)",w)
		element = m.group(1)
		n = m.group(2)
		if element in wzor_sumaryczny:
			wzor_sumaryczny[element] += int(n)
		else:
			wzor_sumaryczny[element] = int(n)

	return wzor_sumaryczny

if __name__ == "__main__" :
	if len(sys.argv) > 1 :
		for arg in sys.argv[1:]:
			print(rozpisz_wzor(arg))
	else:
		print(rozpisz_wzor("C2H5 O1 H1"))

def scal_napisy(*napisy,**parametry) :
	separator = parametry.get("sep","")
	napis = napisy[0]
	for n in napisy[1:]:
		napis += separator + n
	return napis

print(scal_napisy("zupa","grzybowa", sep=","))
print(scal_napisy("zupa", "drugie","kompot","deser", sep=","))
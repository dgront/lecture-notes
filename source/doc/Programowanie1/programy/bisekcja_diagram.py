from core.HtmlViewport import HtmlViewport
from diagrams.Diagram import Point
from diagrams.InteractiveDiagram import InteractiveDiagram

def create_diagram(dom_containing_element, diagram_name):
    drawing = HtmlViewport(dom_containing_element,0,0,345,800)

    int_dia   = InteractiveDiagram(drawing, diagram_name)
    int_dia.declare_variable("tabl", "x", "k_l", "k_p", "k_s")


    start  = int_dia.add_start(center_at=Point(160,30))

    tabl   = int_dia.add_box("zainicjuj tablicę tabl", 150, 40, "tabl = [1,2,3,5,7,9]", below=start.node)
    start.next_command = tabl

    iks   = int_dia.add_box("wczytaj szukane x", 150, 40, "x = input()",
            below=tabl.node, dy="10")
    tabl.next_command = iks

    iksi = int_dia.add_box("zamień napis x na int", 150, 30, "x = int(x)",
           below=iks.node, dy="10")
    iks.next_command = iksi

    k_l = int_dia.add_box("ustal lewy zakres k_l", 150, 30, "k_l = 0",
            below=iksi.node, dy="10")
    iksi.next_command = k_l

    k_p = int_dia.add_box("ustal prawy zakres k_p", 150, 30, "k_p = len(tabl) - 1", 
            below=k_l.node, dy="10")
    k_l.next_command = k_p

# not(k_l==k_p and tabl[k_l]==x)

    while1   = int_dia.add_condition(["czy","True"], 50, "True", below=k_p.node)
    k_p.next_command = while1

    drukuj_brak = int_dia.add_box("drukuj komunikat", 80, 30, 
        """print("Nie znaleziono",x,"w tablicy")""", center_at=(while1.node.right() + Point(50, 50)))
    stop1 = int_dia.add_stop(below=drukuj_brak.node)
    while1.false_command = drukuj_brak
    drukuj_brak.next_command = stop1

    srodek  = int_dia.add_box("oblicz indeks srodka k_s", 120, 30, "k_s = int((k_p+k_l)/2)",
     center_at=(while1.node.left() + Point(-50, 50)))
    while1.true_command = srodek

    if1   = int_dia.add_condition(["czy","tabl[k_s]","==","x"], 50, "tabl[k_s]==x", 
        below=srodek.node)
    srodek.next_command = if1
    drukuj = int_dia.add_box("drukuj wynik", 70, 30, """print("znaleziono",x,"na pozycji",k_s)""",
        center_at=(if1.node.left() + Point(-20, 50)))
    if1.true_command = drukuj
    stop2 = int_dia.add_stop(below=drukuj.node)
    drukuj.next_command = stop2

    if2   = int_dia.add_condition(["czy","tabl[k_s]","&gt;","x"], 50, "tabl[k_s]>x", 
        center_at=(if1.node.right() + Point(+50, 50)))
    if1.false_command = if2

    nowy_k_p = int_dia.add_box("nowy k_p", 70, 30, "k_p = k_s - 1", 
        center_at=(if2.node.left() + Point(-15, 50)))
    nowy_k_l = int_dia.add_box("nowy k_l", 70, 30, "k_l = k_s + 1", 
        center_at=(if2.node.right() + Point(15, 50)))
    if2.true_command = nowy_k_p
    if2.false_command = nowy_k_l
    nowy_k_p.next_command = while1
    nowy_k_l.next_command = while1

    int_dia.connect(start.node.bottom(), tabl.node.top())
    int_dia.connect(tabl.node.bottom(), iks.node.top())
    int_dia.connect(iks.node.bottom(), iksi.node.top())
    int_dia.connect(iksi.node.bottom(), k_l.node.top())
    int_dia.connect(k_l.node.bottom(), k_p.node.top())
    line_1 = int_dia.connect(k_p.node.bottom(), while1.node.top())

    int_dia.connect_xy(while1.node.left(), srodek.node.top())
    int_dia.connect_xy(while1.node.right(), drukuj_brak.node.top())
    int_dia.connect(drukuj_brak.node.bottom(), stop1.node.top())

    int_dia.connect_xy(if1.node.left(), drukuj.node.top())
    int_dia.connect_xy(if1.node.right(), if2.node.top())
    int_dia.connect(drukuj.node.bottom(), stop2.node.top())

    int_dia.connect(srodek.node.bottom(), if1.node.top())
    int_dia.connect_xy(if2.node.left(), nowy_k_p.node.top())
    int_dia.connect_xy(if2.node.right(), nowy_k_l.node.top())
    p1 = Point((nowy_k_p.node.bottom().x+nowy_k_l.node.bottom().x)/2,nowy_k_l.node.bottom().y+20)
    dot1    = int_dia.add_dot(center_at=p1)
    int_dia.connect_yx(nowy_k_p.node.bottom(),p1)
    int_dia.connect_yx(nowy_k_l.node.bottom(),p1)
    p2 = Point(p1.x,p1.y+20)
    p3 = Point(drukuj_brak.node.right().x+10,p2.y)
    p4 = Point(p3.x,line_1.y)
    int_dia.connect(p1, p2, p3, p4, line_1)

    drawing.close()
    return int_dia


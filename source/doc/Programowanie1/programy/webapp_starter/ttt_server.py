from flask import Flask, request, jsonify, render_template
from markupsafe import Markup, escape
import logging

game_room_list = {"default_room":  [['' for _ in range(3)] for _ in range(3)]}

app = Flask(__name__)

# Initialize the game board
# board = [['' for _ in range(3)] for _ in range(3)]
current_player = 'X'

# Function to check for a winner
def check_winner(board):
    # Check rows, columns, and diagonals
    for i in range(3):
        if board[i][0] == board[i][1] == board[i][2] != '':
            return board[i][0]
        if board[0][i] == board[1][i] == board[2][i] != '':
            return board[0][i]
    if board[0][0] == board[1][1] == board[2][2] != '':
        return board[0][0]
    if board[0][2] == board[1][1] == board[2][0] != '':
        return board[0][2]
    return None

# Route to get the current board state
@app.route('/board', methods=['GET'])
def get_board():
    return jsonify({'board': board, 'current_player': current_player})


@app.route('/add_game_room', methods=['POST', 'GET'])
def add_game_room():
        logging.info(f"HERE: {request.args}")
        rm_name = request.args["fname"]
        if "action" in request.args and request.args["action"]=="del" and rm_name in game_room_list:
            game_room_list.remove(rm_name)
        else:
            rm_name = rm_name.replace(" ","_")
            game_room_list[rm_name] =  [['' for _ in range(3)] for _ in range(3)]
        return game_rooms()


@app.route('/manage_game_rooms')
def game_rooms():
    room_table = """<table>        <thead>
            <tr>
                <th>Game Room Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>"""
    
    for rm in game_room_list:
        room_table += f"""<tr><td><a href="/ttt:{rm}">{rm}</a></td><td><a href="/add_game_room?fname=default_room&action=del"><i class="fas fa-trash"></i></a></td></tr>\n"""

    room_table += " </tbody></table>"

    return render_template('game_rooms.html', game_rooms=Markup(room_table))


# Route to get the game page
@app.route('/ttt:<room_name>', methods=['GET'])
def main_page(room_name):
    board = game_room_list[room_name]
    board_as_dict = {}
    for row in [0, 1, 2]:
        for col in [0, 1, 2]:
            board_as_dict[f"b{row}{col}"] = board[row][col]
    logging.info(f"game sent: {board_as_dict}")
    board_as_dict["room_name"] = room_name

    return render_template('ttt.html', **board_as_dict)

# Route to make a move
@app.route('/move:<row>:<column>:<room_name>', methods=['GET'])
def make_move(row, column, room_name):
    global current_player
    row, column = int(row), int(column)
    board = game_room_list[room_name]
    logging.info(f"player move: {row} {column}")
    if board[row][column] == '':
        board[row][column] = current_player
        winner = check_winner(board)
        if winner:  
            return jsonify({'board': board, 'winner': winner})
        current_player = 'O' if current_player == 'X' else 'X'
        return main_page(room_name)
    else:
        return jsonify({'error': 'Invalid move'}), 400


logging.basicConfig(level=logging.INFO)

if __name__ == '__main__':
    app.run(debug=True)


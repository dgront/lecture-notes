from visualife.core.HtmlViewport import HtmlViewport
from visualife.diagrams.Diagram import Point
from visualife.diagrams.InteractiveDiagram import InteractiveDiagram

def create_diagram(dom_containing_element, diagram_name):
    drawing = HtmlViewport(dom_containing_element,0,0,345,800)

    int_dia   = InteractiveDiagram(drawing, diagram_name)
    int_dia.declare_variables(tabl=0, is_sorted=0, i=0, tmp=0, n=0)


    start, _  = int_dia.add_start(center_at=Point(150,30))

    tabl, _, _   = int_dia.add_box("zainicjuj tablicę tabl", 150, 40, "tabl = [3,4,8,6,2,9,1]", below=start.node)
    start.next_command = tabl

    flaga, _   = int_dia.add_box(["ustaw flagę","is_sorted na False"], 100, 50, 
        "is_sorted = False", below=tabl.node, dy="10")
    tabl.next_command = flaga

    n, _ = int_dia.add_box("ustaw n", 100, 30, "n = len(tabl)", below=flaga.node, dy="10")
    flaga.next_command = n

    while1, _   = int_dia.add_condition(["dopóki","not is_sorted"], 50, "not is_sorted", below=n.node)
    n.next_command = while1

    drukuj, _ = int_dia.add_box("drukuj tablicę", 80, 30, "print(tabl)", center_at=(while1.node.left() + Point(-50, 50)))
    while1.false_command = drukuj
    stop1, _ = int_dia.add_stop(below=drukuj.node)
    drukuj.next_command = stop1

    yes_flaga, _   = int_dia.add_box(["ustaw flagę","is_sorted","na True"], 70, 50, "is_sorted = True", center_at=(while1.node.right() + Point(50, 50)))
    while1.true_command = yes_flaga

    fori, _ = int_dia.add_box("i = 0", 50, 50, "i=0", below=yes_flaga.node)
    yes_flaga.next_command = fori

    for1, _ = int_dia.add_condition(["czy","i==n"], 50, "i==n", below=fori.node)
    fori.next_command = for1

    if1, _ = int_dia.add_condition(["czy","zamiana"], 50, "tabl[i-1]>tabl[1]", center_at=(for1.node.left() + Point(-30, 50)))
    for1.false_command = if1
    for1.true_command = while1
    tmp = int_dia.add_box(["zapisz tabl[i]","do tymczasowej"], 100, 30, "tmp = tabl[i]",
            center_at=(if1.node.right() + Point(30, 50)), dy="10")
    if1.true_command = tmp
    swap1, _ = int_dia.add_box("nadpisz tabl[i]", 100, 30, "tabl[i] = tabl[i+1]", 
            below=tmp.node, dy="10")
    tmp.next_command = swap1
    swap2, _ = int_dia.add_box("nadpisz tabl[i+1]", 100, 30, "tabl[i+1] = tmp",
            below=swap1.node, dy="10")
    swap1.next_command = swap2
    not_sorted, _ = int_dia.add_box(["ustaw flagę","is_sorted = False"], 100, 30, 
            "is_sorted = False", below=swap2.node, dy="10")
    swap2.next_command = not_sorted
    iplus1, _ = int_dia.add_box("i+=1", 50, 50, "i+=1", center_at=(for1.node.right() + Point(50, 50)))
    not_sorted.next_command = iplus1
    iplus1.next_command = for1
    if1.false_command = iplus1

    int_dia.connect(start.node.bottom(), tabl.node.top())
    int_dia.connect(tabl.node.bottom(), flaga.node.top())
    int_dia.connect(flaga.node.bottom(), n.node.top())
    line_1 = int_dia.connect(n.node.bottom(), while1.node.top())

    int_dia.connect_xy(while1.node.left(), drukuj.node.top())
    int_dia.connect_xy(while1.node.right(), yes_flaga.node.top())
    int_dia.connect(yes_flaga.node.bottom(), fori.node.top())

    int_dia.connect(fori.node.bottom(), for1.node.top())
    int_dia.connect(drukuj.node.bottom(), stop1.node.top())

    int_dia.connect_xy(for1.node.left(), if1.node.top())
    int_dia.connect_xy(if1.node.right(), tmp.node.top())
    int_dia.connect(tmp.node.bottom(), swap1.node.top())
    int_dia.connect(swap1.node.bottom(), swap2.node.top())

    p1 = Point(if1.node.left().x-10,if1.node.left().y)
    p2 = Point(p1.x,not_sorted.node.bottom().y+20)
    p3 = Point(iplus1.node.bottom().x,p2.y)

    p4 = Point(p3.x,for1.node.top().y-10)
    p5 = Point(for1.node.top().x,p4.y)
    int_dia.connect(if1.node.left(), p1, p2, p3, iplus1.node.bottom())
    int_dia.connect(iplus1.node.top(), p4, p5)
    int_dia.connect(swap2.node.bottom(),not_sorted.node.top())
    int_dia.connect(not_sorted.node.bottom(),not_sorted.node.bottom()+Point(0,20))
    p6 = Point(for1.node.right().x+20,for1.node.right().y)
    p7 = Point(p6.x,line_1.y)
    int_dia.connect(for1.node.right(),p6,p7,line_1)

    drawing.close()
    return int_dia


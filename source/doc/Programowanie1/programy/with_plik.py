import io
napis = """Ala ma kota.
Kot ma Alę.
Zuza ma psa.
"""

with io.StringIO(napis) as plik:
    for linia in plik:
        print(linia.strip().split())

l1 = [1, 2, 3]
l2 = []
for item in l1: l2.append(item)
l3 = [item for item in l1]
l3.append(4)
print(l1, l2, l3)
print(id(l1), id(l2), id(l3))

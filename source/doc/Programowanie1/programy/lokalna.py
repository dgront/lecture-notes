from math import sin, cos

r = 2.5
for i in range(10):
	t = 0.1 * i
	x = r*(t - sin(t))
	y = r*(t - cos(t))
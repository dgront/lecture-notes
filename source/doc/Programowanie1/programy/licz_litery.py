zliczenia = {} # Na początku słownik jest pusty!
dlugi_napis = """Poniższy program oblicza ile razy 
pojawiła się każda z liter w napisie wejściowym
"""
for litera in dlugi_napis:
	if not litera in zliczenia: 
		zliczenia[litera] = 0
	zliczenia[litera] += 1

for litera in zliczenia:
	print(litera, zliczenia[litera])

for litera, cnt in zliczenia.items():
	print(litera, cnt)

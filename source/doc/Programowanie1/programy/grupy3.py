import re
s1 = ">cephalosporin hydroxylase [Streptomyces megasporus] Sequence ID: WP_031505762.1"
s2 = "> Deacetoxycephalosporin C Synthase [Streptomyces clavuligerus] Sequence ID: 1W28_A"
s3 = ">NADPH-dependent FMN reductase [Candidatus Melainabacteria bacterium] Sequence ID: RAI14197.1"

wzorzec = ">\s?(.+) \[(.+)\] Sequence ID: (\w+)"
for seq in [s1,s2,s3] :
	match = re.search(wzorzec, seq)
	if match : 
		nazwa = match.group(1)
		gatunek = match.group(2)
		id = match.group(3)
		print("Nazwa:   %s\nGatunek: %s\nId:      %s\n\n" %(nazwa, gatunek, id))

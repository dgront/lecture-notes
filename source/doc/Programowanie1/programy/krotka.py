krotka = ("a", "b", "c")

# To się nie uda!
# krotka.append("d")

# Na to też nie ma szans
# krotka[1] = "xyz"
for i in range(3):
	print(krotka[i])

# pętla for-each dla krotki
for i in krotka:
	print(i)

# Rozpakowanie krotki:
na, nb, nc = krotka
print(na+nb+nc)

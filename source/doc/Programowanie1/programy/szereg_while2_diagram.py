from visualife.core.HtmlViewport import HtmlViewport
from visualife.diagrams.Diagram import Point
from visualife.diagrams.InteractiveDiagram import InteractiveDiagram

def create_diagram(dom_containing_element, diagram_name):
    drawing = HtmlViewport(dom_containing_element,0,0,345,600)

    int_dia   = InteractiveDiagram(drawing, diagram_name)
    int_dia.declare_variables(suma=0, epsilon=0, i=0, s_i=0)

    start, _  = int_dia.add_start(center_at=Point(170,30))

    suma, _   = int_dia.add_box(["zainicjuj", "zmienną suma"], 150, 40, "suma = 0", below=start.node)
    start.next_command = suma

    eps, _   = int_dia.add_box(["zainicjuj", "zmienną epsilon"], 150, 40, "epsilon = 1e-2", below=suma.node)
    suma.next_command = eps

    i_si, _ = int_dia.add_box("zainicjuj i oraz s_i", 150, 30, "i, s_i = 1, 1", below=eps.node)
    eps.next_command = i_si

    if1, _   = int_dia.add_condition(["czy","s_i &#62; eps", "i&#60;N"], 50, "s_i > epsilon and i < 50", below=i_si.node)
    i_si.next_command = if1

    drukuj, _ = int_dia.add_box("drukuj wynik", 100, 30, """print("suma =",suma,"po",i-1,"krokach")""", center_at=(if1.node.left() + Point(-50, 50)))
    if1.false_command = drukuj

    stop, _ = int_dia.add_stop(below=drukuj.node)
    drukuj.next_command = stop

    dodaj, _  = int_dia.add_box("dodaj s_i do sumy s", 120, 30, "suma += s_i", center_at=(if1.node.right() + Point(50, 50)))
    if1.true_command = dodaj

    incr, _  = int_dia.add_box("zwiększ i o 1", 120, 30, "i += 1", below=dodaj.node)
    dodaj.next_command = incr

    nowy, _  = int_dia.add_box("policz nowe s_i", 120, 30, "s_i = 1.0/(i*i)", below=incr.node)
    incr.next_command = nowy    
    nowy.next_command = if1


    # int_dia.connect(start.node.bottom(), suma.node.top())
    # int_dia.connect(suma.node.bottom(), eps.node.top())
    # int_dia.connect(eps.node.bottom(), i_si.node.top())
    # line_2 = int_dia.connect(i_si.node.bottom(), if1.node.top())
    # int_dia.connect_xy(if1.node.left(), drukuj.node.top())
    # int_dia.connect(drukuj.node.bottom(), stop.node.top())

    # int_dia.connect_xy(if1.node.right(), dodaj.node.top())
    # int_dia.connect(dodaj.node.bottom(), incr.node.top())
    # int_dia.connect(incr.node.bottom(), nowy.node.top())

    p1 = nowy.node.bottom() + Point(0,20)
    p2 = Point(nowy.node.right().x+20,p1.y)
    p3 = Point(p2.x,line_2.y)
    int_dia.connect(nowy.node.bottom(), p1, p2, p3, line_2)

    drawing.close()
    return int_dia

import math

class Vec3:

  def __init__(self,*params):
    if not params:
      self.x, self.y, self.z = 0, 0, 0
    elif len(params) == 1:
      self.x = params[0]
      self.y = params[0]
      self.z = params[0]
    elif len(params) == 3:
      self.x = params[0]
      self.y = params[1]
      self.z = params[2]

  def __str__(self):
    return "%.3f %.3f %.3f" % (self.x, self.y, self.z)

v1 = Vec3()
v2 = Vec3(1)
v3 = Vec3(-1.0,1.2,2.4)
print(v1,"\n",v2,"\n",v3)
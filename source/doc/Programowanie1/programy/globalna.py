global_x = 0

def ustaw_globalna():
    global global_x    # zarządaj dostępu do globalnej
    global_x = 1

def drukuj_globalna():
    print(global_x)     # odczyt globalnej bez pozwolenia

def drukuj_globalna2():
    global_x = 4
    print(global_x)     # globaln jest przesłonięta!

ustaw_globalna()
drukuj_globalna()      # drukuje 1
drukuj_globalna2()     # drukuje 4
print(global_x)        # drukuje .... ?
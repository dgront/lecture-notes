from browser import document
from visualife.core import HtmlViewport

drawing = HtmlViewport(document['drawing_area'], 700, 700)
# List of color names that can be used in HTML:
# https://www.w3schools.com/tags/ref_colornames.asp
drawing.rect("line_name", 100, 100, 50, 50, fill="DarkTurquoise", stroke="DarkSlateBlue", stroke_width=1)
drawing.close()


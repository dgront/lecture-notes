## Jak korzystać z biblioteki VisuaLife?

 - [draw_with_visualife.html](draw_with_visualife.html) to najprostszy możliwy przykład.
   Kod rysujący pojedynczą linię zawarty jest wewnątrz strony HTML.  Wystarczy otworzyć ten plik w przeglądarce internetowej,
   aby go uruchomić. Pythonowe wcięcia liczone są względem pierwszej linijki kodu: linia ``from browser import document, console`` wcięta jest na dwa tabulatory, tak samo jak reszta kodu.

 - [draw_with_VL_external_script.html](draw_with_VL_external_script.html) pokazuje, jak stronę HTML połączyć
   z oddzielnym plikiem ``.py`` (tu: [external_VL_script.py](external_VL_script.py)). Niestety w tym przypadku stronę trzeba udostępniać 
   poprzez serwer WWW. Najprościej użyć PyCharma, który zrobi to automatycznie.

 - [vl_mouse_example.html](vl_mouse_example.html) pokazuje, jak obsłużyć zdarzenia myszki. Rysuje kilka kwadratów i przypisuje im unikalne ``id``.

 - [vl_text_demo.html](vl_text_demo.html) pisze na ekranie tekst. **Zauważ**, jak ważny jest parametr ``text_anchor``!

## przydatne linki
 - [https://visualife.readthedocs.io/en/latest/](https://visualife.readthedocs.io/en/latest/) - strona domowa projektu
 - dokumentacja klasy [visualife.core.SvgViewport](https://visualife.readthedocs.io/en/latest/api/visualife.core.SvgViewport.html#visualife.core.SvgViewport), która udostępnia większość funkcji do rysowania 
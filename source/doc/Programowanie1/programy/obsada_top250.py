import re

import requests

# ---------- Pobieramy listę filmów TOP250
wynik = requests.get("https://www.imdb.com/chart/top/")
tekst = wynik.text                          # --- strona WWW jako tekst

lista_top_250 = []                          # --- lista na ID filmów
for linia in tekst.split("\n"):             # --- w każdej linijce szukamy ID
    wynik = re.search('data-titleid="(tt\d{7})"',linia)
    if wynik:                               # --- jak się znalazło, to dopisujemy do listy
        lista_top_250.append(wynik.group(1))

aktorzy = {}                                # --- klucz:nazwisko, wartość:licznik filmów
for film in lista_top_250:
    print("przetwarzam:",film)
    wynik = requests.get("https://www.imdb.com/title/"+film+"/fullcredits/")
    tekst = wynik.text                      # --- strona WWW z obsadą jako tekst
    for linia in tekst.split("\n"):
        a = re.search('<img height=\"44\" width=\"32\" alt=\"(\w.*?)\" ',linia)
        if a:
            aktor = a.group(1)
            if aktor in aktorzy:
                aktorzy[aktor] += 1
            else:
                aktorzy[aktor] = 1

# ---------- Sortujemy słownik wg liczby wystąpień
aktorzy_kolejno = sorted(aktorzy.keys(),key=lambda a:aktorzy[a])
for aktor in aktorzy_kolejno:
    print(aktor,aktorzy[aktor])

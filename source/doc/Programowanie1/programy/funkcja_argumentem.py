import math

def trapezoidal(func, x_min, x_max, n_steps):
    h = float(x_max - x_min) / n_steps
    result = 0.5 * (func(x_min) + func(x_max))
    for i in range(1, n_steps):
        result += func(x_min + i * h)
    result *= h
    return result

def my_own_func(x):
    return x * (math.sin(x) + math.cos(x))

print(trapezoidal(math.sin, 0, math.pi, 100))
print(trapezoidal(my_own_func, 0, math.pi, 100))

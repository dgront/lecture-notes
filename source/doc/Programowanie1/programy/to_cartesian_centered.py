from random import random
from math import sin, cos, pi as PI

def radial_to_cartesian(r,theta, center=(0,0)):
	x = r*cos(theta) + center[0]
	y = r*sin(theta) + center[1]

	return (x,y)

for i in range(10) :
	r = random() * 5.0
	theta = random() * 2 * PI
	x,y = radial_to_cartesian(r, theta)
	print("%4.2f %4.2f" %(x,y))
	x,y = radial_to_cartesian(r, theta, (1, 2))
	print("%4.2f %4.2f" %(x,y))	
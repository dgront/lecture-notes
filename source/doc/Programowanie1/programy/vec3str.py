import math

class Vec3:

  def __init__(self,x,y,z):
  	self.x = x
  	self.y = y
  	self.z = z

  def length(self):
  	return math.sqrt(self.x*self.x + \
  		self.y*self.y + self.z*self.z)

  def add(self, another_vector):
  	self.x += another_vector.x
  	self.y += another_vector.y
  	self.z += another_vector.z

  def dot_product(self, another_vector):
  	return another_vector.x*self.x + \
  		self.y * another_vector.y + \
  		self.z * another_vector.z

  def __str__(self):
    return "%.3f %.3f %.3f" % (self.x, self.y, self.z)

v1 = Vec3(-1.0,1.2,2.4)
v2 = Vec3(0,1,2)
print(v1.length(), v2.length())
v1.add(v2)
print(v1)
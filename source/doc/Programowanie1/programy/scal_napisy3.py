def scal_napisy(*napisy,**parametry) :
	separator = parametry.get("sep","")
	if isinstance(napisy[0],list):
		napis = scal_napisy(*napisy[0], sep=separator) 
	else:
		napis = napisy[0]
	for n in napisy[1:]:
		if isinstance(n,list):
			napis += separator + scal_napisy(*n, sep=separator) 
		else:
			napis += separator + n
	return napis

zupy = ["grzybowa","rosół"]
drugie = ["kotlet","kasza"]
print(scal_napisy(zupy,"kompot",drugie, sep=","))

import csv
from urllib.parse import quote
from urllib.request import urlopen

from bs4 import BeautifulSoup


# from rdkit import Chem

def name_to_smiles(ids):
    try:
        url = 'http://cactus.nci.nih.gov/chemical/structure/' + quote(ids) + '/smiles'
        ans = urlopen(url).read().decode('utf8')
        return ans
    except:
        return None


def load_data_table(fname:str):
    entries = []
    txt = open(fname).read()
    soup = BeautifulSoup(txt,features="html.parser")
    # print(soup.prettify()[100000:200000])
    for row in soup.find_all('tr'):
        if row.get('id') and row.get('id').find("row_")==0:
            entry = []
            for data in row.find_all('td'):
                entry.append(data.text)
            entries.append(entry)

    return entries


def add_ones(chem_formula: str):
    out = chem_formula[0:1]
    for c in chem_formula[1:]:
        if c.isalpha() and c.isupper() and out[-1].isalpha(): out += "1"
        out += c


def append_smiles(entries):

    complete_entries = []
    for e in entries[:] :
        name, formula, bp, mp = e
        smiles = name_to_smiles(name)
        if smiles:
            e.append(smiles)
            complete_entries.append(e)
            print(e)
    return complete_entries

if __name__ == "__main__":
    # The file comes from:
    # https://www.aatbio.com/data-sets/boiling-point-bp-and-melting-point-mp-reference-table
    entries = load_data_table("BoilingPoints.html")

    ok_entries = append_smiles(entries[:])
    print(ok_entries[0])
    with open('records.tsv', 'w') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t')
        for e in ok_entries:
            print(e)
            writer.writerow(e)

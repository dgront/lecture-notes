import re
import sys

# import tensorflow as tf
# from tensorflow import keras

BOILING = 2
MELTING = 3

reg = re.compile("([a-zA-Z]{1,2})(\d{1,3})")

# data for simple tests, just in case the input file is not available
some_data = {"C8H16": 112.5, 'C13H29N': '275.8', 'C13H28S': '290.8', 'C7H14': '80.8', 'C20H40N2': '77',
             'C8H16': '109.5', 'C4H6N2': '267', 'C4H6N2': '263'}


def parse_temperature(temp_string: str):
    temp_string = temp_string.strip().split()[0]
    if temp_string.find("-") > 0:
        values = temp_string.split("-")
        return (float(values[0]) + float(values[1])) / 2.0
    else:
        return float(temp_string)


def read_records(fname, which_temp_column):
    """Reads the all_records.tsv file and returns its content as a list of data rows"""

    rows = []
    for line in open(fname).readlines():
        tokens = line.split('\t')
        temp_str = tokens[which_temp_column].strip()
        if len(temp_str) == 0: continue
        try:
            temp = parse_temperature(temp_str)
            data_row = [tokens[1], temp, tokens[4]]  # formula, temperature, SMILES
            rows.append(data_row)
        except:
            print("Can't parse temperature:", temp_str)
    return rows


def add_ones(chem_formula: str):
    """Transforms chemical formula so every atom has its multiplicity, even if it's 1

    add_ones() adds "1" when no multiplicity is given, etc. C2H5OH is changed into C2H5O1H1
    """
    out = chem_formula[0:1]
    for c in chem_formula[1:]:
        if c.isalpha() and c.isupper() and out[-1].isalpha(): out += "1"
        out += c
    if out[-1].isalpha(): out += "1"

    return out


def count_atoms(formula: str):
    """Counts atoms by type, stores results in a dictionary

    For example: C2H5OH yields {"C":2, "H":6, "O":1}
    """
    formula = add_ones(formula)
    at_cnts = {}
    for atom, cnt in reg.findall(formula):
        if atom not in at_cnts:
            at_cnts[atom] = int(cnt)
        else:
            at_cnts[atom] += int(cnt)
    return at_cnts


# elements = ["H", "N", "C", "O", "S", "P", "F", "Cl", "Br", "As","I", "Na", "Hg", "B", "Tl", "Al", "Si", "Sn", "Cd", "Ca", "K", "Se", "Sb", "Ba", "Te", "Be", "Bi", "Cr", "Cu", "Zn", "Mg"]
elements = ["H", "N", "C", "O", "S", "P", "F", "Cl", "Br", "As", "I", "Na", "B", "Si", "Sn"]
atom_order = {name: id for (id, name) in enumerate(elements)}


def get_atom_index(symbol: str):
    if symbol in elements:
        return atom_order[symbol]
    else:
        return len(elements)


def prepare_data(input_data_rows):
    """ Converts records from read_records() into two tensors: features and labels"""
    features = []
    labels = []
    total_counts = [0 for _ in atom_order]
    total_counts.append(0)
    for formula, temp, smiles in input_data_rows:
        counts = count_atoms(formula)
        features_row = [0 for _ in atom_order]
        features_row.append(0)
        try:
            for element, count in counts.items():
                atom_index = get_atom_index(element)
                features_row[atom_index] = int(count)
                total_counts[atom_index] += int(count)
        except:
            print("Unknown element:", element)
        else:
            features.append(features_row)
            labels.append(temp)

    for element, count in zip(atom_order, total_counts):
        print(element, count)

    return features, labels


def build_network():
    model = keras.Sequential([
        keras.layers.Dense(len(features[0]), activation='relu'),
        keras.layers.Dense(256, activation='relu'),
        #        keras.layers.Dense(256, activation='relu'),
        keras.layers.Dense(256, activation='relu'),
        keras.layers.Dense(1)
    ])
    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(0.001))
    return model


if __name__ == "__main__":
    records = read_records(sys.argv[1], MELTING)
    #    print(records)
    features, labels = prepare_data(records)
    print("#", len(features), len(labels))

    # model = build_network()
    # history = model.fit(
    #     features, labels,
    #     verbose=2, epochs=5000)
    # # save the entire NN in HDF5 format
    # model.save("boiling.hdf5")


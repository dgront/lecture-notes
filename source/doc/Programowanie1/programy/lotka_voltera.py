from matplotlib import pyplot

a = 0.66
b = 1.0
g = 0.8
d = 1.0

w = 10.0
z = 10.0

dt = 0.00001
t = 0
at, az, aw = [], [], []
for i in range(100000):
    zn = z + dt * (a - b * w) * z
    wn = w + dt * (g * z - d) * w
    w = wn
    z = zn
    t += dt
    az.append(z)
    aw.append(w)
    at.append(t)

pyplot.plot(az, aw)
#pyplot.plot(at, az)
pyplot.savefig("lw.png")


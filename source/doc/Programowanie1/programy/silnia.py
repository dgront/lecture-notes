def silnia(n):
    if n < 2:              # silnia(1) oraz silnia(0) to 1
        return 1
    # wywołanie funkcji przez samą siebie ze zmniejszonym argumentem    
    return n * silnia(n-1) 

print( silnia(7) )

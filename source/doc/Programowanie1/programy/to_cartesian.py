from random import random
from math import sin, cos, pi as PI

def radial_to_cartesian(r,theta):
	x = r*cos(theta)
	y = r*sin(theta)

	return (x,y)

for i in range(10) :
	r = random() * 5.0
	theta = random() * 2 * PI
	x,y = radial_to_cartesian(r, theta)
	print("%4.2f %4.2f" %(x,y))
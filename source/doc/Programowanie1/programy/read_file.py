import io
# ------ zawartość "pliku" do wczytania
napis = """Ala ma kota.
Kot ma Alę.
Zuza ma psa.
"""

plik = io.StringIO(napis)
# ------ Wczytujemy cały plik jedną instrukcją
zawartosc = plik.read()
print(zawartosc)
plik.close()

# ------ Wczytujemy wszystkie linie z pliku jedną instrukcją
plik = io.StringIO(napis)
linijki = plik.readlines() 
print(linijki)

# ------ Wczytujemy plik linija po linii
for l in linijki:
    print(l)
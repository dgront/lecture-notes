import re
s1 = ">cephalosporin hydroxylase [Streptomyces megasporus] Sequence ID: WP_031505762.1"
s2 = "> Deacetoxycephalosporin C Synthase [Streptomyces clavuligerus] Sequence ID: 1W28_A"
s3 = ">NADPH-dependent FMN reductase [Candidatus Melainabacteria bacterium] Sequence ID: RAI14197.1"

znajdz_gatunek = "\[.+\]"
for seq in [s1,s2,s3] :
	match = re.search(znajdz_gatunek, seq)
	if match : print(match.group(0))

.. _dodatkowe_programy:

Kilka przykładów
------------------------------------------------------------

Poniżej znajdują się odnośniki do kilku przykładowych problemów, które możesz przeanalizować
aby podpatrzeć typowe rozwiązania problemów często spotykanych w programowaniu.

:ref:`Totolotek<totolotek>` - problem który dobrze ilustruje, do czego może się przydać tablica.

:ref:`Tetrapeptydy<tetrapeptydy>` - tablica jest doskonała do przechowywania danych, które są ponumerowane.
W przypadku gromadzenia napisów lepiej sprawdzi się słownik. W tym przykładzie policzysz, jak często
w przyrodzie pojawiają się różne czteroaminokwasowe kombinacje (czyli tetrapeptydy)


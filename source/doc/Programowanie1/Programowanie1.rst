.. _programowanie_1:

Podstawy programowania w języku Python
=======================================

.. toctree::
   :maxdepth: 2
   
   Wstęp

.. toctree::
   :maxdepth: 2
   
   1-Zmienne_i_operacje

.. toctree::
   :maxdepth: 2
   
   2-Pętle

.. toctree::
   :maxdepth: 2
   
   3-Instrukcje_warunkowe
   
.. toctree::
   :maxdepth: 2
   
   4-Funkcje

.. toctree::
   :maxdepth: 2
   
   5-Struktury_danych

.. toctree::
   :maxdepth: 2
   
   6-Algorytmy

.. toctree::
   :maxdepth: 2
   
   7-Importowanie

.. toctree::
   :maxdepth: 2

   9-Praca_z_tekstem

.. toctree::
   :maxdepth: 2

   10-Pliki_katalogi

.. toctree::
   :maxdepth: 2

   11-Wprowadzenie_do_obiektów

.. toctree::
   :maxdepth: 2

   12-Pythonalia


.. toctree::
   :maxdepth: 2

   13-Dodatkowe_programy

.. toctree::
   :maxdepth: 2

   14-Zamiast_podsumowania

.. toctree::
   :maxdepth: 1

   15-zbior_przykladow

Notatki do zajęć
===========================

To repozytorium zawiera notatki do prowadzonych przeze mnie wykładów oraz ćwiczeń:

:ref:`Praca w środowisku Unix<unix>`
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Prowadzenie ekspewymentów obliczeniowych jest nierozerwalnie związane z pracą w systemie Unix. W tej części przedstawię podstawy pracy w teminalu: korzystanie z zasobów dyskowych, uruchamianie zadań i analizę wyników. 

.. toctree::
   :maxdepth: 1
   :caption: Praca w środowisku Unix
   :name: Praca w środowisku Unix
   :hidden:

   doc/Unix/Unix


:ref:`Wprowadzenie do programowania w naukach przyrodniczych<programowanie_1>`
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Celem wykładu jest przekazanie podstawowych wiadomości o programowaniu. Ukończywszy kurs, studenci powinni posiadać wiedzę o podstawowych elementach, takich jak zmienne, pętle i warunki. Mam nadzieję, że wiedza ta przełoży się na praktyczną umiejętność zaimplementowania prostych algorytmów. Na wykładzie wykorzystujemy język Python, którego podstawy zostały przedstawione w :ref:`Podstawach programowania w języku Python<programowanie_1>`. W kolejnych wykładach poruszam zagadnienia potrzebne do analizy danych, np. eksperymentalnych, takie jak wczytywanie i parsowanie plików oraz podstawy metod numerycznych.

.. toctree::
   :maxdepth: 1
   :caption: Wprowadzenie do programowania
   :name: Wprowadzenie do programowania w naukach przyrodniczych
   :hidden:

   doc/Programowanie1/Programowanie1


:ref:`Programowane obiektowe<programowanie_2>`
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Podejście obiektowe może znacznie uprościć tworzenie dużego programu. Co ważniejsze, istotnie pomaga w jego rozwijaniu i utrzymywaniu. Celem tego wykładu jest przedstawienie słuchaczom obektowego sposobu myślenia, podstawowych pojęć oraz mechanizów służących konstruowaniu programów obektowych a także wzorców przyjętych w ich projektowaniu. Wykład prowadzony będzie w języku Python.


.. toctree::
   :maxdepth: 1
   :caption: Programowane obiektowe
   :name: Programowane i projektowanie obiektowe
   :hidden:

   doc/Programowanie2/Programowanie2

